from netCDF4 import Dataset,num2date
import xarray as xr
import datetime
import pdb
import numpy as np
import pandas as pd
from getargs import getargs
#
# 1. fixed offset (climatology)
# 2. Account for Temperature and Precip Obs (perfect forecast)
# 3. Account for temperature and precip forecasts, obs trainging (imperfect FC)
# 4. Account for temperature and precip forecasts, FC trainging (imperfect FC)
#


# pdb.set_trace() 
def make_sit(pars):
    
    # climfile is the 3D climate file (to get time from)
    # datafile is the datafile to add sit to 
    # interventions is dictionary with a list of 
    # lat,lon and date and density of intervention

    # first time we need this to make a empty SIT file, then
    # future runs we can simply read this!
    print (pars["climfile"])
    make_empty=False
    
    if make_empty:
        ds_clim = Dataset(pars["climfile"])
        ds_data = Dataset(pars["idatafile"],"a")
        intime=ds_clim["time"]
        if "time" not in ds_data.dimensions:
            time=ds_clim.dimensions["time"]
            otime=ds_data.createDimension(time.name,None)
            times=ds_data.createVariable("time",intime.datatype,intime.dimensions)
            ds_data["time"].setncatts(ds_clim["time"].__dict__)
            times[:]=intime[:]

        #
        # define new SIT variable (sparse!)
        #
        lat=ds_data.dimensions["lat"]
        lon=ds_data.dimensions["lon"]
        sit=ds_data.createVariable('sit', 'f4', ('time', 'lat', 'lon',))
        sit.units="SIT release density m-2"
        sit.standard_name="sit"
        sit.long_name="sit"

        #
        # define sit, first set to zero 
        # 
        sit[:,:,:]=0.0

        # write out files:
        ds_data.close()
        ds_clim.close()

    #
    # now loop over interventions: use date, lon, lat and intensities
    # interventions is a list of dictionaries, each entry has the lon,lat, date, and intensity 
    #
    
    ds=xr.open_dataset(pars["idatafile"])

    #pdb.set_trace()

    # this is where we loop over years and then set interventions according to a rule,
    # this rule may also be based on climate
    years=set(pd.DatetimeIndex(ds.time).year)

    # now make intervention list
    # offset + len
    # can make offset a function of a t2m and precip threshold being exceeded.
    # READ climate in here if you want to account for it

    dens=pars["nvec"]/pars["vecarea"]/pars["nrel"]
    
    for year in years:
        # without climate this is same every year, but will change with climate 
        doys=np.ceil(np.linspace(
            pars["init"],pars["init"]+pars["len"],pars["nrel"]))
        
        for doy in doys:
            # this allows doy>365 and uses correct calendar.
            date=datetime.datetime(year,1,1)+datetime.timedelta(doy-1)

            # pick out nearest points in space and time
            idx=ds.sel(lon=pars["lon"],lat=pars["lat"],time=date,method="nearest")
            ds["sit"].loc[dict(lon=idx.lon,lat=idx.lat,time=idx.time)]=dens

    # put opts in global attributes
    for key in pars:
        ds.attrs["sit_"+key]=pars[key]

    ds.to_netcdf(pars["odatafile"])

def main():

    # density is a tricky one.  If we release 1 million insects we assume that
    # they are dispersed over an area of ca. 0.25km**2.
    # this gives a mean density of 1e6/0.25e6 = 4 m**-2
    
    #define default opts 
    pars={}

    # instead of dates, we will have set spacing after the initial date,
    # the initial date can then be made a function of the seasonal rain/t2m

    # init and len can be climate based.
    pars["init"]=150    # doy of first release
    pars["len"]=100     # num of days in release length
    pars["nrel"]=10     # number of releases
    pars["nvec"]=1.e7    # total number of sterile males available p/annum
    pars["ratio"]=1.0    # ratio of first release to last (for increasing/decreasing)
    pars["vecarea"]=300*300
         
    pars["lon"]=[0]    # can eventually be a list that is adaptable
    pars["lat"]=[10]

    # no defaults: designed to fail 
    pars["idatafile"]="_" # input pop file with 
    pars["odatafile"]="_" # output data file 
    pars["climfile"]="_"  # climate file for time axis

    # set to true if you need to make empty data file first time
    #pars["make_empty"]=False 
    
    # overwrite from command line if you want:
    pars = getargs(pars)
    for key in ["idatafile","odatafile","climfile"]:
        if type(pars[key]) is list:
            pars[key]=pars[key][0]
        
    # for single location driver file, the lon and lat do not matter as nearest grid point is used.
    make_sit(pars)

if __name__ == "__main__":
    main()    
