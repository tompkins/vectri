#!/bin/python

# this calls burst_submit to run across a set of rcps and models.

# this uses SLURM dependencies to loop over dates
# we could submit each model and rcp here too, using the serial queue 
# but that will be slow due to limit of runnign jobs per 
# so i will farm out the model and rcp to another python sript which splits the tasks


import os, sys, getopt
import subprocess
from glob import glob
import ast, uuid

srcdir="/home/netapp-clima/users/tompkins/ISIMIP2/"
scratch="/home/netapp/clima-scratch/tompkins/output/"

def main(argv):
    """ entry point"""

#    tab="ssp2"  # this is only for jobname , not important.
#### DONT RUN THIS FOR ISIMIP    tab="grump"
#    tab="ssp15"
    tab=None

    # get the queue string
    try:
        opts, args = getopt.getopt(argv,"h",["queue=","exps=","endyear=","pops=","tab="])
    except getopt.GetoptError:
        print (argv)
        sys.exit(2)

    # ssp2, ssp1/5, grump, hist

    models=["MIROC5","HadGEM2-ES","GFDL-ESM2M","IPSL-CM5A-LR"]
    queue="esp1" # default queue
    endyear=2100

    for opt, arg in opts:
        if opt in ("-h","--help"):
            print("pass the queue string")
            sys.exit()
        elif opt in ("--queue"):
            queue=arg
        elif opt in ("--tab"):
            tab=arg
        elif opt in ("--exps"):
            exps=ast.literal_eval(arg)
        elif opt in ("--endyear"):
            endyear=int(arg)
        elif opt in ("--pops"):
            pops=ast.literal_eval(arg)

    jobid=None # first job, no dependency 
    restart=1  # this is important! 1=use vectri_restart.nc 0=artifical init conditions

    if tab=="ssp2":
        exps=["rcp26","rcp45","rcp60","rcp85"]
        pops=["ssp2"]
        restart=1
    elif tab=="grump":
        exps=["rcp26","rcp45","rcp60","rcp85"]
        pops=["grump"]   
        restart=1
    elif tab=="ssp15":
        exps=["rcp26","rcp85"]
        pops=["ssp1","ssp5"]
        restart=1
    elif tab=="hist":
        exps=["historical"]
        #pops=["hist","grump"]
        pops=["hist"]
        restart=0
    else:
        print("set Tab")
        sys.exit()

    ntasks=20
    if queue=="esp":
       ntasks=12

    # get the date list 

    # use IPSL as an example, use ??? to avoid the Africa files, 
    # THIS IS JUST TO GET THE DATES
    listfiles=glob(srcdir+"climate/tas_day_IPSL-CM5A-LR_"+exps[0]+"_r1i1p1_EWEMBI_landonly_????????-????????.nc")

    #dates=[s[-27:-10] for s in listfiles] 
    dates=[s[-20:-3] for s in listfiles]

    print(dates)
 
    #dates=dates[-1:] # fudge to run last date only to get restarts for RCP.
    #dates=dates[:1]  # fudge to run first date only for testing

    random_tag=uuid.uuid4().hex.upper()[0:8]

    for idate,date in enumerate(dates):
        year=int(date[0:4])
        if year>=endyear:
             break
        jobfile="vectri_"+date+random_tag
        with open(jobfile,"w") as fh:
            command='python3 ~/vectri/run/burst_submit.py --date="{}" --models=\"{}\" --pops=\"{}\" --exps=\"{}\" --restart={} \n'.format(date,models,pops,exps,restart)

            jobname=date[0:4]+tab

            fh.writelines("#!/bin/bash\n")
            fh.writelines("#SBATCH --job-name=%s\n" % jobname)
            fh.writelines("#SBATCH -p {}\n".format(queue))
            fh.writelines("#SBATCH -N 1 --ntasks-per-node={}\n".format(ntasks))
            fh.writelines("#SBATCH -t 0-24:00\n")     
            fh.writelines("#SBATCH -o {}vectri.%j.out\n".format(scratch))
            fh.writelines("#SBATCH -e {}vectri.%j.err\n".format(scratch))
            fh.writelines("#SBATCH --mail-type=ALL\n")
            fh.writelines("#SBATCH --mail-user=tompkins@ictp.it\n")
            fh.writelines("cd $SLURM_SUBMIT_DIR\n")
            fh.writelines("source /opt-ictp/ESMF/env201906\n")
            fh.writelines("export NETCDF_LIB=$(nf-config --flibs)\n")
            fh.writelines("export NETCDF_INCLUDE=$(nf-config --fflags)\n")
            fh.writelines("export FC=`nf-config --fc`\n")
            fh.writelines(command)

        if jobid==None:
            command=["sbatch","--parsable",jobfile]
        else:
            print ("waiting for ",jobid)
            command=["sbatch","--parsable","--dependency=afterok:"+str(jobid).rstrip(),jobfile]
        jobid=subprocess.check_output(command)
        jobid=jobid.decode('utf-8')
        print("submitted ",jobid)
        # after first submission, restart switched on
        restart=1

        #break # test, one job only

if __name__ == "__main__":
    main(sys.argv[1:])

