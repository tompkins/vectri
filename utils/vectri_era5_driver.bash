#!/bin/bash


# location of grump population file
in_pop=/home/esp-shared-a/Observations/POPULATION/GRUMP/GPWV4/gpw-v4-population-density-adjusted-to-2015-unwpp-country-totals_2020.nc

# Area to retrieve
# italy:
north=48
south=36
west=6
east=20

# years
year1=2000
year2=2002


for year in $( seq ${year1} ${year2} ) ; do 

python3 <<EOF

import cdsapi
c = cdsapi.Client()

c.retrieve(
    'reanalysis-era5-single-levels',
    {
        'product_type': 'reanalysis',
        'variable': ['2m_temperature','total_precipitation'],
        'year': str(${year}),
        'month': [str(m) for m in range(13)],
        'day': [str(d) for d in range(32)],
        'time': [
            '00:00', '01:00', '02:00',
            '03:00', '04:00', '05:00',
            '06:00', '07:00', '08:00',
            '09:00', '10:00', '11:00',
            '12:00', '13:00', '14:00',
            '15:00', '16:00', '17:00',
            '18:00', '19:00', '20:00',
            '21:00', '22:00', '23:00',
        ],
        'area': [
            ${north}, ${west}, ${south},
            ${east},
        ],
        'format': 'netcdf',
    },
    'tmp_'+str(${year})+'.nc')
EOF

# convert units and day totals
  cdo -b f32 -setattribute,tp@units="mm/day" -daysum -mulc,1000 -selvar,tp 'tmp_'${year}'.nc' rain_${year}.nc
  cdo -b f32 daymean -selvar,t2m 'tmp_'${year}'.nc' t2m_${year}.nc
  rm -f tmp*.nc
done

cdo mergetime rain_????.nc rain.nc 
cdo mergetime t2m_????.nc t2m.nc

# now get the population data and project to the area we need
cdo remapcon,t2m.nc $in_pop pop.nc
