#!/bin/python

# this uses SLURM dependencies to loop over dates
# we could submit each model and rcp here too, using the serial queue 
# but that will be slow due to limit of runnign jobs per person
# so i will farm out the model and rcp to another python script which splits the tasks

import os 
from glob import glob
import subprocess as subproc 
import multiprocessing
import getopt, sys, ast

scratch="/home/netapp/clima-scratch/tompkins/isimip/"
srcdir="/home/netapp-clima/users/tompkins/ISIMIP2/"

def main(argv):
    """ entry point"""

    #
    # GET YEAR FROM ARGUMENT
    #
    print(os.getcwd())

    #
    # defaults, can be overwritten in arguments list: 
    #
#    models=["MIROC5","HadGEM2-ES","GFDL-ESM2M","IPSL-CM5A-LR"]
#    exps=["rcp26","rcp45","rcp60","rcp85"]
#    pops=["grump"]

    models=["modelname"]
    exps=["rcp-X"]
    pops=["pop type"]


    try:
        os.mkdir(scratch)
    except:
        pass

    # get the date string
    try:
        opts, args = getopt.getopt(argv,"h",["date=","restart=","pops=","models=","exps="])
    except getopt.GetoptError:
        print (argv)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h","--help"):
            print("pass the date string")
            sys.exit()
        elif opt in ("--date"):
            date=arg
        elif opt in ("--restart"):
            restart=int(arg)
        elif opt in ("--pops"):
            pops=ast.literal_eval(arg)
        elif opt in ("--models"):
            models=ast.literal_eval(arg)
        elif opt in ("--exps"):
            exps=ast.literal_eval(arg)

    print("date ",date, " restart ",restart, "pops=",pops)

    # dictionary of runs 
    runlist=[{"exp":e,"model":m,"pop":p} for m in models for e in exps for p in pops]

    print(runlist)

    #
    # parallel calls over the run cluster 
    #
    ncore=int(multiprocessing.cpu_count())
    pool=multiprocessing.Pool(processes=ncore)
    print ("ok")
    pool.starmap(isimip_job,([run,date,restart] for run in runlist)) # initerr
    pool.close()
    pool.join()

def isimip_job(run,date,restart):
    print (" in the sub",restart)

    # make the run dir :
    rundir=scratch+run["model"]+"/"+run["exp"]+"/"+run["pop"]
    indir=rundir+"/input/"
    ofile=rundir+"/vectri_"+run["model"]+"_"+run["exp"]+"_"+run["pop"]+"_"+date+".nc"
    subproc.call(['mkdir','-p',indir])
    subproc.call(['make','-C',indir,'clean']) # always recompile from scratch

    # source code dir
    vdir=os.environ['VECTRI']

    # input files:
    tasfile=srcdir+"climate/tas_day_"+run["model"]+"_"+run["exp"]+"_r1i1p1_EWEMBI_landonly_"+date+".nc"
    prfile=srcdir+"climate/pr_day_"+run["model"]+"_"+run["exp"]+"_r1i1p1_EWEMBI_landonly_"+date+".nc"
    if run["pop"]=="grump":
        popfile=srcdir+"/population/grump_v4_globe_m2.nc"
    else:
        popfile=srcdir+"/population/popdensity_"+run["pop"]+"soc_0p5deg_daily_"+date+".nc4"

    nmfile=rundir+"/vectri_"+run["model"]+"_"+run["exp"]+"_"+run["pop"]+".namelist"

    optfilesrc=vdir+"/data/vectri_calibrated.options"    
    optfile=rundir+"/vectri_calibrated.options"    

    # move option file over to local directory as we need to add scale factor to it 
    subproc.call(['cp','-f',optfilesrc,optfile])
    fopt=open(optfile,'a')
    fopt.write('rrainfall_factor=86400.0\n')
    fopt.write('rpopdensity_min=1.e-6\n')
    fopt.write('rhostclear=7.0\n')
    fopt.write('rhostimmuneclear=365.0\n')
    fopt.write('rmigration=0.001\n')
    fopt.write('rvect_diffusion=0.01\n')
    fopt.write('loutput_rain=.false.\n')
    fopt.write('loutput_t2m=.false.\n')
    fopt.write('loutput_larvae=.false.\n')
    fopt.write('loutput_lbiomass=.false.\n')
    fopt.write('loutput_cspr=.false.\n')

    # last check all driver files are there:
    for fcheck in [tasfile,prfile,popfile,optfile]:
        if not os.path.isfile(fcheck):
            print("File ",fcheck," is missing!!!")
            sys.exit()
     
    # run command
    command=[vdir+"vectri","-c",tasfile,"-p",prfile,"-d",popfile,"-r",indir,"-o",ofile,"-n",nmfile,"-a",optfile,"-z","run_output.txt"]
    if restart==1:
        print ("RESTARTING")
        lastdate=(datetime.strptime(date,'%Y%m%d')-timedelta(days=1)).strftime('%Y%m%d')
        restart_file=glob(indir+"restart*"+lastdate+"*.nc")
        if len(restart_file)>1:
            print("too many restart files ",restart_file) 
             sys.exit()
        if len(restart_file)==0:
            restart_file=glob(indir+"../../../historical/hist/input/restart*"+lastdate+"*.nc")     if len(restart_file)!=1:
            print("don't find restart files ",restart_file)
             sys.exit()
        command+=["-i",restart_file[0]]
        print(command)
    else: 
        # two year spin up
        print ("not restarting, so add spin up",restart)
        fopt.write('nloopspinup=2 \n') 

    # close opt file
    fopt.close()

    print("command ",command)
    
    with open(indir+'stdout'+run["model"]+"_"+run["exp"]+"_"+run["pop"], 'w') as f:
        subproc.call(command, stdout=f)
    print ("model run complete: ",run["model"],run["exp"],run["pop"])
    subproc.call(["cp","-f",indir+"restart_vectri.nc",indir+"restart_"+run["model"]+"_"+run["exp"]+"_"+run["pop"]+"_"+date+".nc"])
    

if __name__ == "__main__":
    main(sys.argv[1:])



