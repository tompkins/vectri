#!/bin/bash

#
# This 2019 written script downloads the population density files from the worldpop site and converts the tif format to netcdf. The result should be two files global_pop_2010.nc and africa_pop_2010.nc (you can also of course convert other files and years if you wish by adapting the script.  Probably best to 
#

# Enter your target directory here - probably best to run this on your scratch disk, not in your GIT respository!!! 
#

targetdir=./

mkdir -p $targetdir
cd $targetdir

#
# if you only want to run in one continent you can use this:
#
filelist='Africa_1km_Population.7z Asia_1km_Population.7z Latin_America_and_the_Caribbean_1km_Population.7z'

for file in $filelist ; do 
  wget ftp://ftp.worldpop.org.uk/GIS/Population/Whole_Continent/$file
  7za e $file
done

#
# global 
#
wget ftp://ftp.worldpop.org.uk/GIS/Population/Global_2000_2020/2010/0_Mosaicked/ppp_2010_1km_Aggregated.tif

#
# convert to netcdf, here I just convert the 2010 global and Africa files
#
gdal_translate -of netCDF ppp_2010_1km_Aggregated.tif globe_pop_2010.nc
gdal_translate -of netCDF AFR_PPP_2010_adj_v2.tif africa_pop_2010.nc

#
# rename the variable since Band1 is a bit boring and vectri doesn't
# read boring non-description variables... only exciting ones like "pop"
#
ncrename -v Band1,pop globe_pop_2010.nc
ncrename -v Band1,pop africa_pop_2010.nc

#
# RECALL, once you have this data file you can interpolate the population data onto the same grid as your climate input file with the following command:
# 
# cdo remapcon,yourclimatefile.nc  africa_pop_2010.nc vectri_data_file.nc
#
