import os
import sys
import argparse
import numpy as np
import xarray as xr

def usage():
    """Prints the usage message for the script."""
    print("Vectri_point_input")
    print()
    print("This programme makes an input netcdf file from either")
    print("csv station data or by specifying constant values for temperature, rainfall and/or population")
    print("you can mix and match, e.g. read in temp/rain from files and specify a constant population")
    print("If you specify constant values for all three you also need to specify the number of days needed in the files ")
    print()
    print(" options are: ")
    print("  -h --help : print this usage message")
    print()
    print("  --lat : latitude of station (default={})".format(lat))
    print("  --lon : longitude of station (default={})".format(lon))
    print("  --temp : filename of temperature data *OR* specified constant value")
    print("  --rain : filename of rainfall data *OR* specified constant value")
    print("  --pop  : filename of population data *OR* specified constant value (default 100 per km**2)")
    print("  --climfile : OUTPUT, filename of vectri climate file (default is vectri_clim.nc)")
    print("  --datafile : OUTPUT, filename of vectri data file (default is vectri_data.nc)")
    print("  --cdate : column of yyyymmdd data - overrides next 3 options")
    print("  --cyyyy : column of yyyy data")
    print("  --cmm : column of mm data")
    print("  --cdd : column of dd data")
    print("  --ctmin : column of tmin (or tmean) data")
    print("  --ctmax : column of tmax (or tmean) data")
    print("  (if you have mean temperature then point both Tmin/max to this column)")
    print("  --crain : column of rain data")
    print("  --cpop  : column of population data")
    print("  --tmiss : missing value for temperature data")
    print("  --rmiss : missing value for rainfall data")
    print("  --nday  : number of days in file if constant values are used (start date is set to arbitrary value")
    print("  --nhead : number of header lines to ignore in files, global setting (default 0)")
    print("  --nheadt : number of header lines to ignore in temp file (defaults to nhead if not specified)")
    print("  --nheadr : number of header lines to ignore in rain file (defaults to nhead if not specified)")
    print("  --nheadp : number of header lines to ignore in pop file (defaults to nhead if not specified)")

def get_date(ifile, nh, cdate, cyyyy, cmm, cdd):
    """
    Extracts the date from the input file.

    Args:
        ifile: Filename of the input file.
        nh: Number of header lines to skip.
        cdate: Column index for date in YYYY-MM-DD format.
        cyyyy: Column index for year.
        cmm: Column index for month.
        cdd: Column index for day.

    Returns:
        date0: The starting date in YYYY-MM-DD format.
    """
    if cdate:
        dates = np.genfromtxt(ifile, skip_header=nh, usecols=(cdate-1), dtype=str)
        date0 = dates[0] 
    else:
        years = np.genfromtxt(ifile, skip_header=nh, usecols=(cyyyy-1), dtype=int)
        months = np.genfromtxt(ifile, skip_header=nh, usecols=(cmm-1), dtype=int)
        days = np.genfromtxt(ifile, skip_header=nh, usecols=(cdd-1), dtype=int)
        date0 = f"{years[0]:04d}-{months[0]:02d}-{days[0]:02d}" 
    return date0

def write_pop_nc(lat, lon, popden, datafile):
    """
    Writes the population data to a NetCDF file.

    Args:
        lat: Latitude of the station.
        lon: Longitude of the station.
        popden: Population density.
        datafile: Output filename for the NetCDF file.
    """
    ds = xr.Dataset(
        {
            'population': (('lat', 'lon'), [[popden]]),
        },
        coords={
            'lat': [lat],
            'lon': [lon],
        },
        attrs={
            'population:units': 'per km**2',
            'population:long_name': 'population density',
            '_FillValue': 9.96920997E+36, 
        }
    )
    ds.to_netcdf(datafile)
def write_clim_nc(lat, lon, time, tmeanlist, rainlist, date0, tunits, climfile):
    """
    Writes the climate data to a NetCDF file.

    Args:
        lat: Latitude of the station.
        lon: Longitude of the station.
        time: Array of time values.
        tmeanlist: List of mean temperature values.
        rainlist: List of rainfall values.
        date0: Starting date for the time dimension.
        tunits: Units for temperature.
        climfile: Output filename for the NetCDF file.
    """
    ds = xr.Dataset(
        {
            'tas': (('time',), np.array(tmeanlist)), 
            'rain': (('time',), np.array(rainlist)),
        },
        coords={
            'time': (('time',), time),
            'lat': lat,
            'lon': lon,
        },
        attrs={
            'time:standard_name': 'time',
            'time:long_name': 'time',
            'time:units': f'days since {date0} 00:00:00',
            'time:calendar': 'gregorian',
            'time:axis': 'T',
            'lat:units': 'degrees_north',
            'lon:units': 'degrees_east',
            'tas:units': tunits,
            'tas:long_name': 'Temperature',
            'tas:_FillValue': 9.96920997E+36,
            'rain:units': 'mm/day',
            'rain:long_name': 'Precipitation',
            'rain:_FillValue': 9.96920997E+36,
        }
    )
    ds.to_netcdf(climfile)

if __name__ == "__main__":
    # Default values
    lat = 10
    lon = 0
    temp = 25
    rain = 10
    pop = 100
    nhead = 0
    climfile = "vectri_clim.nc"
    datafile = "vectri_data.nc"
    cdate = None
    cyyyy = None
    cmm = None
    cdd = None
    ctmin = None
    ctmax = None
    crain = None
    cpop = None
    tmiss = None
    rmiss = None
    nday = 1

    # Parse command-line arguments
    parser = argparse.ArgumentParser(description="Create Vectri input files.")
    parser.add_argument("--lat", type=float, default=lat, help="Latitude of station")
    parser.add_argument("--lon", type=float, default=lon, help="Longitude of station")
    parser.add_argument("--temp", type=str, default=str(temp), help="Filename of temperature data or constant value")
    parser.add_argument("--rain", type=str, default=str(rain), help="Filename of rainfall data or constant value")
    parser.add_argument("--pop", type=str, default=str(pop), help="Filename of population data or constant value")
    parser.add_argument("--climfile", type=str, default=climfile, help="Output filename for climate file")
    parser.add_argument("--datafile", type=str, default=datafile, help="Output filename for data file")
    parser.add_argument("--cdate", type=int, default=None, help="Column of yyyymmdd data - overrides next 3 options")
    parser.add_argument("--cyyyy", type=int, default=None, help="Column of yyyy data")
    parser.add_argument("--cmm", type=int, default=None, help="Column of mm data")
    parser.add_argument("--cdd", type=int, default=None, help="Column of dd data")
    parser.add_argument("--ctmin", type=int, default=None, help="Column of tmin (or tmean) data")
    parser.add_argument("--ctmax", type=int, default=None, help="Column of tmax (or tmean) data")
    parser.add_argument("--crain", type=int, default=None, help="Column of rain data")
    parser.add_argument("--cpop", type=int, default=None, help="Column of population data")
    parser.add_argument("--tmiss", type=float, default=None, help="Missing value for temperature data")
    parser.add_argument("--rmiss", type=float, default=None, help="Missing value for rainfall data")
    parser.add_argument("--nday", type=int, default=nday, help="Number of days in file if constant values are used")
    parser.add_argument("--nhead", type=int, default=nhead, help="Number of header lines to ignore in files, global setting")
    parser.add_argument("--nheadt", type=int, default=None, help="Number of header lines to ignore in temp file")
    parser.add_argument("--nheadr", type=int, default=None, help="Number of header lines to ignore in rain file")
    parser.add_argument("--nheadp", type=int, default=None, help="Number of header lines to ignore in pop file")
    args = parser.parse_args()

    # Set header to global if not specified
    if args.nheadt is None:
        args.nheadt = args.nhead
    if args.nheadr is None:
        args.nheadr = args.nhead
    if args.nheadp is None:
        args.nheadp = args.nhead

    # Read temp and date from file
    if os.path.isfile(args.rain):
        # rainlist = np.genfromtxt(args.rain, skip_header=args.nheadr, usecols=(args.crain-1), dtype=float)
        rainlist = np.genfromtxt(args.rain, skip_header=1, usecols=(3), dtype=float) 
        if not hasattr(locals(), 'dateset'):
            nday = len(rainlist)
            date0 = get_date(args.rain, args.nheadr, args.cdate, args.cyyyy, args.cmm, args.cdd)
            dateset = 1
    else:
        print(f"Assuming rain input {args.rain} is a constant")
        rainlist = [float(args.rain)] * nday

    print("rain done")

    if os.path.isfile(args.temp):
        print(f"Temperature input file exists: {args.temp}")
        tminlist = np.genfromtxt(args.temp, skip_header=args.nheadt, usecols=(args.ctmin-1), dtype=float)
        tmaxlist = np.genfromtxt(args.temp, skip_header=args.nheadt, usecols=(args.ctmax-1), dtype=float)
        nday = len(tminlist)  # Ensure nday is consistent with data
        tmeanlist = (tminlist + tmaxlist) / 2.0
        date0 = get_date(args.temp, args.nheadt, args.cdate, args.cyyyy, args.cmm, args.cdd)
        dateset = 1
    else:
        print(f"Assuming temperature input {args.temp} is a constant")
        tmeanlist = [float(args.temp)] * nday

    print("rain and tmean done")

    if not dateset:
        print("No dates set or read from files")
        print("This implies both rain and temp are constants")
        print(f"Replicating constant input for {nday} days (use --nday to change)")
        date0 = "2000-01-01"  # Arbitrary date

    # If temp is constant and nday is different, replicate temp values
    if len(tmeanlist) < nday:
        tmeanlist = [float(args.temp)] * nday

	# If rain is constant and nday is different, replicate rain values
    print (len(rainlist),nday,len(tmeanlist))
    if len(rainlist) < nday:
        rainlist = [float(args.rain)] * nday

    # Ensure nday is consistent across data sources
    ndaytemp = len(tmeanlist)
    ndayrain = len(rainlist)
    if ndaytemp != ndayrain:
        print(f"ERROR, something went wrong, rain and temperature different lengths: {ndaytemp}, {ndayrain}")
        sys.exit(1)

    # Create time array
    time = np.arange(nday) 

    # Determine temperature units
    if tmeanlist[0] < 180.0:
        tunits = "deg C"
    else:
        tunits = "Kelvin"

    # Write climate file
    print("Writing climate file")
    write_clim_nc(args.lat, args.lon, time, tmeanlist, rainlist, date0, tunits, args.climfile)

    if os.path.isfile(args.pop):
        if args.pop.endswith(".nc"):
            print(f"Getting population density at lat={args.lat} and lon={args.lon} from netcdf file {args.pop}")
            ds_pop = xr.open_dataset(args.pop)
            popden = ds_pop.population.sel(lat=args.lat, lon=args.lon, method='nearest').values[0]
        else:
            with open(args.pop, 'r') as f:
                for _ in range(args.nheadp):
                    next(f)
                line = next(f)
                popden = float(line.split()[args.cpop - 1])
            print(f"Read pop value {popden} from text file {args.pop}")
        write_pop_nc(args.lat, args.lon, popden, args.datafile)
    else:
        popden = float(args.pop)
        print(f"Setting pop to constant of {popden} per sq. km")
        write_pop_nc(args.lat, args.lon, popden, args.datafile)
   
