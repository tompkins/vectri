#!/bin/bash
#
# Tompkins
# 2019 utils script to convert csv station data to a vectri input file
#
usage(){
    echo "Vectri_point_input"
    echo " "
    echo "This programme makes an input netcdf file from either"
    echo "csv station data or by specifying constant values for temperature, rainfall and/or population"
    echo "you can mix and match, e.g. read in temp/rain from files and specify a constant population"
    echo "If you specify constant values for all three you also need to specify the number of days needed in the files "
    echo " "
    echo " options are: "
    echo "  -h --help : print this usage message"
    echo " "
    echo "  --lat : latitude of station (default="${lat}")"
    echo "  --lon : longitude of station (default="${lon}")"
    echo "  --temp : filename of temperature data *OR* specified constant value"
    echo "  --rain : filename of rainfall data *OR* specified constant value"
    echo "  --pop  : filename of population data *OR* specified constant value (default 100 per km**2)"
    echo "  --climfile : OUTPUT, filename of vectri climate file (default is vectri_clim.nc)"
    echo "  --datafile : OUTPUT, filename of vectri data file (default is vectri_data.nc)"
    echo "  --cdate : column of yyyymmdd data - overrides next 3 options"
    echo "  --cyyyy : column of yyyy data"
    echo "  --cmm : column of mm data"
    echo "  --cdd : column of dd data"
    echo "  --ctmin : column of tmin (or tmean) data"
    echo "  --ctmax : column of tmax (or tmean) data"
    echo "  (if you have mean temperature then point both Tmin/max to this column)"
    echo "  --crain : column of rain data"
    echo "  --cpop  : column of population data"
    echo "  --tmiss : missing value for temperature data"
    echo "  --rmiss : missing value for rainfall data"
    echo "  --nday  : number of days in file if constant values are used (start date is set to arbitrary value" 
    echo "  --nhead : number of header lines to ignore in files, global setting (default 0)"
    echo "  --nheadt : number of header lines to ignore in temp file (defaults to nhead if not specified)"
    echo "  --nheadr : number of header lines to ignore in rain file (defaults to nhead if not specified)"
    echo "  --nheadp : number of header lines to ignore in pop file (defaults to nhead if not specified)"
}
getdate(){
    # skip header
    ifile=$1
    nh=$2
    if [ -z ${cdate} ] ; then
	if [ -z ${cyyyy} ] ; then
	    echo "%ERROR: neither cdate or cyyyy are set, cant find dates"
            exit 1
        else
            yyyy=(`awk 'FNR>h {print $x}' h=$nh x=$cyyyy < $ifile`)
            mm=(`awk 'FNR>h {print $x}' h=$nh x=$cmm < $ifile`)
            dd=(`awk 'FNR>h {print $x}' h=$nh x=$cdd < $ifile`)
            #date0=${yyyy[${nhead}]}-`printf "%0*d\n" 2 ${mm[${nhead}]}`-`printf "%0*d\n" 2 ${dd[${nhead}]}`
            date0=${yyyy[1]}-`printf "%0*d\n" 2 ${mm[1]}`-`printf "%0*d\n" 2 ${dd[1]}`
        fi 
    else
        echo reading complete date, needs to be in yyyy-mm-dd format
        dates=(`awk 'FNR>h {print $x}' h=$nh x=$cdate < $ifile `)
        date0=${dates[${1}]}
    fi 
#echo date is $date0
}


writepop(){
#
# write out the nc files with ncgen.
#
    cdlfile=cdlfile.cdl
    cat << EOF2 > $cdlfile
    netcdf popufile {    // example netCDF specification in CDL
     
     dimensions:
     lat = 1, lon = 1;
     
     variables:
       double     lat(lat), lon(lon);
       float      population(lat,lon);
       lat:units = "degrees_north";
       lon:units = "degrees_east";
       population:units = "per km**2";
       population:long_name = "population density";
       population:_FillValue = 9.96920997E+36;
     
     data:
EOF2

     # add data
     echo "lat=${lat};" >> $cdlfile
     echo "lon=${lon};" >> $cdlfile
     echo "population=$popden;" >> $cdlfile
     echo "}" >> $cdlfile

     # convert to netcdf
     ncgen -o $datafile $cdlfile
}

writeclim(){
#
# write out the nc files with ncgen.
#
    cdlfile=cdlclim.cdl

    # comma separated list 
    printf -v time "%s," "${days[@]}"
    time="${time%,}"
    printf -v tas "%s," "${tmeanlist[@]}"
    tas="${tas%,}"
    printf -v pr "%s," "${rainlist[@]}"
    pr="${pr%,}"


    cat << EOF2 > $cdlfile
    netcdf climfile {    // example netCDF specification in CDL
     
     dimensions:
     time = UNLIMITED ; 
     lat = 1, 
     lon = 1;
     
     variables:
       double time(time) ;
       time:standard_name = "time" ;
       time:long_name = "time" ;
       time:units = "days since ${date0} 00:00:00" ;
       time:calendar = "gregorian" ;
       time:axis = "T" ;
       float      lat(lat), lon(lon);
       float      tas(time,lat,lon), rain(time,lat,lon);
       lat:units = "degrees_north";
       lon:units = "degrees_east";
       tas:units = "${tunits}";
       tas:long_name = "Temperature";
       tas:_FillValue = 9.96920997E+36;
       rain:units = "mm/day";
       rain:long_name = "Precipitation";
       rain:_FillValue = 9.96920997E+36;
     data:
EOF2
     # add data
     echo "time=${time};" >> $cdlfile
     echo "lat=${lat};" >> $cdlfile
     echo "lon=${lon};" >> $cdlfile
     echo "tas=${tas};" >> $cdlfile
     echo "rain=${pr};" >> $cdlfile
     echo "}" >> $cdlfile

     # convert to netcdf
     ncgen -o $climfile $cdlfile
#     rm -f $cdlfile
}


#
# default 
#

# fixed values by default
temp=25
rain=10
pop=100

nhead=0
climfile=vectri_clim.nc
datafile=vectri_data.nc
lat=10
lon=0

nday=1     # default

#
# double check 
#
echo $VECTRI
if [ -z ${VECTRI} ] ; then 
  echo VECTRI is not set to point to your code, please set this up according to the instructions
  exit 1
else
  echo ---------------------------
  echo VECTRI is set and points to: $VECTRI
  echo ---------------------------
fi 

#
# need realpath (not mac so replace)
#
#if [[ "$(realpath "$(pwd)")"/ == "$(realpath "${VECTRI}")"/* ]]; then
#  echo "error, do not run code in (sub)directory of " $VECTRI
#  exit 1
#fi   

# less robust than realpath but works in most cases if realpath not available.
if [[ `pwd` == *${VECTRI}* ]]; then
  echo DO NOT RUN VECTRI IN YOUR GIT REPOSITORY!!!
  exit 1
fi


#
# get options
#
optslist=h,help,lat,lon,temp,rain,pop,cdate,cyyyy,cmm,cdd,ctmin,ctmax,crain,cpop,tmiss,rmiss,nday,nhead,nheadt,nheadr,nheadp,climfile,datafile,

while getopts h-: OPT; do  # allow -a, -b with arg, -c, and -- "with arg"
  # support long options: https://stackoverflow.com/a/28466267/519360
  if [ "$OPT" = "-" ]; then   # long option: reformulate OPT and OPTARG
    OPT="${OPTARG%%=*}"       # extract long option name
    OPTARG="${OPTARG#"$OPT"}" # extract long option argument (may be empty)
    OPTARG="${OPTARG#=}"      # if long option argument, remove assigning `=`
  fi
  found=false
  for arg in ${optslist//,/ } ; do
      case "$OPT" in
          h | help )
	      usage
	      exit
	      ;;
	  ${arg} )
	      declare ${arg}=${OPTARG}
	      found=true
	      break
	      ;;
      esac
  done
  if [ $found = false ] ; then
      echo bad argument $OPT
      usage 
      exit
  fi 
done

#
# set header to global if not specified
#
if [ -z ${nheadt} ] ; then 
  nheadt=$nhead
fi
if [ -z ${nheadr} ] ; then 
  nheadr=$nhead
fi
if [ -z ${nheadp} ] ; then 
  nheadp=$nhead
fi

# read temp and date from file

if [ -e $rain ] ; then
 # rainlist=(`awk 'FNR>h {print $x}' x=$crain h=$nheadr < $rain`)
  rainlist=(`awk 'FNR>1 {print $x}' x=4 h=1 < $rain`)
  if [ -z $dateset ] ; then 
    nday=`echo ${#rainlist[@]}`
    getdate $rain $nheadr
    dateset=1
  fi
else 
  echo assuming rain input $rain is a constant 
  rainlist=$rain # fudge to get one element array
fi

echo rain done

if [ -e $temp ] ; then
  echo temperature input file exists $temp $nheadt
  tminlist=(`awk 'FNR>h {print $x}' x=$ctmin h=$nheadt < $temp`)
  tmaxlist=(`awk 'FNR>h {print $x}' x=$ctmax h=$nheadt < $temp`)
  nday=`echo ${#tminlist[@]}`
  for (( ix=0; ix<${nday}; ix++ )); do
      tmeanlist+=$(echo "(${tminlist[$ix]} + ${tmaxlist[$ix]}) / 2" | bc -l)" "
  done
  tmeanlist=($(echo $tmeanlist)) # convert to array 
  #  declare -p tmeanlist 
  getdate $temp $nheadt
  dateset=1
else 
  # not a file so assume is a constant
  echo assuming temperature input $temp is a constant
  tmeanlist=$temp # fudge to get one element "array"
fi

echo rain and tmean done

#
# date not set
#
if [ -z $dateset ] ; then 
  echo "no dates set or read from files"
  echo "this implies both rain and temp are constants"  
  echo "replicating constant input for $nday days (use --nday to change)"
  date0=2000-01-01 # arbitary date
fi

#
# if temp is constant then less nday and replicate
#
if [ ${#tmeanlist[@]} -lt ${nday} ] ; then 
  # replicate 
  declare -a tmeanlist=( $(for i in $(seq 1 $nday ); do echo $temp; done) )
fi

#
# simple autodetection of units
#
if (( $(echo "${tmeanlist[0]} < 180.0" | bc -l) )) ; then
  tunits="deg C"
else
  tunits="Kelvin"
fi

#
# if rain is constant then less nday and replicate
#
if [ ${#rainlist[@]} -lt $nday ] ; then 
  # replicate 
  declare -a rainlist=( $(for i in $(seq 1 $nday); do echo $rain; done) )
fi 

#
# nday and date0 is now defined but days 1..n are not, do here:
#

# if created by replication this does nothing, otherwise sets to data length
ndaytemp=${#tmeanlist[@]} 
ndayrain=${#rainlist[@]}
if [ $ndaytemp -ne $ndayrain ] ; then
  echo "ERROR, something went wrong, rain and temperature different lengths",$ndaytemp,$ndayrain
  exit 1
fi 
declare -a days=($(for i in $(seq 0 $(echo $nday-1 | bc)); do echo $i; done) )

#
# write the clim file 
#
echo writing climate file
writeclim

if [ -e $pop ] ; then
  if [ ${pop:(-3)} == ".nc" ] ; then
    echo "getting population density at lat="$lat "and lon=" $lon " from netcdf file "$pop 
    cdo -remapnn,lon=$lon/lat=$lat $pop $datafile
  else
    for i in `seq 1 $nheadp` ; do read -r line < $pop ; done
    read -r line < $pop
    popden=`echo $line | awk -v cpop=$cpop '{print $cpop}'`
    echo read pop value $popden from text file $pop 
    writepop
  fi
else
  # value is a constant
  popden=$pop 
  echo "setting pop to constant of $popden per sq. km, I do hope this is a numeric value"
  writepop
fi  

