#!/bin/python

from os import system,getenv
import subprocess
import matplotlib.pyplot as plt
import numpy as np

mode=1
rainplot=10
tempplot=25
pop=100
nstep=500
nens=50

VECTRI=getenv("VECTRI")

tempv=np.linspace(17,39,num=nens)
rainv=np.linspace(0.1,30,num=nens)

vars=["eir","cspr","PRd","waterfrac","immunity","cases"]
varsf=[365,100,100,100,100,1000]
varsu=[" (bites per year)"," (%)"," (%)"," (%)"," (%)"," (per thousand)"]
yaxis=["log","linear","linear","linear","linear","linear"]

indexr=-999
indext=-999

vals=np.zeros((len(vars),len(tempv),len(rainv)))

# temperature/rain loop
for irain,rain in enumerate(rainv) :
    if (indexr==-999 and rain>rainplot):
        indexr=irain
    for itemp,temp in enumerate(tempv) :
        if (indext==-999 and temp>tempplot):
            indext=itemp


        # make the netcdf files

        
        ### RUN the utils script to generate const 
        system("$VECTRI/utils/vectri_point_input --temp "+str(temp)+" --rain "+str(rain)+" --nday 300")

        # run the code
        system("$VECTRI/vectri -c vectri_clim.nc -d vectri_data.nc")

        # average the output
        system("ncks -O 3 vectri.nc v3.nc")
        system("cdo -s -timmean v3.nc tm.nc")

        # pick up the values and store
        for ivar,var in enumerate(vars) : 
            val=subprocess.check_output("ncdump -v,{0} tm.nc | tail -n 2 | head -n 1 | awk '{{print $1;}}'".format(var), shell=True)
            print (var,val)
            val=max(1.e-5,float(val))
            vals[ivar,itemp,irain]=float(val)*varsf[ivar]

plt.figure(1)
for ivar,var in enumerate(vars): 
    plt.subplot(len(vars)/2,2,ivar)
    plt.plot(tempv,vals[ivar,:,indexr])
    plt.xlabel('Temperature (C)')
    plt.ylabel(vars[ivar]+varsu[ivar])
    plt.tight_layout()
    plt.yscale(yaxis[ivar])
plt.savefig('eir_equil_temp.pdf',dpi=300)
plt.clf()

plt.figure(1)
for ivar,var in enumerate(vars): 
    plt.subplot(len(vars)/2,2,ivar)
    plt.plot(tempv,vals[ivar,indext,:])
    plt.xlabel('Rainfall (mm/day)')
    plt.ylabel(vars[ivar]+varsu[ivar])
    plt.tight_layout()
    plt.yscale(yaxis[ivar])
plt.savefig('eir_equil_rain.pdf',dpi=300)
plt.clf()





