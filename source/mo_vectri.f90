MODULE mo_vectri
!-------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins AM. 2011, ICTP
! tompkins@ictp.it
!
! main arrays and variables 
! for mosquitoes, larva -- all gridded data 
!--------------------------------------------------------
  USE mo_constants
  USE mo_control
  USE ISO_FORTRAN_ENV, ONLY : INT32, INT64

  IMPLICIT NONE

  ! spatial dimensions
  INTEGER  :: nlat,nlon
 
  ! time dimensions 
  INTEGER :: nday        ! total integration time in days
  INTEGER :: start_date  ! start date yyyymmdd

  ! conversion of immune rate into tau parameter
  ! assumes 95% are immune at this value
  REAL :: rimmune_gain_tau

  ! daily death rate
  REAL :: rpop_death_rate_daily

  ! fraction of grid cell covered in water that offers breeding sites 
  ! this indicates pools, but not deep lakes
  ! There will be two components, permanent water bodies ponds
  ! and edges of lakes and rivers
  ! A second component that grows temporary pools with a 
  ! simple pond model, stored in rwaterfrac - 
  ! the total is in rwaterfrac *which is the prognostic variable*
  ! from v1.3.4 the permanent fraction is a function of space ready for 
  ! the LANDSAT/Sentinel classification maps.
  !
  ! from v1.9 infil is now also a function of lat and lon
  REAL, ALLOCATABLE :: rwaterpond(:,:)  ! nlat,nlon  ! temporary ponds        
  REAL, ALLOCATABLE :: rwaterperm(:,:)  ! nlat,nlon  ! permanent features, lakes rivers etc        
  REAL, ALLOCATABLE :: rwaterurbn(:,:)  ! nlat,nlon  ! urban water availiability (gutters, tires, cans etc)
  REAL, ALLOCATABLE :: rsoilinfil(:,:)  ! nlat,nlon          

  ! zoophilic rate, in places with low population rate zoophilic rate is 
  ! higher - Kileen et al. (2001) quotes two papers that show high trend 
  ! in Gambia, while sensitivity lower in Tanzania - will also depend
  ! on method since knockdown in huts could exaggerate anthropophilicity
  ! NOTE: zoophilic actually is anthropophilic rate!!! 1.0-zoo
  ! rbitezoo is just to make things faster...
  REAL, ALLOCATABLE :: rzoophilic(:,:), rbitezoo(:,:)  ! nx,ny

  ! From v2.0 there will be multiple species of Anopheles and Aedes
  ! Subspecies can be held in the same generalized array, but the two species
  ! are kept separate as they transmit different diseases and have different
  ! behaviours re. water use and also mortality functions. 
  ! 
  ! number of female mosquitoes in gridcell 
  ! *** ngono defined the resolution of the gonotrophic cycle (up to v1.7)
  ! new vectors start in index 0 of ngono until they get a blood meal 
  !   they then move to box i, and advance according to degree days.
  ! from v1.8 onwards the gonotrophic cycle no longer resolved.
  !
  ! likewise ninfv describes the evolution of the parasite within the vector
  !   again, new vectors start and remain in box iinfv=zero until they 
  !   are infected during feeding.
  REAL, ALLOCATABLE :: rvect(:,:,:) ! ranoph(0:ninfv,nx,ny) soon -> ,nspecanoph)

  ! INTERVENTIONS ON VECTORS v1.10 (v2.0)? 
  ! a) Steralized insect technique SIT
  REAL, ALLOCATABLE :: rsitf(:,:) ! Density SIT affected female densit
  REAL, ALLOCATABLE :: rsitm(:,:) ! Density SIT males density (released *density* required)
  REAL, ALLOCATABLE :: rsit_release(:,:)  ! nlat,nlon  ! SIT interventions        
  ! b) Bednets (all  LLINs)
  REAL, ALLOCATABLE :: rbednet(:,:)  ! nlat,nlon  ! LLIN bednet in place (m$^{-2}$)
  REAL, ALLOCATABLE :: rbednet_release(:,:)  ! nlat,nlon  ! LLIN bednet distribution (m$^{-2}$)
  REAL :: rbednet_fac
  ! c) Indoor Residual Spraying (IRS)
  ! still to do
  ! d) genetic modification
  ! still to do
  
  
  ! POSSIBLE DEVELOPMENT:
  !
  ! REAL, ALLOCATABLE :: ranoph(:,:,:) ! ranoph(0:ninfv,nx,ny) soon -> ,nspecanoph)
  ! REAL, ALLOCATABLE :: raedes(:,:,:) ! raedes(0:ninfv,nx,ny) soon -> ,nspecaedes)

  ! larvae density per m**2 
  ! multiplied by water coverage to give total per box
  ! water coverage includes permanent pools, and adjusts with rains
  ! thus if 10 day rain rate is zero still have breeding sites
  REAL, ALLOCATABLE :: rlarv(:,:,:)    ! rlarv(0:nlarv,nx,ny)     
  REAL, ALLOCATABLE :: rmasslarv(:)     ! mass of larvae (0:nlarv)

  ! host infection state - No age dependence yet.
  ! 0=susceptible
  ! 1-ninfh-1=Exposed
  ! ninfh=Infectious 
  ! ninfh+1=Recovered-Immune1 (with parasites) [from v1.6 onwards]
  ! ninfh+2=Recovered-Immune2 (no parasites)   [from v1.8 onwards]
  REAL, ALLOCATABLE :: rhost(:,:,:,:) ! rhost(0:ninfh+2,nhost,nx,ny)   

! meteorological variables
  INTEGER(INT64), ALLOCATABLE  :: ndate(:)      ! ndate(nstep)         !          integer date
  REAL, ALLOCATABLE :: rtemp (:,:) ! rtemp(nstep,nx,ny)   ! (K)      T2m as a function of time and space
!  REAL, ALLOCATABLE :: rrh(:,:)    ! rrh(nstep,nx,ny)     ! (frac)   RH as a function of time and space
  REAL, ALLOCATABLE :: rrain(:,:)  ! rrain(nstep,nx,ny)   ! (mm/day) precip as a function of time and space
  REAL :: rrain_fillvalue, rtemp_fillvalue ! missing values in datasets
  REAL :: rsit_FillValue ! missing value in input rsit
  REAL :: rbednet_FillValue ! missing value in input rbednet
!----------------------------
! Non-gridded malaria arrays
!----------------------------
  REAL :: rpdfvect2host(0:nbitepdf) ! PDF of transmission probability 
                                    ! as a function of bite number
  REAL :: rbiteSwgt(0:nbitepdf)     ! Susceptibles bite distribution weighting
                                    ! as a function of bite number
  REAL :: rbiteEIwgt(0:nbitepdf)    ! EI bite distribution weighting
                                    ! as a function of bite number
  REAL :: rsumrbiteSwgt, rsumrbiteEIwgt ! sum of the weightings

! rate constants for larvae maturation, array for convenience.
  REAL, DIMENSION (4,2) :: rlarvmature

! derived variable
  INTEGER ::  nrun ! length of run in timesteps


!--------------------------------
! control variables and constants
!--------------------------------
!

!
! run control
!
  INTEGER :: iounit=7
  CHARACTER(LEN=500)  :: txt_outfile, nc_outfile="vectri.nc", climfile, precipfile="none"
  CHARACTER(LEN=500)  :: datafile="datafile",initfile="none",restartfile="init.grb"
  CHARACTER(LEN=500)  :: input
  CHARACTER(LEN=500)  :: output
  CHARACTER(LEN=500)  :: fgrib_rain        = 'pr.grb'
  CHARACTER(LEN=500)  :: fgrib_temperature = 't2.grb'
  CHARACTER(LEN=500)  :: fgrib_popdensity  = 'popdensity.grb'

  
! these...
  INTEGER, PARAMETER :: slen=100

  CHARACTER (len = slen) :: now,version
  CHARACTER (len = 1000) :: runcommand


!
! data structure for climate data, soil and hydrological data
!
  TYPE datafld
      INTEGER :: varid  ! netcdf identifier of field
      CHARACTER(LEN=slen) :: varname ! internal var name
      CHARACTER(LEN=slen) :: ncname ! nc field name
      REAL :: miss=-999
      REAL :: infil=-999
      REAL, ALLOCATABLE :: vals(:,:)
  END TYPE datafld

  TYPE(datafld),SAVE,DIMENSION(3):: soil= [ &
    &   datafld(-99,"clay","GLDAS_soilfraction_clay",-999), &
    &   datafld(-99,"silt","GLDAS_soilfraction_silt",-999), &
    &   datafld(-99,"sand","GLDAS_soilfraction_sand",-999)  ]

!  type(datafld) :: rain,temp
!  type(datafld) :: wperm, population
  

  
! Input variable name lists ! add here if you want to increase possible names
! possible standard names of precip/temp and coordinates from CMIP5, GRIB ECMWF and VECTRI dumps
! the first empty string is to allow the user to specify in namelis (LEAVE!)
  CHARACTER (LEN=slen) :: rain_names(8)=[character(len=slen) ::"","precipitation","precip","tp", &
                       & "pr","rain","prAdjust","PRECIPITATION"]  
  CHARACTER (LEN=slen) :: temp_names(7)=[character(len=slen) ::"","temperature","tas","t2m","tasAdjust","T2M","T2m"]

! standard population density and other data file names here
  CHARACTER (LEN=slen) :: pop_names(5)  =[character(len=slen) ::"","population","pop","density","number_of_people"]
  CHARACTER (LEN=slen) :: wperm_names(2)=[character(len=slen) :: "","wperm"]
  CHARACTER (LEN=slen) :: wurbn_names(2)=[character(len=slen) :: "","wurbn"]
  CHARACTER (LEN=slen) :: wpond_names(3)=[character(len=slen) ::"","pondfrac","sfcheadsubrt"]
  CHARACTER (LEN=slen) :: sit_names(2)=[character(len=slen) ::"","sit"]
  CHARACTER (LEN=slen) :: bednet_names(3)=[character(len=slen) ::"","llin","bednet"]

  CHARACTER (LEN=slen) :: lonnames(4)=[character(len=slen)::"lon","longitude","easting","X"]
  CHARACTER (LEN=slen) :: latnames(4)=[character(len=slen)::"lat","latitude","northing","Y"]
  CHARACTER (LEN=slen) :: time_units,lat_units,lon_units ! e.g. "days since 1990-11-25 00:00 UTC"
  CHARACTER (LEN=slen) :: time_long_name,lat_long_name,lon_long_name,rain_long_name,temp_long_name
  CHARACTER (LEN=slen) :: time_calendar,lat_axis,lon_axis,time_axis

  CHARACTER (LEN=4) :: time_name="time"
  CHARACTER (LEN=slen) :: lat_name,lon_name

! The following are defaulted to empty strings, but you can overwrite them in the namelist file
! If they are NOT empty the string value is used, otherwise the search array is used for "default" values.
  CHARACTER (LEN=slen) :: rain_name=" ",temp_name=" " ! keep these same as input
  CHARACTER (LEN=slen) :: pop_name=" ",wperm_name=" ",wpond_name=" ",wurbn_name=" ",sit_name=" ",bednet_name=" "
!  CHARACTER (LEN=slen) :: pop_name=" ",wperm_name=" ",wpond_name=" "

  
!  CHARACTER (LEN = *), PARAMETER :: lat_name = "latitude"
!  CHARACTER (LEN = *), PARAMETER :: lon_name = "longitude"
!  CHARACTER (LEN = *), PARAMETER :: time_name = "time"
  CHARACTER (LEN = *), PARAMETER :: units = "units"
!  CHARACTER (LEN = *), PARAMETER :: lat_units = "degrees_north"
!  CHARACTER (LEN = *), PARAMETER :: lon_units = "degrees_east"

! population density, and population related parameters
  REAL :: rpopdensity2010, rpopdensity_FillValue
  REAL, ALLOCATABLE :: rpopdensity(:,:) ! rpopdensity(nx,ny)

! wperm hydrology params
  REAL :: rwperm_FillValue

! water occupancy rates for LUC experiments
  REAL, ALLOCATABLE :: rwateroccupancy(:,:) ! rwateroccupancy(nx,ny)

! grib or netcdf
  INTEGER :: grib

  ! ensemble number
  INTEGER :: ens(1)=1
  
! region data
  REAL, ALLOCATABLE :: lons(:), lats(:) ! lon lat values
  ! diff coeffs, a/(1+a) where a=2D dt/dx**2 and 1-this
  REAL, ALLOCATABLE :: ralfa_x(:,:),ralfa_y(:,:)
  
  ! netcdf and input file indices, set to true by default, will turn off if no data found
  ! Can also override using namelist to prevent search of datafile for intervention
  LOGICAL :: lpoptime=.FALSE. ! indicates population is a function of time
  LOGICAL :: lsit=.TRUE.      ! SIT interventions
  LOGICAL :: lbednet=.TRUE.  ! LLIN/bednets (needs mods if not all LLIN)
  LOGICAL :: lspray=.TRUE.   ! IRS spraying 
  LOGICAL :: linterventions=.TRUE. ! any interventions active  (needed for netcdf4 output group)

  ! will put intervention in class structure (eventually same for all vars)
  INTEGER :: sit_init, sit_len, sit_nrel, sit_nvec
  REAL :: sit_ratio

  INTEGER :: ncid_clim, ncid_rain, ncid_data    ! file id
  INTEGER :: tempvarid_in,rainvarid_in,popvarid,pondvarid,urbnvarid
  INTEGER :: timedimid_in,latdimid_in,londimid_in   ! varid
  INTEGER :: sitVarID,bednetVarID

! 
! --------------------------
! output diagnostics control
! --------------------------
!  LOGICAL :: ltemp_latreverse=.false. ! reverse the lats?
!  LOGICAL :: lrain_latreverse=.false.

  INTEGER :: ndaydiag ! diagnostics every n days
  INTEGER :: ncidout  ! ncdf file id for output
  INTEGER :: grpid_vector, grpid_disease, grpid_hydro, grpid_input, grpid_interventions
  INTEGER :: LonDimID, LatDimID, timeDimId

! ncids and indices for 2d output fields
  INTEGER, PARAMETER :: ncvar_ps=2
  INTEGER :: ncvar_rain(ncvar_ps)       
  INTEGER :: ncvar_t2m(ncvar_ps)       
  INTEGER :: ncvar_soilinfil(ncvar_ps)       
  INTEGER :: ncvar_vector(ncvar_ps)    
  INTEGER :: ncvar_larvae(ncvar_ps)    
  INTEGER :: ncvar_lbiomass(ncvar_ps)   
  INTEGER :: ncvar_pr(ncvar_ps) 
  INTEGER :: ncvar_prd(ncvar_ps) 
  INTEGER :: ncvar_eir(ncvar_ps)       
  INTEGER :: ncvar_cases(ncvar_ps)       
  INTEGER :: ncvar_immunity(ncvar_ps)       
  INTEGER :: ncvar_cspr(ncvar_ps)       
  INTEGER :: ncvar_hbr(ncvar_ps)      
  INTEGER :: ncvar_egg(ncvar_ps)       
  INTEGER :: ncvar_emergence(ncvar_ps)       
  INTEGER :: ncvar_wpond(ncvar_ps)  
  INTEGER :: ncvar_wperm(ncvar_ps)       
  INTEGER :: ncvar_wurbn(ncvar_ps)       
  INTEGER :: ncvar_sitm(ncvar_ps)       
  INTEGER :: ncvar_sitf(ncvar_ps)       
  INTEGER :: ncvar_bednet(ncvar_ps)       
  INTEGER :: ncvar_vecthostratio(ncvar_ps)       
  INTEGER :: ncvar_popdensity(ncvar_ps)       
  INTEGER :: ndiag2d=0 ! number of diagnostics (defined in setup)
  REAL, ALLOCATABLE :: rdiag2d(:,:,:) ! nlon, nlat, ndiag2d 


! IDs for output variables
!  CHARACTER (len = *), PARAMETER :: temp_units = "celsius"
!  CHARACTER (len = *), PARAMETER :: rain_units = "mm/day"



END MODULE mo_vectri
