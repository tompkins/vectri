MODULE mo_namelist
!-------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins AM. 2011, ICTP
! tompkins@ictp.it
!
! namelists for input
!---------------------------------------------------------
  USE mo_constants
  USE mo_control
  USE mo_climate
  USE mo_vectri

  IMPLICIT NONE

! program control namelist
  NAMELIST /control/txt_outfile,nc_outfile,input,initfile,climfile,datafile,precipfile, &
& now,version,runcommand,ens,ivec

! constants namelist 
  NAMELIST /constants/neggmn,rlarv_tmin,rlarv_tmax, rbiocapacity, &
       & rlarv_eggtime, &
       & rlarv_pupaetime, &
       & rlarv_flushmin,  &
       & rlarv_flushtau, &
       & nlarv_scheme, &
       & rmasslarv_stage4, &
       & rbiocapacity, &
       & rlarvsurv, &
       & rwater_tempoffset, &
       & npud_scheme, &
       & wperm_default, &
       & rvect_diffusion, &
       & wpond_infil_clay, wpond_infil_silt, wpond_infil_sand, &
       & wpond_rate, &
       & wpond_evap, &
       & wpond_CN, &
       & wpond_min, wpond_max,  &
       & wpond_shapep2, &
       & wpond_ia, &
       & wpond_depthref,&
       & wpond_ref, &
       & wpond_ratio, &
       & wperm_ratio, &
       & wurbn_ratio, &
       & wurbn_tau, &
       & wurbn_sf, &
       & rbiteratio, &
       & rzoophilic_tau, &
       & rzoophilic_min, &
       & rbitehighrisk, &
       & rhostclear, &
       & rhostimmuneclear, &
       & rpthost2vect_I, rpthost2vect_R, rptvect2host, &
       & rtgono, dgono, rtsporo, dsporo, &
       & nsurvival_scheme, rhost_infect_init, rpop_death_rate, npop_option, &
       & rpopdensity_min, rtemperature_offset, rtemperature_trend, rrainfall_factor, &
       & rmigration,rbeta_indoor, &
       & rimmune_gain_eira,rimmune_loss_tau, &
       & rsit_breed,rsit_mortality, rbednet_tau,lsit, &
       & loutput_population, &    
& loutput_wperm, &    
& loutput_wpond, &     
& loutput_wurbn, &     
& loutput_rain, &          
& loutput_t2m, &           
& loutput_vector, &        
& loutput_larvae, &        
& loutput_lbiomass, &
& loutput_pr, & 
& loutput_prd, &           
& loutput_hbr, &           
& loutput_cspr, &          
& loutput_eir, &           
& loutput_cases, &           
& loutput_egg, &    
& loutput_emergence, &    
& loutput_immunity, &           
& loutput_vecthostratio, &
& loutput_sit, &
& loutput_bednet, &
& lverbose, &
& lgrib, &
& nloopspinup, nlenspinup, &
& rvecsurv, rvecsurv_min, &
& rain_name,temp_name,pop_name,wperm_name,wpond_name,aovi

  END MODULE mo_namelist

