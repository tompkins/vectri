MODULE mo_grib_tools

    USE mo_vectri
    USE mo_control
    USE mo_constants
    USE eccodes

    IMPLICIT NONE
    PRIVATE

    PUBLIC :: get_dims_grib
    PUBLIC :: open_output_grib
    PUBLIC :: read_slice_grib
    PUBLIC :: write_data_grib
    PUBLIC :: read_dims_grib
    PUBLIC :: write_restart_grib
    PUBLIC :: read_restart_grib
    PUBLIC :: read_data_grib

    PROCEDURE(), POINTER :: assert => assert_dbg

    ! file names for input and output (and parameter identifiers)
    INTEGER, PARAMETER :: rain_pids(1)        =    [228]
    INTEGER, PARAMETER :: temperature_pids(2) =     [51,167]


    INTEGER, PARAMETER :: cases_pid           =  261003   ! "mal_cases", "Malaria cases", "fraction"
    INTEGER, PARAMETER :: cspr_pid            =  261004   ! "mal_prot_rate", "Malaria circumsporozoite protein rate", "fraction"
    INTEGER, PARAMETER :: eir_pid             =  261005   ! "mal_innoc_rate", "Plasmodium falciparum entomological inoculation rate", "bites per day per person"
    INTEGER, PARAMETER :: hbr_pid             =  261006   ! "mal_hbite_rate", "Human bite rate by anopheles vectors", "bites per day per person"
    INTEGER, PARAMETER :: immunity_pid        =  261007   ! "mal_immun", "Malaria immunity", "fraction"
    INTEGER, PARAMETER :: pr_pid              =  261008   ! "mal_para_rate", "Falciparum parasite ratio", "fraction"
    INTEGER, PARAMETER :: prd_pid             =  261009   ! "mal_para_ratio", "Detectable falciparum parasite ratio (after day 10)", "fraction"
    INTEGER, PARAMETER :: vecthostratio_pid   =  261010   ! "mal_host_ratio", "Anopheles vector to host ratio", "fraction"
    INTEGER, PARAMETER :: vector_pid          =  261011   ! "mal_vect_dens", "Anopheles vector number", "density m^-2"
    INTEGER, PARAMETER :: waterfrac_pid       =  261012   ! "mal_hab_frac", "Fraction of malarial vector reproductive habitat", "fraction"

    INTEGER, PARAMETER :: popdensity_pids(1)  = [261013]  ! "pop_dens", "Population density", "density m^-2"]
    !TODO                                               ! "vectinfect", "Proportion of vectors infected", "fraction"
    INTEGER, PARAMETER :: rain_pid            =  212001   ! "", "Precipitation", "mm/day"
    INTEGER, PARAMETER :: temperature_pid     =  212002   ! "", "Temperature", "degrees C"
    INTEGER, PARAMETER :: larvae_pid          =  212008   ! "larvae", "Larvae number", "density m^-2"
    INTEGER, PARAMETER :: lbiomass_pid        =  212009   ! "lbiomass", "Larvae biomass", "mg m^-2"


!    INTEGER, PARAMETER :: popdensity_pids(1)  = [212010]  ! "", "Population density", "km^-2"]
!    INTEGER, PARAMETER :: pr_pid              =  212011   ! "pr", "Parasite ratio (proportion of hosts infected)", "fraction"
!    INTEGER, PARAMETER :: prd_pid             =  212012   ! "prd", "Detectable parasite ratio (after day 10)", "fraction"
!    INTEGER, PARAMETER :: vecthostratio_pid   =  212013   ! "vecthostratio", "Vector to host ratio", "fraction"
!    INTEGER, PARAMETER :: vector_pid          =  212014   ! "vector", "Vector number", "density m^-2"
!    INTEGER, PARAMETER :: waterfrac_pid       =  212015   ! "waterfrac", "Water coverage of grid cell", "fraction"
    !TODO                                               ! "vectinfect", "Proportion of vectors infected", "fraction"

    INTEGER, PARAMETER :: vectri_header_pid   = 212017  ! "", "Vectri restart header", ""
    INTEGER, PARAMETER :: vectri_rlarv_pid    = 212018  ! "", "Vectri restart larvae array", "m^-2"
    INTEGER, PARAMETER :: vectri_rvect_pid    = 212019  ! "", "Vectri restart vector array", "m^-2"
    INTEGER, PARAMETER :: vectri_rhost_pid    = 212020  ! "", "Vectri restart host array", "m^-2"

    TYPE :: GribField
        INTEGER :: fd
        INTEGER :: handle
        INTEGER :: paramId
        CHARACTER(LEN=20) :: shortName
        CHARACTER(LEN=200) :: name
        INTEGER :: Ni, Nj
        REAL :: grid(2)
        REAL :: area(4)
        INTEGER :: count
        CONTAINS
            PROCEDURE, PUBLIC :: open_as_input => gribfield_open_as_input
            PROCEDURE, PUBLIC :: open_as_output => gribfield_open_as_output
            PROCEDURE, PUBLIC :: open_as_restart => gribfield_open_as_restart
            PROCEDURE, PUBLIC :: next => gribfield_next
            PROCEDURE, PUBLIC :: close => gribfield_close
            PROCEDURE, PUBLIC :: latitudes => gribfield_latitudes
            PROCEDURE, PUBLIC :: longitudes => gribfield_longitudes
            PROCEDURE, PUBLIC :: values => gribfield_values
            PROCEDURE, PUBLIC :: header => gribfield_header
            PROCEDURE, PUBLIC :: write_other_field => gribfield_write_other_field
    END TYPE

    TYPE(GribField) :: gclimate(2)
    TYPE(GribField) :: gpopdensity


CONTAINS


FUNCTION EmptyGribField() RESULT(g)
    TYPE(GribField) :: g
    g%fd = 0
    g%handle = 0
    g%paramId = 0
    g%shortName = ''
    g%name = ''
    g%Ni = 0
    g%Nj = 0
    g%grid = 0.
    g%area = 0.
    g%count = 0
END FUNCTION


SUBROUTINE gribfield_latitudes(this, values)
    CLASS(GribField), INTENT(IN) :: this
    REAL, ALLOCATABLE, INTENT(INOUT) :: values(:)
    INTEGER :: i
    REAL :: first, increment, last

    CALL assert(ALLOCATED(values) .AND. SIZE(values) == this%Nj, 'latitude values allocation')

    first = this%area(1)
    increment = this%grid(2)
    DO i = 1, this%Nj
        values(i) = first - (i - 1) * increment
    END DO
    last = values(this%Nj)

    CALL assert(-90. <= last .AND. last <= first .AND. first <= 90., 'latitude range')
END SUBROUTINE


SUBROUTINE gribfield_longitudes(this, values)
    CLASS(GribField), INTENT(IN) :: this
    REAL, ALLOCATABLE, INTENT(INOUT) :: values(:)
    INTEGER :: i
    REAL :: first, increment, last

    CALL assert(ALLOCATED(values) .AND. SIZE(values) == this%Ni, 'longitude values allocation')

    first = this%area(2)
    increment = this%grid(1)
    DO i = 1, this%Ni
        values(i) = first + (i - 1) * increment
    END DO
    last = values(this%Ni)

    CALL assert(first <= last .AND. last - first < 360., 'longitude range')
END SUBROUTINE


SUBROUTINE gribfield_values(this, values)
    CLASS(GribField), INTENT(IN) :: this
    REAL, ALLOCATABLE, INTENT(INOUT) :: values(:,:)

    ! Note: this routine reorders the input; when the data structures are one-dimensional
    ! arrays, it can be read directly which is much more efficient
    REAL, ALLOCATABLE :: tmp(:)
    INTEGER :: n

    CALL assert(ALLOCATED(values), 'gribfield_values values allocated')
    CALL assert(SIZE(values) == this%Ni * this%Nj, 'gribfield_values values size mismatch)')

    CALL codes_get_size(this%handle, "values", n)
    CALL assert(SIZE(values) == n, 'gribfield_values codes_get_size("values") mismatch')

    ALLOCATE(tmp(n))
    CALL codes_get(this%handle, "values", tmp)
    values = RESHAPE(tmp, SHAPE=(/ this%Ni, this%Nj /))
    DEALLOCATE(tmp)
END SUBROUTINE


SUBROUTINE gribfield_header(this)
    CLASS(GribField), INTENT(INOUT) :: this
    CALL assert(this%handle /= 0, 'this%handle')

    CALL codes_get(this%handle, 'shortName', this%shortName)
    CALL codes_get(this%handle, 'name', this%name)
    CALL codes_get(this%handle, 'paramId', this%paramid)
    CALL assert(this%paramId > 0, 'paramId > 0')

    CALL codes_get(this%handle, 'Ni', this%Ni)
    CALL codes_get(this%handle, 'Nj', this%Nj)
    CALL assert(this%Ni > 0 .AND. this%Nj > 0, '(Ni, Nj) > 0')

    CALL codes_get(this%handle, 'latitudeOfFirstGridPointInDegrees',  this%area(1))
    CALL codes_get(this%handle, 'longitudeOfFirstGridPointInDegrees', this%area(2))
    CALL codes_get(this%handle, 'latitudeOfLastGridPointInDegrees',   this%area(3))
    CALL codes_get(this%handle, 'longitudeOfLastGridPointInDegrees',  this%area(4))
    CALL assert(this%area(1) >= this%area(3), 'area: North > South')
    !CALL assert(this%area(4) >= this%area(2), 'area: East > West')

    CALL codes_get(this%handle, 'iDirectionIncrementInDegrees', this%grid(1))
    CALL codes_get(this%handle, 'jDirectionIncrementInDegrees', this%grid(2))
    CALL assert(this%grid(1) >= 0. .AND. this%grid(2) >= 0., 'grid increments >= 0')
END SUBROUTINE


FUNCTION gribfield_next(this) RESULT (r)
    CLASS(GribField), INTENT(INOUT) :: this
    INTEGER :: iret
    LOGICAL :: r
    IF (this%handle /= 0) CALL codes_release(this%handle)
    CALL codes_grib_new_from_file(this%fd, this%handle, iret)
    r = iret /= CODES_END_OF_FILE
    CALL assert(.NOT. r .OR. iret == 0 .AND. this%handle /= 0, 'codes_grib_new_from_file')
END FUNCTION


SUBROUTINE gribfield_close(this)
    CLASS(GribField), INTENT(INOUT) :: this
    IF (this%handle /= 0) CALL codes_release(this%handle)
    this%handle = 0
    CALL codes_close_file(this%fd)
    this%fd = 0
END SUBROUTINE


SUBROUTINE gribfield_open_as_input(this, file, var, names, pids)
    CLASS(GribField), INTENT(INOUT) :: this

    CHARACTER(LEN=*), INTENT(IN) :: file
    CHARACTER(LEN=*), INTENT(IN) :: var
    CHARACTER(LEN=*), DIMENSION(:), INTENT(IN) :: names
    INTEGER,          DIMENSION(:), INTENT(IN) :: pids

    CHARACTER(LEN=20) :: shortName
    INTEGER :: i, Ni, Nj
    LOGICAL :: found

    ! open file and read messages to the end
    CALL codes_open_file(this%fd, file, 'r')
    CALL assert(this%next(), 'file "'//TRIM(file)//'": '//TRIM(var)//' GRIB not found')

    ! first GRIB message: get header (variable name/id and geometry), then
    ! next GRIB messages: confirm (Ni,Nj), increment count
    CALL this%header()

    found = .false.
    DO i = 1, SIZE(names)
        found = this%shortName .EQ. names(i)
        IF (found) EXIT
    END DO
    IF (.NOT. found) THEN
        DO i = 1, SIZE(pids)
            found = this%paramId .EQ. pids(i)
            IF (found) EXIT
        END DO
    END IF
    CALL assert(found, 'file "'//TRIM(file)//'": '//TRIM(var)//' name not found')
    WRITE(iounit,*) 'Found '//TRIM(var)//' variable called: ', TRIM(this%shortName), ' with paramId=', this%paramId

    this%count = 1
    DO WHILE (this%next())
        this%count = this%count + 1
        CALL codes_get(this%handle, 'paramId', i)
        CALL codes_get(this%handle, 'Ni', Ni)
        CALL codes_get(this%handle, 'Nj', Nj)
        CALL assert(Ni == this%Ni .AND. Nj == this%Nj .AND. i == this%paramId,&
                    'Input fields should have the same paramId and geometry (Ni, Nj)')
    END DO

    ! end reached, re-open the file to read messages one-by-one
    CALL this%close()
    CALL codes_open_file(this%fd, file, 'r')
    CALL assert(this%fd /= 0, 'file "'//TRIM(file)//'": GRIB codes_open_file (r)')
END SUBROUTINE


SUBROUTINE gribfield_open_as_output(this, file)
    CLASS(GribField), INTENT(INOUT) :: this
    CHARACTER(LEN=*), INTENT(IN) :: file

    this%fd = 0
    this%handle = 0
    CALL assert(LEN(file) > 0, 'file "'//TRIM(file)//'": invalid file name')
    CALL codes_open_file(this%fd, file, 'a')
    CALL assert(this%fd /= 0, 'file "'//TRIM(file)//'": GRIB codes_open_file (a)')
END SUBROUTINE


SUBROUTINE gribfield_open_as_restart(this, file)
    CLASS(GribField), INTENT(INOUT) :: this
    CHARACTER(LEN=*), INTENT(IN) :: file

    this%fd = 0
    this%handle = 0
    CALL assert(LEN(file) > 0, 'file "'//TRIM(file)//'": invalid file name')
    CALL codes_open_file(this%fd, file, 'r')
    CALL assert(this%fd /= 0, 'file "'//TRIM(file)//'": GRIB codes_open_file (r)')
END SUBROUTINE


SUBROUTINE gribfield_write_other_field(this, fd, paramid, values)
    CLASS(GribField), INTENT(INOUT) :: this
    INTEGER, INTENT(IN) :: fd, paramid
    REAL, INTENT(IN) :: values(:,:)
    INTEGER :: handle

    CALL assert(fd /= 0 .AND. paramid > 0, 'gribfield_write_other_field: requirements')
    CALL assert(this%Ni * this%Nj == SIZE(values, 1) * SIZE(values, 2), 'gribfield_write_other_field: values size')

    handle = 0
    CALL codes_clone(this%handle, handle)
    CALL assert(handle /= 0, 'gribfield_write_other_field: codes_clone')
    CALL codes_set(handle, 'edition', 2)
    CALL codes_set(handle, 'centre', 98)
    CALL codes_set(handle, 'paramId', paramid)
    CALL codes_set(handle, 'values', PACK(values, MASK=.TRUE.))
    CALL codes_write(handle, fd)
    CALL codes_release(handle)
END SUBROUTINE


SUBROUTINE get_dims_grib
    INTEGER :: i

    gclimate(:) = EmptyGribField()

    CALL gclimate(1)%open_as_input(fgrib_rain, 'rain', rainnames, rain_pids)
    CALL gclimate(2)%open_as_input(fgrib_temperature, 'temperature', tempnames, temperature_pids)

    DO i = 2, SIZE(gclimate)
        IF (gclimate(1)%Ni /= gclimate(i)%Ni .OR. &
            gclimate(1)%Nj /= gclimate(i)%Nj .OR. &
            gclimate(1)%count /= gclimate(i)%count) THEN
            WRITE(iounit, *) '%ERROR: climate fields should have the same geometry (Ni, Nj, count)'
            STOP 1
        END IF
    END DO

    ! Note: geometry is taken from first GRIB field
    nlon = gclimate(1)%Ni
    nlat = gclimate(1)%Nj
    nday = gclimate(1)%count

    rain_name      = TRIM(gclimate(1)%shortName)
    rain_long_name = TRIM(gclimate(1)%name)

    temp_name      = TRIM(gclimate(2)%shortName)
    temp_long_name = TRIM(gclimate(2)%name)

    ! compatibility (hardcoded, VarID logic compatible with open_output_cdf)
    londimid_in    = 1
    londimid       = 1
    lon_name       = "longitude"
    lon_long_name  = "longitude"
    lon_units      = "degrees_east"
    lon_axis       = "X"

    latdimid_in    = 2
    latdimid       = 2
    lat_name       = "latitude"
    lat_long_name  = "latitude"
    lat_units      = "degrees_north"
    lat_axis       = "Y"

    timedimid_in   = 3
    timedimid      = 3
    time_name      = "time"
    time_long_name = "time"
    time_calendar  = "standard"
    time_units     = "hours since 1900-01-01 00:00:00"
    time_axis      = ' '

    ncid_clim    = 65536
    rtemp_fillvalue  =   -32767.
    rrain_fillvalue  =   -32767.

    tempvarid_in = 3
    IF (loutput_population) tempvarid_in = 4
    rainvarid_in = tempvarid_in + 1
END SUBROUTINE


SUBROUTINE read_slice_grib

    CALL assert(gclimate(1)%next(), 'gclimate(1)%next()')
    CALL gclimate(1)%values(rrain)

    CALL assert(gclimate(2)%next(), 'gclimate(2)%next()')
    CALL gclimate(2)%values(rtemp)

END SUBROUTINE read_slice_grib


SUBROUTINE open_output_grib

    ! Open output file
    ncidout = 0
    CALL codes_open_file(ncidout, nc_outfile, 'w')
    CALL assert(ncidout /= 0, 'codes_open_file (w): '//nc_outfile)

    ! compatibility (set NetCDF variable indices) and allocate diagnostics
    CALL assert(.NOT. ALLOCATED(rdiag2d), "Diagnostic variables: deallocated")

    ndiag2d = 0
    CALL define_ncdf_compatible_output(loutput_population, ncvar_popdensity, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_rain, ncvar_rain, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_t2m, ncvar_t2m, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_waterfrac, ncvar_waterfrac, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_vector, ncvar_vector, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_larvae, ncvar_larvae, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_lbiomass, ncvar_lbiomass, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_pr, ncvar_pr, ndiag2d)
    !TODO CALL define_ncdf_compatible_output(?, ncvar_vectinfect, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_prd, ncvar_prd, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_hbr, ncvar_hbr, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_cspr, ncvar_cspr, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_eir, ncvar_eir, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_cases, ncvar_cases, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_egg, ncvar_egg, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_emergence, ncvar_emergence, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_immunity, ncvar_immunity, ndiag2d)
    CALL define_ncdf_compatible_output(loutput_vecthostratio, ncvar_vecthostratio, ndiag2d)

    CALL assert(nlon > 0 .AND. nlat > 0 .AND. ndiag2d > 0, 'Diagnostic variables: size')
    ALLOCATE(rdiag2d(nlon, nlat, ndiag2d))
    rdiag2d = 0.
    CALL assert(ALLOCATED(rdiag2d), 'Diagnostic variables: allocated')

    ncid_data = 0

    CONTAINS
    SUBROUTINE define_ncdf_compatible_output(loutput, ivarid, n)
        LOGICAL, INTENT(IN) :: loutput
        INTEGER, INTENT(INOUT) :: ivarid(2), n
        IF (.NOT. loutput) THEN
            ivarid = 0
            RETURN
        END IF
        n = n + 1
        ivarid(1) = n + 3  ! incl. lon, lat, time
        ivarid(2) = n
    END SUBROUTINE
END SUBROUTINE


SUBROUTINE write_data_grib
    INTEGER :: i

    DO i = 1, size(rdiag2d, 3)
        WHERE (rpopdensity(:,:) < 0.) rdiag2d(:,:,i) = 0.
    END DO

    CALL assert(ncidout /= 0, 'Output file open: '//nc_outfile)

    IF (loutput_rain)          CALL write_field(rain_pid,        rrain)
    IF (loutput_t2m)           CALL write_field(temperature_pid, rtemp)
    IF (loutput_waterfrac)     CALL write_field(waterfrac_pid,   rwaterfrac)

    IF (loutput_vector)        CALL write_field(vector_pid,        rdiag2d(:,:,ncvar_vector(2)))
    IF (loutput_larvae)        CALL write_field(larvae_pid,        rdiag2d(:,:,ncvar_larvae(2)))
    IF (loutput_lbiomass)      CALL write_field(lbiomass_pid,      rdiag2d(:,:,ncvar_lbiomass(2)))
    IF (loutput_pr)            CALL write_field(pr_pid,            rdiag2d(:,:,ncvar_pr(2)))
    !                          CALL write_field(FIXME?,            rdiag2d(:,:,ncvar_vectinfect(2)))
    IF (loutput_prd)           CALL write_field(prd_pid,           rdiag2d(:,:,ncvar_prd(2)))
    IF (loutput_hbr)           CALL write_field(hbr_pid,           rdiag2d(:,:,ncvar_hbr(2)))
    IF (loutput_cspr)          CALL write_field(cspr_pid,          rdiag2d(:,:,ncvar_cspr(2)))
    IF (loutput_eir)           CALL write_field(eir_pid,           rdiag2d(:,:,ncvar_eir(2)))
    IF (loutput_cases)         CALL write_field(cases_pid,         rdiag2d(:,:,ncvar_cases(2)))
    IF (loutput_egg)           CALL write_field(egg_pid,           rdiag2d(:,:,ncvar_egg(2)))
    IF (loutput_emergence)     CALL write_field(emergence_pid,     rdiag2d(:,:,ncvar_emergence(2)))
    IF (loutput_immunity)      CALL write_field(immunity_pid,      rdiag2d(:,:,ncvar_immunity(2)))
    IF (loutput_vecthostratio) CALL write_field(vecthostratio_pid, rdiag2d(:,:,ncvar_vecthostratio(2)))

CONTAINS
    SUBROUTINE write_field(paramid, values)
        INTEGER, INTENT(IN) :: paramid
        REAL, INTENT(IN) :: values(:,:)
        ! Note: first GRIB field defines metadata
        CALL gclimate(1)%write_other_field(ncidout, paramid, values)
    END SUBROUTINE
END SUBROUTINE


SUBROUTINE read_dims_grib
    ! Note: first GRIB field defines geometry
    ! this could call codes_grib_get_data when the data structures are one-dimensional,
    ! creating these fields can more efficient
    CALL gclimate(1)%latitudes(lats)
    CALL gclimate(1)%longitudes(lons)

    ! compatibility
    CALL assert(ALLOCATED(ndate), 'ALLOCATED(ndate)')
    ndate = 0
    
END SUBROUTINE


SUBROUTINE write_restart_grib
    INTEGER :: i, j, fd, handle
    REAL :: header(4)

    ! Dump restart file (header, rvect, rhost, rlarv, rwaterfrac)
    CALL assert(gclimate(1)%handle /= 0)
    CALL codes_open_file(fd, restartfile, 'w')
    CALL codes_clone(gclimate(1)%handle, handle)
    CALL codes_set(handle, 'paramId', vectri_header_pid)
    CALL codes_set(handle, 'edition', 2)
    CALL codes_set(handle, 'gridType', 'unstructured_grid')
    CALL codes_set(handle, 'packingType', 'grid_ieee')
    header(1) = nhost
    header(2) = ninfh
    header(3) = ninfv
    header(4) = nlarv
    CALL codes_set(handle, 'values', header)
    CALL codes_write(handle, fd)
    CALL codes_release(handle)

    DO j = 0, ninfv
        CALL gclimate(1)%write_other_field(fd, vectri_rvect_pid, rvect(j, :, :))
    END DO
    
    DO i = 0, ninfh + 1
        DO j = 1, nhost
            CALL gclimate(1)%write_other_field(fd, vectri_rhost_pid, rhost(i, j, :, :))
        END DO
    END DO

    DO i = 0, nlarv
        CALL gclimate(1)%write_other_field(fd, vectri_rlarv_pid, rlarv(i, :, :))
    END DO

    CALL gclimate(1)%write_other_field(fd, waterfrac_pid, rwaterfrac)

    CALL codes_close_file(fd)

    ! Close input/output files
    DO i = 1, SIZE(gclimate)
        CALL gclimate(i)%close()
    END DO
END SUBROUTINE


SUBROUTINE read_restart_grib

    INTEGER :: i, j, header_size
    REAL, ALLOCATABLE :: header(:), tmp(:,:)
    TYPE(GribField) :: grestart

    CALL assert(nlon > 0 .AND. &
                nlat > 0 .AND. &
                ALLOCATED(rvect) .AND. &
                ALLOCATED(rhost) .AND. &
                ALLOCATED(rlarv) .AND. &
                ALLOCATED(rwaterfrac), &
                'Restart: arrays allocated')
    ALLOCATE(header(4))
    ALLOCATE(tmp(nlon, nlat))

    ! Read restart file (header, rvect, rhost, rlarv, rwaterfrac)
    CALL grestart%open_as_restart(initfile)
    CALL assert(grestart%next(), 'Restart (header): grestart%next()')

    CALL codes_get_size(grestart%handle, 'values', header_size)
    CALL assert(SIZE(header) == header_size)

    CALL codes_get(grestart%handle, 'values', header)
    CALL assert(nhost == header(1) .AND. &
                ninfh == header(2) .AND. &
                ninfv == header(3) .AND. &
                nlarv == header(4), &
                TRIM(ADJUSTL(initfile))//': header parameterisation')

    DO j = 0, ninfv
        CALL restart_next_values('Restart (rvect)', vectri_rvect_pid, tmp)
        rvect(j, :, :) = tmp
    END DO

    DO i = 0, ninfh + 1
        DO j = 1, nhost
            CALL restart_next_values('Restart (rhost)', vectri_rhost_pid, tmp)
            rhost(i, j, :, :) = tmp
        END DO
    END DO

    DO i = 0, nlarv
        CALL restart_next_values('Restart (rlarv)', vectri_rlarv_pid, tmp)
        rlarv(i, :, :) = tmp
    END DO

    CALL restart_next_values('Restart (rwaterfrac)', waterfrac_pid, rwaterfrac)

    CALL grestart%close()

    CONTAINS
    SUBROUTINE restart_next_values(message, paramId, tmp)
        CHARACTER(LEN=*), INTENT(IN) :: message
        INTEGER, INTENT(IN) :: paramId
        REAL, ALLOCATABLE :: tmp(:,:)
        CALL assert(grestart%next(), message//': grestart%next()')
        CALL grestart%header()
        CALL assert(grestart%Ni == gclimate(1)%Ni, message//': grestart%Ni == gclimate(1)%Ni')
        CALL assert(grestart%Nj == gclimate(1)%Nj, message//': grestart%Nj == gclimate(1)%Nj')
        CALL assert(grestart%paramId == paramId, message//': vectri_rlarv_pid')
        CALL grestart%values(tmp)
    END SUBROUTINE
END SUBROUTINE


SUBROUTINE assert_prd(condition, message)
    LOGICAL, INTENT(IN) :: condition
    CHARACTER(LEN=*), INTENT(IN) :: message
    ! Do nothing
END SUBROUTINE


SUBROUTINE assert_dbg(condition, message)
    LOGICAL, INTENT(IN) :: condition
    CHARACTER(LEN=*), INTENT(IN) :: message
    IF (.NOT. condition) THEN
        WRITE(iounit, *) '%ERROR: assertion failed: '//message
        STOP 1
    END IF
END SUBROUTINE


SUBROUTINE read_data_grib

    ! read population data
    gpopdensity = EmptyGribField()
    CALL gpopdensity%open_as_input(fgrib_popdensity, 'Population density', popnames, popdensity_pids)
    pop_name = TRIM(gpopdensity%shortName)

    IF (gpopdensity%Ni /= nlon .OR. gpopdensity%Nj /= nlat .OR. gpopdensity%count /= 1) THEN
        WRITE(iounit, *) '%ERROR: population density field should have the same geometry (Ni, Nj, count) as the climate fields'
        STOP 1
    END IF

    ! Fixed single timeslice
    ! If pop is function of time, then best to move to clim file
    IF (kpop_option == 1) THEN
        CALL assert(allocated(rpopdensity), 'allocated(rpopdensity)')
        CALL assert(gpopdensity%next(), 'gpopdensity%next()')
        CALL gpopdensity%values(rpopdensity)
    ENDIF

    ! wperm data here
    !TODO

    ! LUC data
    !TODO (rwateroccupancy?)

    ! scale factors and processing!
    ! * all sparsely populated point set to missing
    ! * all negative water scal fac set to zero
    WHERE (rpopdensity < 0.)              rpopdensity = rfillValue
    ! from v1.9 all data needs to be in SI units, no assumption of scaling
    ! WHERE (rpopdensity /= rfillValue)     rpopdensity = rpopdensity * 1.e-6
    WHERE (rwateroccupancy < 0.)          rwateroccupancy = 0.
END SUBROUTINE


END MODULE

