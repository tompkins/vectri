MODULE mo_ncdf_tools

  USE netcdf
  USE mo_control
  USE mo_constants
  USE mo_vectri
  USE mo_climate

  IMPLICIT NONE

!--------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins AM. 2011, ICTP
! updated 01/2019
! tompkins@ictp.it
!
! tools for ncdf input/output
!---------------------------------------------------------

CONTAINS

SUBROUTINE get_dims_cdf
!--------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins AM. Aug 2012, ICTP
! tompkins@ictp.it
!
! open and read the climate netcdf files... added to V1.3
!
!---------------------------------------------------------
 
    ! local variables
    INTEGER :: i,istatus=0

    !-----------------------------------------------------------------------------
    ! climate data
    ! lat/lon and times now read from input file 
    !-----------------------------------------------------------------------------
    WRITE(iounit,*)'%I: opening climate file ',TRIM(climfile)

    CALL check(NF90_OPEN(path=climfile,mode=nf90_nowrite,ncid=ncid_clim))
    !------------------------
    ! get the time dimensions
    !------------------------
    time_name="time"
    CALL check(NF90_INQ_DIMID(ncid_clim,time_name, timeDimID_in))
    CALL check(NF90_INQUIRE_DIMENSION(ncid_clim, timeDimID_in, len = nday))
    ! get time metadata 
    istatus=NF90_GET_ATT(ncid_clim, timeDimId_in, "units",time_units)
    istatus=NF90_GET_ATT(ncid_clim, timeDimId_in, "long_name",time_long_name)
    istatus=NF90_GET_ATT(ncid_clim, timeDimId_in, "calendar",time_calendar)
    istatus=NF90_GET_ATT(ncid_clim, londimId_in, "axis",time_axis)

    ! get lat/lon dim and metadata
    DO i=1,SIZE(latnames)
      istatus=NF90_INQ_DIMID(ncid_clim,TRIM(latnames(i)), LatDimID_in)
      lat_name=latnames(i)
      IF(istatus==nf90_noerr) EXIT
    ENDDO
    IF (istatus/=nf90_noerr) STOP 'lat name not found'
    CALL check(NF90_INQUIRE_DIMENSION(ncid_clim, LatDimID_in, len = nlat))

    ! get lat metadata 
    istatus=NF90_GET_ATT(ncid_clim, latdimId_in, "units",lat_units)
    istatus=NF90_GET_ATT(ncid_clim, latdimId_in, "long_name",lat_long_name)
    istatus=NF90_GET_ATT(ncid_clim, latdimId_in, "axis",lat_axis)

    DO i=1,SIZE(lonnames)
      istatus=NF90_INQ_DIMID(ncid_clim,TRIM(lonnames(i)), LonDimID_in)
      lon_name=lonnames(i)
      IF(istatus==nf90_noerr)EXIT
    ENDDO
    IF (istatus/=nf90_noerr)STOP 'lon name not found in clim file'
    CALL check(NF90_INQUIRE_DIMENSION(ncid_clim, LonDimID_in, len=nlon))
    istatus=NF90_GET_ATT(ncid_clim, londimId_in, "units",lon_units)
    istatus=NF90_GET_ATT(ncid_clim, londimId_in, "long_name",lon_long_name)
    istatus=NF90_GET_ATT(ncid_clim, londimId_in, "axis",lon_axis)

    ! if separate rain file specified then open, else set rain ncid to climfile
    IF (TRIM(precipfile)=='none') THEN
        ncid_rain=ncid_clim
    ELSE
        WRITE(iounit,*)'%I: opening separate rain file ',TRIM(precipfile)
        CALL check(NF90_OPEN(path=precipfile,mode=nf90_nowrite,ncid=ncid_rain))
    ENDIF

    !---------------------------------------------
    ! get metadata rainfall, check standard names
    !---------------------------------------------
    rain_names(1)=rain_name ! This just passes an empty string if not defined in namelist
    DO i=1,SIZE(rain_names)
      istatus=NF90_INQ_VARID(ncid_rain, TRIM(rain_names(i)), rainVarId_in)
      rain_name=rain_names(i)
      IF(istatus==nf90_noerr)EXIT
    ENDDO
    IF (istatus /= nf90_noerr) THEN
      WRITE(IOUNIT,*)'%E: ERROR: rain name not found, check clim/precip file'
      STOP
    ENDIF
    WRITE(iounit,*) '%I: Found rain variable called: ',TRIM(rain_name)

    ! if attribute not there then default to fillvalue
    istatus=NF90_GET_ATT(ncid_rain, rainVarId_in, "_FillValue",rrain_FillValue)
    IF (istatus /= nf90_noerr) rrain_FillValue=rfillvalue
    istatus=NF90_GET_ATT(ncid_rain, rainVarId_in, "long_name",rain_long_name)

    !---------------------------------------------
    ! get metadata temperature, check standard names
    !---------------------------------------------
    temp_names(1)=temp_name 
    DO i=1,SIZE(temp_names)
      istatus=NF90_INQ_VARID(ncid_clim, TRIM(temp_names(i)), tempVarId_in)
      temp_name=temp_names(i)
      IF(istatus==nf90_noerr)EXIT
    ENDDO
    IF (istatus/=nf90_noerr) THEN
      WRITE(IOUNIT,*)'%E: ERROR: temperature name not found, check clim file'
      STOP
    ENDIF
    WRITE(iounit,*) '%I: Found temperature variable called: ',TRIM(temp_name)
    ! if attribute not there then default to fillvalue
    istatus=NF90_GET_ATT(ncid_clim, tempVarId_in, "_FillValue", rtemp_FillValue)
    IF (istatus /= nf90_noerr) rtemp_FillValue=rfillvalue
    istatus=NF90_GET_ATT(ncid_clim, tempVarId_in, "long_name",temp_long_name)
    WRITE(iounit,*) '%I: Input dimensions sizes read'
    
END SUBROUTINE get_dims_cdf

SUBROUTINE open_output_cdf
!--------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins AM. aug 2012, ICTP
! tompkins@ictp.it
!
! opens the output files - added to v1.3
!
! updates in v1.5 Jan 2019 to use metadata from climate file
!---------------------------------------------------------

  ! netcdf vars - only needed locally
  INTEGER :: latvarid, lonvarid, timevarid

  !---------------------------
  ! OUTPUT NETCDF FILE
  !---------------------------
  CALL check(NF90_CREATE(path = nc_outfile, cmode=NF90_NETCDF4, ncid = ncidout))

  ! define the dimensions
  CALL check(NF90_DEF_DIM(ncidout, TRIM(lon_name), nlon, LonDimID))
  CALL check(NF90_DEF_DIM(ncidout, TRIM(lat_name), nlat, LatDimID))
  CALL check(NF90_DEF_DIM(ncidout, TRIM(time_name), NF90_UNLIMITED, timeDimID))

  ! Define the coordinate variables. They will hold the coordinate
  ! information, that is, the latitudes and longitudes. A varid is
  ! returned for each.
  CALL check(NF90_DEF_VAR(ncidout, lon_name, NF90_REAL, londimid, lonvarid) )
  CALL check(NF90_DEF_VAR(ncidout, lat_name, NF90_REAL, latdimid, latvarid) )
  CALL check(NF90_DEF_VAR(ncidout, time_name, NF90_DOUBLE, timedimid, timevarid) )

  ! Assign units attributes to coordinate var data. This attaches a
  ! text attribute to each of the coordinate variables, containing the
  ! units.
  CALL check( nf90_put_att(ncidout, latvarid, units, lat_units) )
  CALL check( nf90_put_att(ncidout, lonvarid, units, lon_units) )
  CALL check( nf90_put_att(ncidout, timevarid, units, time_units) )

  CALL check( nf90_put_att(ncidout, latvarid, "long_name", lat_long_name) )
  CALL check( nf90_put_att(ncidout, lonvarid, "long_name", lat_long_name) )
  CALL check( nf90_put_att(ncidout, timevarid, "long_name", time_long_name) )

  CALL check( nf90_put_att(ncidout, timevarid, "calendar", time_calendar) )
  CALL check( nf90_put_att(ncidout, lonvarid, "axis", TRIM(lon_axis)) )
  CALL check( nf90_put_att(ncidout, latvarid, "axis", TRIM(lat_axis)) )
  CALL check( nf90_put_att(ncidout, timevarid, "axis", TRIM(time_axis)) )

  ! define global attributes
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "origin","Adrian Tompkins, ICTP, Strada Costiera 11, Trieste, Italy"))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "url","www.ictp.it/~tompkins/vectri"))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "email","tompkins@ictp.it"))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "license","free for research educational purposes, no 3rd party distribution"))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "date", now))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "version", version))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "command", runcommand))

  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "climfile", TRIM(climfile)))
  !CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "tempfile", TRIM(tempfile)))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "ivec", ivec))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "neggmn", neggmn))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarv_tmin",rlarv_tmin))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarv_tmax",rlarv_tmax))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarv_eggtime",rlarv_eggtime))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarv_pupaetime",rlarv_pupaetime))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarv_flushmin",rlarv_flushmin))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarv_flushtau",rlarv_flushtau))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarv_ermert",rlarv_ermert))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarv_jepson",rlarv_jepson))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarv_bayoh",rlarv_bayoh))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rbeta_indoor",rbeta_indoor))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rmasslarv_stage4",rmasslarv_stage4))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rbiocapacity",rbiocapacity))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rlarvsurv",rlarvsurv))

  ! Intervention block
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rsit_breed",rsit_breed))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rsit_mortality",rsit_mortality))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rbednet_tau",rbednet_tau))

  ! output depends on hydrology version
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wperm_default", wperm_default))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "npud_scheme", npud_scheme))

  SELECT CASE(npud_scheme)
  CASE(1)
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_rate", wpond_rate))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_evap", wpond_evap))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_max", wpond_max))
  CASE(2)
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_rate", wpond_rate))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_CN", wpond_CN))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_min", wpond_min))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_max", wpond_max))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_evap", wpond_evap))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_infil_clay", wpond_infil_clay))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_infil_sand", wpond_infil_sand))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_infil_silt", wpond_infil_silt))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_shapep2", wpond_shapep2))
  END SELECT

  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rwater_tempoffset",rwater_tempoffset ))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wpond_ratio",wpond_ratio ))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wperm_ratio",wperm_ratio ))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wurbn_ratio",wurbn_ratio ))

  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wurbn_tau",wurbn_tau ))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "wurbn_sf",wurbn_sf ))
  
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rbiteratio",rbiteratio))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rbitehighrisk",rbitehighrisk))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rvect_diffusion",rvect_diffusion))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rzoophilic_tau",rzoophilic_tau))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rzoophilic_min",rzoophilic_min))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rhostclear",rhostclear))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rhostimmuneclear",rhostimmuneclear))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rpthost2vect_I",rpthost2vect_I))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rpthost2vect_R",rpthost2vect_R))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rptvect2host",rptvect2host))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rtgono",rtgono))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "dgono",dgono))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rtsporo",rtsporo))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "dsporo",dsporo))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "nlarv_scheme",nlarv_scheme))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rvecsurv",rvecsurv))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rvecsurv_min",rvecsurv_min))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "nsurvival_scheme",nsurvival_scheme))
!  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rtlarsurvmax",rtlarsurvmax))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rmar1",rmar1))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rmar2",rmar2))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rtvecsurvmin",rtvecsurvmin))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rtvecsurvmax",rtvecsurvmax))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rhost_infectd",rhost_infectd))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rhost_detectd",rhost_detectd))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rimmune_gain_eira",rimmune_gain_eira))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rimmune_loss_tau",rimmune_loss_tau))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rpop_death_rate",rpop_death_rate))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rpopdensity_min",rpopdensity_min))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "nloopspinup",nloopspinup))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "nlenspinup",nlenspinup))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "dt",dt))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "nnumeric",nnumeric))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rtemperature_offset",rtemperature_offset))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rtemperature_trend",rtemperature_trend))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rrainfall_factor",rrainfall_factor))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rmigration",rmigration))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rhost_infect_init",rhost_infect_init))

  IF (lsit) THEN
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "sit_init",sit_init))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "sit_nrel",sit_nrel))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "sit_len",sit_len))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "sit_ratio",sit_ratio))
    CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "sit_nvec",sit_nvec))
  ENDIF

  !---------------------------------------------
  ! *** define the 3d structures
  ! routine also sets up the diagnostics indices
  !---------------------------------------------
  !                             index      short name title       units

  ! from v2.0 in groups:
  !
  ! vector - all info regarding vector (vec and larvae density, hbr etc)
  ! disease - all info pertaining to disease (Prd, cases, EIR etc)
  ! interventions - all info on interventions
  ! hydro - all data regarding hydrology and soil types
  ! input - all in driving data, copied to output for convenience. 
  CALL check(NF90_DEF_GRP(ncidout,"vector", grpid_vector))
  CALL check(NF90_DEF_GRP(ncidout,"disease", grpid_disease))
  IF (linterventions) CALL check(NF90_DEF_GRP(ncidout,"interventions", grpid_interventions))
  CALL check(NF90_DEF_GRP(ncidout,"hydrology", grpid_hydro))
  CALL check(NF90_DEF_GRP(ncidout,"input", grpid_input))
  
  ndiag2d=0

  !------- INPUT GRP
  
  IF(loutput_population) THEN
    IF (lpoptime) THEN
      CALL define_output_cdf(ncvar_popdensity,TRIM(pop_name), &
  & "population density","km^-2",  ndiag2d, (/LonDimId, LatDimID, timeDimID/),rfillvalue,grpid=grpid_input)
    ELSE
      CALL define_output_cdf(ncvar_popdensity,TRIM(pop_name), &
  & "population density","km^-2",  ndiag2d, (/LonDimId, LatDimID/),rfillvalue,grpid=grpid_input)
    ENDIF
  ENDIF

  IF(loutput_wperm) THEN
      CALL define_output_cdf(ncvar_wperm,TRIM(wperm_name), &
  & "Permanent breeding site fraction","",  ndiag2d, (/LonDimId, LatDimID/),rfillvalue,grpid=grpid_hydro)
  ENDIF 
 
  IF(loutput_wurbn) THEN
      CALL define_output_cdf(ncvar_wurbn,TRIM(wurbn_name), &
  & "Urban breeding site fraction","",  ndiag2d, (/LonDimId, LatDimID/),rfillvalue,grpid=grpid_hydro)
  ENDIF 

  IF(loutput_wpond) &
  & CALL define_output_cdf(ncvar_wpond,"wpond" , &
  & "fraction coverage temporary ponds","fraction", ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_hydro)
  
  IF(loutput_soilinfil) &
  & CALL define_output_cdf(ncvar_soilinfil,"infiltration" , &
  & "Infiltration Rate","mm day^-1", ndiag2d, (/ LonDimId, LatDimID /),rfillvalue,grpid=grpid_hydro)

  IF(loutput_rain) &
  & CALL define_output_cdf(ncvar_rain,TRIM(rain_name),rain_long_name,"mm day^-1", ndiag2d, &
  & (/ LonDimId, LatDimID, timeDimID /),rrain_fillvalue,grpid=grpid_input )

  IF(loutput_t2m) &
  & CALL define_output_cdf(ncvar_t2m,TRIM(temp_name),temp_long_name,"degrees C", ndiag2d, &
  & (/ LonDimId, LatDimID, timeDimID /),rtemp_fillvalue,grpid=grpid_input )


  !------- VECTOR GRP
  
  IF(loutput_vector) &
  & CALL define_output_cdf(ncvar_vector,"vector"  ,TRIM(vector_name)//" vector density","m^-2", ndiag2d,(/LonDimId, LatDimID, timeDimID/),rfillvalue,grpid=grpid_vector)

  IF(loutput_larvae) &
  & CALL define_output_cdf(ncvar_larvae,"larvae"  ,TRIM(vector_name)//" larvae Density","m^-2", ndiag2d, (/LonDimId, LatDimID, timeDimID/),rfillvalue,grpid=grpid_vector)

  IF(loutput_lbiomass) &
  & CALL define_output_cdf(ncvar_lbiomass,"lbiomass"  , &
  & TRIM(vector_name)//" larvae biomass","mg m^-2",  ndiag2d, (/LonDimId, LatDimID, timeDimID/),rfillvalue, grpid=grpid_vector)

  IF(loutput_egg) & 
  & CALL define_output_cdf(ncvar_egg,"eggs" , &
  &  "Density of new eggs laid per timestep (day)","m^-2 day^-1",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_vector)

  IF(loutput_emergence) & 
  & CALL define_output_cdf(ncvar_emergence,"emergence" , &
  &  "Emergence rate of new vectors per timestep (day)","m^-2 day^-1",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_vector)


  !------ DISEASE GRP
  
  IF(loutput_pr) &
  & CALL define_output_cdf(ncvar_pr,"PR"  , &
  & TRIM(disease_name)//" Parasite Ratio - Proportion of hosts infected","fraction",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_disease)
!  CALL define_output_cdf(ncvar_vectinfect,"vectinfect"  , &
!   & "Proportion of vectors infected","frac",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /) )

  IF(loutput_prd) &
  & CALL define_output_cdf(ncvar_prd,"PRd", &
  & TRIM(disease_name)//"detectable parasite ratio", &
  & "frac",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_disease)

  IF(loutput_hbr) &
  & CALL define_output_cdf(ncvar_hbr,"hbr"  ,TRIM(vector_name)//" human bite rate", &
  & "bites per day per person",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_vector)

  IF(loutput_cspr) & 
  & CALL define_output_cdf(ncvar_cspr,"cspr" , &
  &  "circumsporozoite protein rate - proportion of infective vectors","frac",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_disease)

  IF(loutput_eir) & 
  & CALL define_output_cdf(ncvar_eir,"eir" , &
  &  TRIM(disease_name)//" Entomological Inoculation Rate","infective bites per day per person",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_disease)

  IF(loutput_cases) & 
  & CALL define_output_cdf(ncvar_cases,"cases" , &
  &  "Number of new cases "//TRIM(disease_name),"fraction",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_disease)

  IF(loutput_immunity) & 
  & CALL define_output_cdf(ncvar_immunity,"immunity" , &
  &  "Immune propulation "//TRIM(disease_name),"fraction",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_disease)

  IF(loutput_vecthostratio) & 
  & CALL define_output_cdf(ncvar_vecthostratio,"vecthostratio"  , &
  & TRIM(vector_name)//" - vector to host ratio","frac",  ndiag2d, (/ LonDimId, LatDimID, timeDimID /),rfillvalue,grpid=grpid_vector)

  IF(loutput_sit.and.lsit) THEN
    CALL define_output_cdf(ncvar_sitm,"sit_males"  , &
    & TRIM(vector_name)//" SIT males","m^-2",  ndiag2d, (/LonDimId, LatDimID, timeDimID/),rfillvalue, grpid=grpid_interventions)
    CALL define_output_cdf(ncvar_sitf,"sit_females"  , &
    & TRIM(vector_name)//" SIT females","m^-2",  ndiag2d, (/LonDimId, LatDimID, timeDimID/),rfillvalue, grpid=grpid_interventions)
  ENDIF
  
  ! End define mode.
  CALL check(NF90_ENDDEF(ncidout))

  ! Write the coordinate variable data. This will put the latitudes
  ! and longitudes of our data grid into the netCDF file.
  CALL check( NF90_PUT_VAR(ncidout, latvarid, lats) )
  CALL check( NF90_PUT_VAR(ncidout, lonvarid, lons) )
  CALL check( NF90_PUT_VAR(ncidout, timevarid, ndate) )

  ! NOTE we rescale to non-SI units for NETCDF output for ease of use.
  IF (loutput_population .and. .not. lpoptime) &
  & CALL check( NF90_PUT_VAR(grpid_input, ncvar_popdensity(1), rpopdensity ))

  IF (loutput_wperm) &
  & CALL check( NF90_PUT_VAR(grpid_hydro, ncvar_wperm(1), rwaterperm ))

  IF (loutput_wurbn) &
  & CALL check( NF90_PUT_VAR(grpid_hydro, ncvar_wurbn(1), rwaterurbn*wurbn_ratio ))

  IF (loutput_soilinfil) &
  & CALL check( NF90_PUT_VAR(grpid_hydro, ncvar_soilinfil(1), rsoilinfil ))

  !------------------
  ! Set up the arrays
  !------------------
  ! at end of list ndiag contains the number of diagnostics... so we can allocate the diags array
  ! NOTE: ***NEVER*** use rdiag in any calculations of cross variables as the user can switch these
  !       off.  rdiag is for diagnostic storage ONLY! Any other use is unsafe.
  ALLOCATE(rdiag2d(nlon,nlat,ndiag2d))
  rdiag2d=rfillValue

  RETURN
END SUBROUTINE open_output_cdf

SUBROUTINE check(status)
  ! check for errors on netcdf functions
  INTEGER, INTENT(in) :: status
  IF (status /= nf90_noerr) THEN
    WRITE(IOUNIT,*)TRIM(NF90_STRERROR(status))
    STOP 'Bad NETCDF status'
  END IF
END SUBROUTINE check

SUBROUTINE define_output_cdf(ivarid,name,title,iunits,ndiag,idarray,fillvalue,grpid)
  ! 
  ! routine to define cdf fields in output
  !
  INTEGER, INTENT(in) :: idarray(:) ! flexible vector to set coordinate dimensions
  INTEGER, OPTIONAL, INTENT(in) :: grpid 
  INTEGER, INTENT(inout) :: ivarid(2), ndiag

  INTEGER :: id
  REAL, INTENT(in) :: fillvalue
  CHARACTER (len=*), INTENT(IN) :: name, title, iunits

  IF (PRESENT(grpid)) THEN
     id=grpid
  ELSE
     id=ncidout
  ENDIF
  
  CALL check(NF90_DEF_VAR(id, name, nf90_FLOAT, idarray , iVarID(1)))
  CALL check(NF90_PUT_ATT(id, iVarID(1), "long_name", title) )
  CALL check(NF90_PUT_ATT(id, iVarID(1), "units", iunits) )
  CALL check(NF90_PUT_ATT(id, iVarID(1), "_FillValue", fillvalue) )

  ndiag=ndiag+1   ! increase the diagnostic counter
  iVarid(2)=ndiag
END SUBROUTINE define_output_cdf

!
! routine to read dimension data from input file in cdf mode
!
SUBROUTINE read_dims_cdf
  !
  ! added 01/2019
  ! read in the dimensions from the clim file 
  !
  CALL check(NF90_GET_VAR(ncid_clim, LatDimID_in, lats))
  CALL check(NF90_GET_VAR(ncid_clim, LonDimID_in, lons))
  CALL check(NF90_GET_VAR(ncid_clim, TimeDimID_in, ndate))
  write(iounit,*) '%I: Input dimensions data read' 

END SUBROUTINE read_dims_cdf

SUBROUTINE read_slice_cdf(iday)
  !--------------------------------------------------------- 
  ! VECTRI: VECtor borne disease infection model of TRIeste.
  !
  ! Tompkins AM. 2012, ICTP
  ! tompkins@ictp.it
  !
  ! read timeslice from climate file
  ! NOTE!!!  ALL POST READ CHECKS NEED TO BE IN A DIFFERENT ROUTINE OR
  ! DUPLICATED IN GRIB INTERFACE TO AVOID BREAKING THE GRIB 
  !---------------------------------------------------------
  INTEGER, INTENT(IN) :: iday
  CALL check(NF90_GET_VAR(ncid_rain, rainVarId_in, rrain, start=(/1,1,iday/),count=(/nlon,nlat,1/)))
  CALL check(NF90_GET_VAR(ncid_clim, tempVarId_in, rtemp, start=(/1,1,iday/),count=(/nlon,nlat,1/)))
  
  IF (lpoptime) THEN ! population function of time
    CALL check(NF90_GET_VAR(ncid_data,popVarId,rpopdensity,start=(/1,1,iday/),count=(/nlon,nlat,1/)))
    WHERE(rpopdensity==rpopdensity_FillValue)rpopdensity=rfillValue
  ENDIF

  ! SIT interventions
  IF (lsit) THEN ! Steralized Insect Technique active 
    CALL check(NF90_GET_VAR(ncid_data,sitVarId,rsit_release,start=(/1,1,iday/),count=(/nlon,nlat,1/)))
    WHERE(rsit_release==rsit_FillValue)rsit_release=rfillValue
  ELSE
    rsit_release=0.0
  ENDIF

  ! BEDNETS
  IF (lbednet) THEN ! bednets in driver dataset
    CALL check(NF90_GET_VAR(ncid_data,bednetVarId,rbednet_release,start=(/1,1,iday/),count=(/nlon,nlat,1/)))
    WHERE(rbednet_release==rbednet_FillValue)rbednet_release=rfillValue
  ELSE
    rbednet=0.0
  ENDIF
  
  IF (npud_scheme==0) THEN ! ponds read wpond from external file
    CALL check(NF90_GET_VAR(ncid_data,pondVarId,rwaterpond,start=(/1,1,iday/),count=(/nlon,nlat,1/)))
  ENDIF

END SUBROUTINE read_slice_cdf

SUBROUTINE write_data_cdf(iday)
!--------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins AM. 2013, ICTP
! tompkins@ictp.it
!
! writedata subroutine to write to netcdf output
!
!
!---------------------------------------------------------

  INTEGER, INTENT(IN) :: iday

  INTEGER :: i

  DO i=1,ndiag2d
    WHERE(rpopdensity(:,:)<0.0) rdiag2d(:,:,i)=rfillvalue
  ENDDO

  IF (loutput_rain) CALL check( NF90_PUT_VAR(grpid_input, ncvar_rain(1), rrain, start=(/ 1, 1, iday /) ))
  IF (loutput_t2m) CALL check( NF90_PUT_VAR(grpid_input, ncvar_t2m(1), rtemp, start=(/ 1, 1, iday /) ))
  IF (loutput_population .AND. lpoptime) &
  & CALL check( NF90_PUT_VAR(grpid_input, ncvar_popdensity(1), rpopdensity, start=(/ 1, 1, iday /) ))

  IF(loutput_wpond) &
  & CALL check(NF90_PUT_VAR(grpid_hydro, ncvar_wpond(1), rwaterpond*wpond_ratio, start=(/ 1, 1, iday /)))
  IF(loutput_vector) &
  & CALL check(NF90_PUT_VAR(grpid_vector, ncvar_vector(1),   rdiag2d(:,:,ncvar_vector(2)), start=(/ 1, 1, iday /)))
  IF(loutput_larvae) &
  & CALL check(NF90_PUT_VAR(grpid_vector, ncvar_larvae(1),   rdiag2d(:,:,ncvar_larvae(2)), start=(/ 1, 1, iday /)))
  IF(loutput_lbiomass) & 
  & CALL check(NF90_PUT_VAR(grpid_vector, ncvar_lbiomass(1),  rdiag2d(:,:,ncvar_lbiomass(2)), start=(/ 1, 1, iday /)))
  IF(loutput_egg) &
  & CALL check(NF90_PUT_VAR(grpid_vector, ncvar_egg(1),  rdiag2d(:,:,ncvar_egg(2)), start=(/ 1, 1, iday /)))
  IF(loutput_emergence) &
  & CALL check(NF90_PUT_VAR(grpid_vector, ncvar_emergence(1),  rdiag2d(:,:,ncvar_emergence(2)), start=(/ 1, 1, iday /)))

  IF(loutput_pr) &
  & CALL check(NF90_PUT_VAR(grpid_disease, ncvar_pr(1),  rdiag2d(:,:,ncvar_pr(2)), start=(/ 1, 1, iday /)))
  !     CALL check(NF90_PUT_VAR(ncidout, ncvar_vectinfect(1),  rdiag2d(:,:,ncvar_vectinfect(2)), start=(/ 1, 1, iday /)))
  IF(loutput_prd) &
  & CALL check(NF90_PUT_VAR(grpid_disease, ncvar_prd(1),  rdiag2d(:,:,ncvar_prd(2)), start=(/ 1, 1, iday /)))
  IF(loutput_hbr) &
  & CALL check(NF90_PUT_VAR(grpid_vector, ncvar_hbr(1),  rdiag2d(:,:,ncvar_hbr(2)), start=(/ 1, 1, iday /)))
  IF(loutput_cspr) &
  & CALL check(NF90_PUT_VAR(grpid_disease, ncvar_cspr(1),  rdiag2d(:,:,ncvar_cspr(2)), start=(/ 1, 1, iday /)))
  IF(loutput_eir) &
  & CALL check(NF90_PUT_VAR(grpid_disease, ncvar_eir(1),  rdiag2d(:,:,ncvar_eir(2)), start=(/ 1, 1, iday /)))
  IF(loutput_cases) &
  & CALL check(NF90_PUT_VAR(grpid_disease, ncvar_cases(1),  rdiag2d(:,:,ncvar_cases(2)), start=(/ 1, 1, iday /)))
  IF(loutput_immunity) THEN
    CALL check(NF90_PUT_VAR(grpid_disease, ncvar_immunity(1),  rdiag2d(:,:,ncvar_immunity(2)), start=(/ 1, 1, iday /)))
    IF(loutput_sit.and.lsit) THEN
       CALL check(NF90_PUT_VAR(grpid_interventions, ncvar_sitm(1), rsitm,start=(/ 1, 1, iday/)))
       CALL check(NF90_PUT_VAR(grpid_interventions, ncvar_sitf(1), rsitf,start=(/ 1, 1, iday/)))
    ENDIF
    If (loutput_bednet.and.lbednet) THEN
       CALL check(NF90_PUT_VAR(grpid_interventions, ncvar_sitf(1), rbednet,start=(/ 1, 1, iday/)))
    ENDIF
  ENDIF
       
  IF(loutput_vecthostratio) &
  & CALL check(NF90_PUT_VAR(grpid_vector, ncvar_vecthostratio(1),  rdiag2d(:,:,ncvar_vecthostratio(2)), start=(/ 1, 1, iday /)))
 
END SUBROUTINE write_data_cdf

!------------------------------------------------------------------------

SUBROUTINE write_restart_cdf

!--------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins AM. 2011, ICTP
! tompkins@ictp.it
!
! setdown subroutine to close shop and tidy up...
!---------------------------------------------------------

  INTEGER :: latvarid, lonvarid
  INTEGER :: &
           & nlarvDimID, &
           & ninfvDimID, &
           & ninfhDimID, &
           & nhostDimID

  !---
  ! 1. close down the ascii and netcdf output files
  !---
  CALL check(NF90_CLOSE(ncidout))
  CALL check(NF90_CLOSE(ncid_clim)) ! close climate file

  CALL check(NF90_CLOSE(ncid_data)) ! close data file

  write(iounit,*) 'input/output closed - now dump restart'

  !---
  ! dump a restart file
  !---
  CALL check(NF90_CREATE(path=TRIM(input)//"restart_vectri.nc", &
       & cmode=or(nf90_clobber,nf90_64bit_offset), ncid = ncidout))

! define the dimensions
  CALL check(NF90_DEF_DIM(ncidout, TRIM(lon_name), nlon, lonvarid))
  CALL check(NF90_DEF_DIM(ncidout, TRIM(lat_name), nlat, latvarid))
  
  CALL check(NF90_DEF_VAR(ncidout, lon_name, NF90_REAL, londimid, lonvarid) )
  CALL check(NF90_DEF_VAR(ncidout, lat_name, NF90_REAL, latdimid, latvarid) )

  CALL check(NF90_PUT_ATT(ncidout, lonvarid, units, lon_units))
  CALL check(NF90_PUT_ATT(ncidout, latvarid, units, lat_units))

  ! define dims:
  !CALL check(NF90_DEF_DIM(ncidout, "ngono", ngono+1, ngonoDimID))
  CALL check(NF90_DEF_DIM(ncidout, "nlarv", nlarv+1, nlarvDimID))
  CALL check(NF90_DEF_DIM(ncidout, "ninfv", ninfv+1, ninfvDimID))
  CALL check(NF90_DEF_DIM(ncidout, "ninfh", ninfh+3, ninfhDimID)) ! immunity+2
  CALL check(NF90_DEF_DIM(ncidout, "nhost", nhost,   nhostDimID))

  ! define global attributes
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "rundate", now))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "vectri", version))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "command", runcommand))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "comments","vectri restart file"))
  CALL check( NF90_PUT_ATT(ncidout, NF90_GLOBAL, "refdate", time_units))

  !---------------------------------------------
  ! define the output structures
  ! routine also sets up the diagnostics indices
  ! reuse the nc 2 byte arrays for indices
  !---------------------------------------------
  !                             index      short name title       units
  ndiag2d=0 ! dummy variable

  CALL define_output_cdf(ncvar_larvae,"larv"  , &
       & "larvae array","m^-2", ndiag2d, (/ nlarvDimID,LonDimId,LatDimID  /),rfillvalue)

  CALL define_output_cdf(ncvar_vector,"vect"  , &
  & "vect array","m^-2", ndiag2d, (/ninfvDimId, LonDimID,  LatDimId  /),rfillvalue)
  CALL define_output_cdf(ncvar_pr,"host"  , &
  & "host array","m^-2", ndiag2d, (/ninfhDimID, nhostDimId,  LonDimID, LatDimId  /),rfillvalue)
  CALL define_output_cdf(ncvar_wpond,"water_frac"  , &
  & "Water coverage of grid cell","fraction", ndiag2d, (/ LonDimID, LatDimId /),rfillvalue)
  
  IF (lsit) THEN
    CALL define_output_cdf(ncvar_sitm,"sitm"  , &
  & "rsitm","m^-2", ndiag2d, (/ LonDimID, LatDimId /),rfillvalue)
    CALL define_output_cdf(ncvar_sitm,"sitf"  , &
  & "rsitf","m^-2", ndiag2d, (/ LonDimID, LatDimId /),rfillvalue)
  ENDIF
  IF (lbednet) THEN
    CALL define_output_cdf(ncvar_bednet,"bednet"  , &
  & "rbednet","km^-2", ndiag2d, (/ LonDimID, LatDimId /),rfillvalue)
  ENDIF

  ! *** ADD NEW PROGNOSTIC FIELDS HERE TO RESTART ***

  ! End define mode.
  CALL check(NF90_ENDDEF(ncidout))

  ! Write the coordinate variable data. This will put the latitudes
  ! and longitudes of our data grid into the netCDF file.
  CALL check( NF90_PUT_VAR(ncidout, latvarid, lats) )
  CALL check( NF90_PUT_VAR(ncidout, lonvarid, lons) )
  CALL check( NF90_PUT_VAR(ncidout, ncvar_larvae(1), rlarv ))
  CALL check( NF90_PUT_VAR(ncidout, ncvar_vector(1), rvect ))
  CALL check( NF90_PUT_VAR(ncidout, ncvar_pr(1), rhost ))
  CALL check( NF90_PUT_VAR(ncidout, ncvar_wpond(1), rwaterpond ))
  IF (lsit) THEN
    CALL check( NF90_PUT_VAR(ncidout, ncvar_sitm(1), rsitm ))
    CALL check( NF90_PUT_VAR(ncidout, ncvar_sitf(1), rsitf ))
  ENDIF
  IF (lbednet) CALL check( NF90_PUT_VAR(ncidout, ncvar_bednet(1), rbednet ))
  CALL check( NF90_CLOSE(ncidout))

  WRITE (iounit,*) 'restart finished ok'

  RETURN
END SUBROUTINE write_restart_cdf

SUBROUTINE read_restart_cdf
  !
  ! read the restart file
  !
  INTEGER :: ncid,ivarid,nlatcheck,nloncheck
  REAL    :: zfillvalue ! local fill value 


  WRITE(iounit,*) 'opening netcdf RESTART condition file ',TRIM(initfile)
  CALL check(NF90_OPEN(path=initfile,mode=nf90_nowrite,ncid=ncid))

  CALL check(NF90_INQ_DIMID(ncid, lat_name, LatDimID))
  CALL check(NF90_INQUIRE_DIMENSION(Ncid, LatDimID, len = nlatcheck))

  CALL check(NF90_INQ_DIMID(ncid, lon_name, LonDimID))
  CALL check(NF90_INQUIRE_DIMENSION(Ncid, LonDimID, len = nloncheck))
  IF (nlon.ne.nloncheck .or. nlat.ne.nlatcheck) THEN
    WRITE(iounit,*) '%E: *** data dimensions error - restart and climate file differ *** ', &
         & nlon,nloncheck,nlat,nlatcheck,'initfile'
    STOP '%E: *** data dimensions input error *** '
  ENDIF

  !
  ! a. read data for PR initialization
  !
  CALL check(NF90_INQ_VARID(ncid, "host", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, rhost))
  CALL check(NF90_GET_ATT(ncid, iVarId, "_FillValue", zFillValue))

  !
  ! b. read data for larvae initialization
  !
  CALL check(NF90_INQ_VARID(ncid, "larv", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, rlarv))
  !
  ! c. read data for vector initialization
  !
  CALL check(NF90_INQ_VARID(ncid, "vect", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, rvect))
  !
  ! d. read data for water fraction
  !
  CALL check(NF90_INQ_VARID(ncid, "water_frac", iVarId))
  CALL check(NF90_GET_VAR(ncid, iVarId, rwaterpond))

  ! e. read any interventions 
  IF (lsit) THEN
    CALL check(NF90_INQ_VARID(ncid, "sitm", iVarId))
    CALL check(NF90_GET_VAR(ncid, iVarId, rsitm))
    CALL check(NF90_INQ_VARID(ncid, "sitf", iVarId))
    CALL check(NF90_GET_VAR(ncid, iVarId, rsitf))
  ENDIF
  IF (lbednet) THEN
    CALL check(NF90_INQ_VARID(ncid, "bednet", iVarId))
    CALL check(NF90_GET_VAR(ncid, iVarId, rbednet))
  ENDIF
     


  
  ! close the initialization file
  CALL check(NF90_CLOSE(ncid))
  WRITE(iounit,*)'%I: initial conditions read ok'

END SUBROUTINE read_restart_cdf

SUBROUTINE read_data_cdf
  !--------------------------------------------------------- 
  !  added 01/2019 - to read all fixed environmental data
  !                 must be on same grid as climate data 
  !
  ! data file contains all the non-climate information on a common grid:
  ! - Population density (static or function of time)  >v1.6
  ! - Permanent water bodies (static) >v1.8 
  ! - Dynamic pond fraction (function of time) > v1.9
  ! - Land soil type (static) > 1.9
  ! - Land Use (optional)
  ! - SIT interventions > v1.10 (v2?)
  !
  !
  ! from v1.8 all safety checks on initialization need to be moved to initialize in mo_interface 
  !           otherwise they will be missed when GRIB file format is used.
  !
  ! from 1.8.3 added the ability to read a dynamic externally read pond fraction
  !
  ! from v1.9 - soil types
  ! from v1.10 - SIT interventions
  ! 

  IMPLICIT NONE

  INTEGER :: iVarID,i,istatus,ndimpop

  !----------------
  ! fixed data file
  !----------------
  WRITE(iounit,*)'%I: opening datafile ',TRIM(datafile)
  CALL check(NF90_OPEN(path=datafile,mode=nf90_nowrite,ncid=ncid_data))
  !istatus=NF90_INQ_DIMID(ncid_data,"time",timedimid_data)
  !IF(istatus==nf90_noerr) WRITE(iounit,*)'%I: found time variable in ',datafile

  !  REINSTATE THIS CHECK:CALL check_latlon(ncid_,tlats,tlons)

  !---------------------
  ! read population data
  !---------------------
  pop_names(1)=pop_name ! passes empty string if not in namelist
  DO i=1,SIZE(pop_names)
     istatus=NF90_INQ_VARID(ncid_data,TRIM(pop_names(i)),popvarid)
     pop_name=pop_names(i)
     IF(istatus==nf90_noerr)EXIT
  ENDDO
  IF (istatus/=nf90_noerr)STOP 'pop name not found'
  WRITE(iounit,*) '%I: reading population from datafile using name ',TRIM(pop_name)

  CALL check(NF90_INQ_VARID(ncid_data,pop_name, popVarId))
  CALL check(NF90_GET_ATT(ncid_data, popVarId, "_FillValue", rpopdensity_FillValue))

  ! If pop is 2D array lat/lon, read here, otherwise
  ! If pop is function of time, then read one slice in read_slice_cdf
  CALL check(NF90_INQUIRE_VARIABLE(ncid_data,popVarId, ndims=ndimpop))
  IF (ndimpop<=2 .OR. npop_option==1) THEN ! not a function of time, read (1st slice/all) now:    
     lpoptime=.FALSE.
     CALL check(NF90_GET_VAR(ncid_data, popVarId, rpopdensity(:,:)))
     WHERE(rpopdensity==rpopdensity_FillValue)rpopdensity=rfillValue
  ELSE
     lpoptime=.TRUE.
     !CALL check(NF90_GET_VAR(ncid_data, iVarId, rpopdensity(:,:),start=(/1,1,1/),count=(/nlon,nlat,1/))) 
  ENDIF

  !---------------------
  ! read soil data
  ! need to duplicate in GRIB sections.
  !---------------------
  DO i=1,SIZE(soil)
     istatus=NF90_INQ_VARID(ncid_data,TRIM(soil(i)%ncname),soil(i)%varid)   
     IF(istatus/=nf90_noerr) EXIT  ! bomb out of loop if not found
     WRITE(iounit,*) '%I: from datafile reading ',TRIM(soil(i)%ncname)
     CALL check(NF90_GET_ATT(ncid_data, soil(i)%varid, "_FillValue", soil(i)%miss))
     CALL check(NF90_GET_VAR(ncid_data, soil(i)%varid, soil(i)%vals(:,:)))     
  ENDDO
  IF (istatus/=nf90_noerr) THEN
     WRITE(iounit,*) '%I: No soil texture found: switching to default 0.333 for each soil type '
     DO i=1,SIZE(soil)
        soil(i)%vals(:,:)=1./3.
     ENDDO
  ENDIF

  !--------------------------------------------------
  ! wperm permanent pond fraction at lake/river edges
  !--------------------------------------------------
  DO i=1,SIZE(wperm_names)
     istatus=NF90_INQ_VARID(ncid_data,TRIM(wperm_names(i)),iVarId)
     wperm_name=wperm_names(i)
     IF(istatus==nf90_noerr)EXIT
  ENDDO
  IF (istatus==nf90_noerr) THEN
     WRITE (iounit,*) '%I: reading wperm from datafile using name ',wperm_name
     loutput_wperm=.true. ! switch output on automatically
     CALL check(NF90_INQ_VARID(ncid_data,wperm_name, iVarId))
     CALL check(NF90_GET_ATT(ncid_data, iVarId, "_FillValue", rwperm_FillValue))
     CALL check(NF90_GET_VAR(ncid_data, iVarId, rwaterperm(:,:)))
     WHERE (rwaterperm==rwperm_FillValue) rwaterperm=wperm_default
  ELSE 
     ! in v1.8+ this is ONLY for cases where no perm is in the netcdf file, needs to be duplicated
     ! to the grib sections.
     !
     WRITE (iounit,*) '%I: wperm not found, setting to default: ',wperm_default
     !WRITE (iounit,*) '%I: VECTRI searched for these wperm names:',wperm_names
     rwaterperm(:,:)=wperm_default
  ENDIF

  !--------------------------------------------------
  ! dynamic waterfrac information from data file?
  !--------------------------------------------------
  IF (npud_scheme==0) THEN
     DO i=1,SIZE(wpond_names)
        istatus=NF90_INQ_VARID(ncid_data,TRIM(wpond_names(i)),iVarId)
        wpond_name=wpond_names(i)
        IF(istatus==nf90_noerr)EXIT
     ENDDO
     IF (istatus==nf90_noerr) THEN
        WRITE (iounit,*) '%I: reading wpond frac from datafile '
        CALL check(NF90_INQ_VARID(ncid_data,wpond_name,pondVarId))
     ELSE 
        npud_scheme=npud_scheme_default
        WRITE (iounit,*) '%I: No pond fraction in datafile, defaulting to scheme ',npud_scheme_default
     ENDIF
  ELSE 
     WRITE (iounit,*) '%I: Using internal pond scheme ',npud_scheme
  ENDIF

  !--------------------------------------------------
  ! static wurbn fraction information from data file?
  !--------------------------------------------------
     DO i=1,SIZE(wurbn_names)
        istatus=NF90_INQ_VARID(ncid_data,TRIM(wurbn_names(i)),iVarId)
        wurbn_name=wurbn_names(i)
        IF(istatus==nf90_noerr)EXIT
     ENDDO
     IF (istatus==nf90_noerr) THEN
        WRITE (iounit,*) '%I: reading wurbn frac from datafile '
        CALL check(NF90_INQ_VARID(ncid_data,wurbn_name,urbnVarId))
        CALL check(NF90_GET_VAR(ncid_data, urbnVarId, rwaterurbn(:,:)))     
     ELSE 
        wurbn_name="wurbn"
        WRITE (iounit,*) '%I: No urban water fraction in datafile, defaulting to internal scheme '
     ENDIF


  !----------------
  ! LUC data
  !----------------
  IF (.FALSE.) THEN
     !---------------------
     ! read data
     !---------------------
     CALL check(NF90_INQ_VARID(ncid_data, "Band1", iVarId))
     ! CALL check(NF90_GET_ATT(ncid_data, iVarId, "_FillValue", rpopdensity_FillValue))

     ! simple fixed population with fixed growth - otherwise will link to population MODEL! 
     CALL check(NF90_GET_VAR(ncid_data, iVarId, rwateroccupancy(:,:)))
  ELSE  ! single grid point OR NEED TO ADD SWITCH FOR CONSTANT VALUE
     rwateroccupancy=1.0 ! or fixed value
  ENDIF !l2d

  !--------------------------------------------------
  ! SIT interventions
  !--------------------------------------------------
  IF (lsit) THEN
     DO i=1,SIZE(sit_names)
        istatus=NF90_INQ_VARID(ncid_data,TRIM(sit_names(i)),sitVarId)
        sit_name=sit_names(i)
        IF(istatus==nf90_noerr)EXIT
     ENDDO
     IF (istatus==nf90_noerr) THEN
        WRITE (iounit,*) '%I: reading SIT from datafile using name ',sit_name
        loutput_sit=.true. ! switch output on automatically, can override in namelist 
        CALL check(NF90_INQ_VARID(ncid_data,sit_name, sitVarId))
        CALL check(NF90_GET_ATT(ncid_data, sitVarId, "_FillValue", rsit_FillValue))

        ! read in global atts
        CALL check(NF90_GET_ATT(ncid_data, nf90_global, "sit_init",sit_init))
        CALL check(NF90_GET_ATT(ncid_data, nf90_global, "sit_nrel",sit_nrel))
        CALL check(NF90_GET_ATT(ncid_data, nf90_global, "sit_nvec",sit_nvec))
        CALL check(NF90_GET_ATT(ncid_data, nf90_global, "sit_len",sit_len))
        CALL check(NF90_GET_ATT(ncid_data, nf90_global, "sit_ratio",sit_ratio))

     ELSE 
        ! in v1.9.1 this is ONLY for cases where no SIT is in the netcdf file, needs to be duplicated
        ! to the grib sections.
        !
        WRITE (iounit,*) '%I: SIT interventions not found, no SIT interventions applied: '
        lsit=.FALSE. ! turn off since default is true...
     ENDIF
  ENDIF
  !--------------------------------------------------
  ! BEDNET interventions
  !--------------------------------------------------
  IF (lbednet) THEN
     DO i=1,SIZE(bednet_names)
        istatus=NF90_INQ_VARID(ncid_data,TRIM(bednet_names(i)),bednetVarId)
        bednet_name=bednet_names(i)
        IF(istatus==nf90_noerr)EXIT
     ENDDO
     IF (istatus==nf90_noerr) THEN
        WRITE (iounit,*) '%I: reading BEDNETS from datafile using name ',bednet_name
        loutput_bednet=.true. ! switch output on automatically for the moment (can override in namelist)
        CALL check(NF90_INQ_VARID(ncid_data,bednet_name, bednetVarId))
        CALL check(NF90_GET_ATT(ncid_data, bednetVarId, "_FillValue", rbednet_FillValue))
     ELSE 
        ! in v1.8+ this is ONLY for cases where no perm is in the netcdf file, needs to be duplicated
        ! to the grib sections.
        !
        WRITE (iounit,*) '%I: BEDNET interventions not found, no BEDNET interventions applied: '
        lbednet=.FALSE.
     ENDIF
  ENDIF

  WRITE (iounit,*) '%I: Datafile successfully read'

END SUBROUTINE read_data_cdf

END MODULE mo_ncdf_tools

