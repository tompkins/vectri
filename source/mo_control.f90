MODULE mo_control
!--------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins AM. 2011, ICTP
! tompkins@ictp.it
!
! run control parameters for model with initial values
!---------------------------------------------------------

  USE netcdf
  USE mo_constants

  IMPLICIT NONE


  LOGICAL :: lgrib=.false. ! add to namelist
  LOGICAL :: lverbose=.false. 
  LOGICAL :: lascii=.false. 

  REAL, PARAMETER :: dt =24./24.       ! timestep in days, ~HAS TO BE DAILY FOR MOMENT =1

! Time constants
  REAL :: rdaysinyear=365.0    ! number of days in year (LMM uses 360)

! model spin up - nloopspinup now refers to the number of spin up "loops" - and the loop length
! is set by nlenspinup.  Examples:
! Repeat the first year 3 times:          nloopspinup=3  nlenspinup=365
! Repeat the first 60 days, 10 times:   nloopspinup=10 nlenspinup=60
  INTEGER  :: nloopspinup=0    ! Number of loops (no longer fixed to a year, name same for compatibility)
  INTEGER  :: nlenspinup=365   ! spinup period of one loop (units of days)

! move derived parameters to setup.f90
  INTEGER :: nstep ! =nday/dt ! length of run in timesteps ASSUME DT=1 for moment
  
! numerics parameters
!
! numeric scheme for advection
! 1:  - box category HM04
! 2:  - split box (inverse SL)
! 3:  - semi langrangian linear interpolation
! 4:  - semi langrangian cubic interpolation
! 5:  - semi langrangian GTD cubic interpolation
! 6:  - semi langrangian cubic spline interpolation
  INTEGER :: nnumeric=2

! array resolution definition constants
  INTEGER, PARAMETER :: nlarv=25  ! resolution of larvae development
  INTEGER, PARAMETER :: ninfv=25  ! resolution of vector parasite development
  
  INTEGER, PARAMETER :: ninfh=NINT(rhost_infectd/dt)       ! resolution of host parasite development 
  INTEGER, PARAMETER :: nimmune1=ninfh+1,nimmune2=ninfh+2  ! increase model speed.  

  ! Array size of eir pdf distributions
  ! No point setting this parameter to values > 20 since infection probability is already 0.999 
  INTEGER, PARAMETER :: nbitepdf=5  

! trickle minimum vector concentration due to accidental import or advection
  REAL :: rvect_min=1.e-4  ! 1.3.2 bug correct, remove scale by ninfect

! index of point to dump quick timeseries diagnostics
  INTEGER :: nxdg=427,nydg=226 

! metdata files
  INTEGER :: npop_option=0 ! 0=detect if population function of time from input file automatically
                           ! 1=population fixed in time, read first data slice 
                           ! 2 and above, free for further options (e.g. idealized scenarios) currently unused

  REAL :: rfillvalue=NF90_FILL_DOUBLE  ! old default was -9999.0 ! missing value
  
END MODULE mo_control
