PROGRAM VECTRI
  ! 
  ! VECTRI: VECtor borne disease infection model of ICTP TRIeste.
  !
  ! Author: A.M.Tompkins
  !         ICTP, Strada Costiera 11, 34152 Trieste, ITALY
  !         tompkins@ictp.it
  !
  ! --------------------------------------------------------------------------
  ! LICENSE INFORMATION: 
  ! 1) This software is issued under a free-to-use license for research/education purposes
  ! 2) Please do not distribute further without prior permission from the author
  ! 3) Please email a copy of manuscripts using the software to the author 
  ! 4) For the model description cite:
  !      Tompkins and Ermert 2013 for the key model description (v1.2.6)
  !      Tompkins and Di Giuseppe 2015 for the updates to mortality
  !      Asare et al. 2016a GH for revisions to surface hydrology
  !      Tompkins and Thomson 2018 PLOS 1, for details on calibration process
  !
  !  Documentation for running the model and a short exercise sheet is found in doc/
  !
  ! --------------------------------------------------------------------------
  !
  ! Loosely based on HM2005 and Depinay 62 and Bomblies 2008 
  !
  ! Eventual wishlist of features:
  !   1) modified survival rate formula (Ermert 2010)
  !   2) advection/diffusion(flight) of vector from cell to cell 
  !   3) modified surface hydrology for egg laying
  !      eventually this will try to represent improved conditions in drought near rivers
  !      and improved conditions in rainy season elsewhere.
  !   4) diffusion of infected human population to account for local migration
  !   5) gravity model/relaxation of proportion p to account for long-distance migration
  !   6) standard deviation of temperature to account for subgrid-scale variability and microclimates
  !   7) biting rates account for population density and are Poisson distributed.
  !   8) Immunity
  !
  ! modules explicitly the 
  ! 1) gonotrophic cycle: 
  !   the time taken to prepare a brood (determines biting cycle)
  !
  ! 2) sporogonic cycle (SC) (the process of fertilization
  !   of the macrogametocyte, formation of the oocyst,
  !   ookinete, penetration of the midgut and then the subsequent
  !   development of the sporozoites which dwell in the
  !   salivary glands)
  !
  !   variable convention
  !   -------------------
  !   integer parameter n
  !   integer local     i
  !   real parameter    r
  !   real local        z
  !   
  !   references 
  !
  !   1) Hoshen and Morse (2004)
  !   2) Ermert (2010)
  !   3) Bomblies et al. (2008)
  !   4) garki model (1974)
  !   5) depinay et al. (2004)
  !   6) Tompkins and Ermert (2013) <- model description at v1.2.6
  !   7) Craig et al. 1999 
  !
  ! --- Glossary ---
  !
  !  Entomological data
  !
  !   HBR - host bite rate - number of bites per host per day
  !   EIR - Entomological inoculation rate (n of infectious bites per person)
  !   CSPR - Circumsporozoite protein rate (fraction eir/hbr)
  !
  !  Epidemiological data
  !    
  !
  ! --- order of code ---
  !
  ! Note that the model equations are solved with a very simple 1st order step solver
  ! which is not accurate.  Eventually I want to rewrite the code in parallel and 
  ! improve the numerical solver to something second order accurate. 
  ! 
  !
  ! 1. write out diagnostics
  ! 2. read in met data for timestep
  !
  ! DISEASE PROGRESSION:
  ! 3. biting treatment
  !    3.1 - calculation transmission probabilities
  !    3.2 - Vector to human transmission
  !      3.2.1 Evolution of disease in host
  !      3.2.2 clearing rate 
  ! 4.  Sporogonic cycle
  !
  ! VECTOR DEVELOPMENT (incl. some interventions)
  ! 5.  Gonotrophic cycle
  ! 6.  Vector temperature dependent survival
  ! 7.  Vector Oviposition
  ! 8.  Pond model
  ! 9.  Larvae maturation - limitation of resourses
  ! 10. Larvae maturation - larvae progression
  ! 12. Larvae hatching
  !
  ! INTERVENTION MODELLING
  ! 13. interventions
  !
  ! 14. Diffusion of vectors.
  ! 
  ! 15. NCDF output
  !
  ! Release history now in README file
  !
  USE mo_constants
  USE mo_control
  USE mo_vectri
  USE mo_interface
  USE mo_inout
  USE ISO_FORTRAN_ENV, ONLY : INT32, INT64

  IMPLICIT NONE

  ! local integers 
  INTEGER :: i,ix,iy,ixp1,ixm1,iyp1,iym1,iloop &
       &           ,iinfv,ihost,iinfh &
       &           ,icheck=0
  
  INTEGER :: istep,iday
  INTEGER(INT64) :: idate


! local real scalars
  REAL ::    zgonof, zsporof, zhostf &
       &           ,zsurvp_vec &  ! adult vector survival probability
       &           ,zrain  &  ! dummy rain variables, daily
       &           ,zlarvmaturef, zdel, zfac, zmasslarv, zcapacity, zbiolimit &
       &           ,ztemp, ztempindoor, ztempwater & !dummy temperature, indoor temperature and water temperature
       &           ,zlimit, zcases, znewegg &
       &           ,zvectinfectbite, zhost_SE, zhost_I, zhost_R &
       &           ,zprobhost2vect, zprobhost2vect_I, zprobhost2vect_R &
       &           ,zprobvect2hostS, zprobvect2hostEIR & !transmission probabilities
       &           ,znnhbr, znndailyeir, znndailyeirS, znndailyeirEI &
       &           ,zpud1, zpud2, zrunoff, zflushr & ! useful pond variables
       &           ,zguess, zsit_newfemales, sum_egg

  
  ! fortran timer functions
  REAL :: time1=0.0,time2=0.0
  LOGICAL :: lspinup, &  ! spin up period - turn off output
       &            ltimer=.false.,ltimc=.false.      ! turn the cpu timer on for the first timestep
  ! local real vectors
  REAL, ALLOCATABLE :: zvect_density(:,:),zvect_one_d_density(:,:),zvect_one_d_density_males(:,:)
  ! calculate of parasite ratio including ALL exposed (from day 1, i.e. not detectable)
  REAL, ALLOCATABLE :: zpr(:,:),zcspr(:,:),zbednet_ratio(:,:)
  ! copy of rvect array
  REAL, ALLOCATABLE :: zsurvp_larv(:)
  
  REAL, ALLOCATABLE :: zvectn(:,:,:)


! --------------------
! START OF VECTRI CODE
! --------------------

  CALL setup ! initial conditions for arrays

  CALL timer('start of main loop:',icheck,time1,time2)

! allocate the 2d local arrays
  ALLOCATE(zvect_density(nlon,nlat))
  ALLOCATE(zvect_one_d_density(nlon,nlat))
  ALLOCATE(zvect_one_d_density_males(nlon,nlat))
  ALLOCATE(zpr(nlon,nlat))
  ALLOCATE(zcspr(nlon,nlat))
  ALLOCATE(zbednet_ratio(nlon,nlat))
  ALLOCATE(zvectn(0:ninfv,nlon,nlat))
  ALLOCATE(zsurvp_larv(0:nlarv))
  
  ! Spin up
  WRITE(iounit,*) 'starting run',nrun
  DO iloop=1,nrun
     WRITE(*,'(1a1,A5,I10,$)') char(13), 'step ',iloop
     IF (ltimer) CALL timer('go  ',icheck,time1,time2) !set up timer

     ! for spin up
     IF (iloop<=nloopspinup*INT(nlenspinup)/dt) THEN
        istep=MOD(iloop-1,INT(nlenspinup))+1
        IF (iloop==1) WRITE(iounit,*) 'spin up period switched on for ',nloopspinup*nlenspinup,' days' 
        lspinup=.TRUE. 
     ELSE 
        istep=iloop-nloopspinup*INT(nlenspinup/dt)
        IF (iloop==nloopspinup*INT(nlenspinup)/dt+1) write(iounit,*) 'INTEGRATION STARTS' 
        lspinup=.FALSE. 
     ENDIF

     idate=ndate(istep)
     iday=istep*NINT(dt)

     !-----------------------------------------------
     ! read in met data timeslice
     ! also reads in pop/interventions from data file
     !-----------------------------------------------
     CALL read_slice(istep)
     
     !-------------------------------------------------
     ! apply simple fixed climate or population changes
     !-------------------------------------------------
     CALL update_input(istep)

     !------------------------------------------------
     ! 1. diagnostics for the present timestep
     !    Some of these are used in the analysis
     !    While some are for diagnostic purposes only 
     !------------------------------------------------

     !-----------------------
     ! 1.1 derive diagnostics
     !-----------------------

     !------------------------------------------------
     ! safety
     !------------------------------------------------
     rlarv=MAX(rlarv,0.0)
     rvect=MAX(rvect,0.0)
     rvect(0,:,:)=MAX(rvect(0,:,:),rvect_min)

     ! copy rvect for diffusion calculation

     ! temporary diagnostics:
     zvect_density(:,:)=SUM(rvect,DIM=1) ! total vector number = vector density    
     zvect_one_d_density(:,:)=1.0/MAX(zvect_density(:,:),reps)
     !zvect_one_d_density_males(:,:)=1.0/MAX(zvect_density(:,:)+rsitm(:,:),reps) ! male density fac
     zpr=SUM(rhost(1:nimmune1,nadult_ni,:,:),DIM=1) ! parasite ratio
     zcspr=rvect(ninfv,:,:)*zvect_one_d_density ! CSPR
     IF (lbednet) zbednet_ratio=1.0-rbite_night*rbednet/MAX(rpopdensity,reps) ! proportion sleeping under LLIN nets
     
     ! store some diagnostics:
     IF (loutput_vector) rdiag2d(:,:,ncvar_vector(2))=zvect_density  ! store vector density
     IF (loutput_larvae) rdiag2d(:,:,ncvar_larvae(2))=SUM(rlarv,DIM=1) ! total larvae number

     IF (loutput_prd)    rdiag2d(:,:,ncvar_prd(2))= &                  ! parasite ratio
          &   SUM(rhost(INT(rhost_detectd):nimmune1,nadult_ni,:,:),DIM=1) !-rmigration  !
     IF (loutput_pr)     rdiag2d(:,:,ncvar_pr(2))=SUM(rhost(1:ninfh,nadult_ni,:,:),DIM=1)

     IF (loutput_cspr)   rdiag2d(:,:,ncvar_cspr(2))=zcspr
     IF (loutput_vecthostratio) rdiag2d(:,:,ncvar_vecthostratio(2))= &
          & 1e6*zvect_density(:,:)/MAX(rpopdensity(:,:),reps)

     ! zoophilic rates - well actually is anthropophilic rate.
     WHERE (rpopdensity(:,:)>=0.0) &
          & rzoophilic=1.0-(1.0-rzoophilic_min)*EXP(-rpopdensity(:,:)/rzoophilic_tau)
     rbitezoo(:,:)=1.0-(1.0-rbiteratio*rzoophilic(:,:))**dt ! product useful 

     
     !-----------------
     ! GRIDDED LOOP !!!
     !-----------------
     DO iy=1,nlat
        DO ix=1,nlon
           IF (ix==nxdg.and.iy==nydg) THEN
              CONTINUE !dummy statement for debugging 
           ENDIF

           IF (ltimer.and.iy==nlat/2.and.ix==nlon/2) THEN
              ltimc=.true.
           ELSE
              ltimc=.false.
           ENDIF
           
           !---------------------------------------------
           ! 1.2 point diagnostics for calculations below
           !---------------------------------------------
           ! total biomass of larvae per m2 of water surface
           zmasslarv=SUM(rlarv(:,ix,iy)*rmasslarv(:))
           if (loutput_lbiomass)rdiag2d(ix,iy,ncvar_lbiomass(2))=zmasslarv
           
           ! numbers of l1/2, and l3-4 instars for cannibalism
           ! *very* approximately 1/3 and 1/3 using data for Aedes Agypti
           ! Lin et al. 2001  https://doi.org/10.1046/j.1365-2915.2000.00207.x
           !zl1l2larv=SUM(rlarv(0:int(nlarv/3),ix,iy)
           !zl3l4larv=SUM(rlarv(int(nlarv/3)+1:int(2*nlarv/3),ix,iy)
           
           ! proportion of hosts in SEIR categories
           zhost_I=SUM(rhost(ninfh,:,ix,iy))
           zhost_R=SUM(rhost(nimmune1:nimmune2,:,ix,iy))
           zhost_SE=1.0-zhost_I-zhost_R
           
           IF(lverbose)&
           WRITE(iounit,*)'host check ',zhost_SE,SUM(rhost(0:ninfh-1,:,ix,iy))

           ! daily cspr: proportion of infected vectors that can tranmit and may bite
           zvectinfectbite=rvect(ninfv,ix,iy)*zvect_one_d_density(ix,iy)
           
           !------------------------
           ! 2. meteorological data
           !------------------------
           zrain=MIN(MAX(rrain(ix,iy),0.0),200.0)  ! 1 day rainfall, with safety limits applied.

           ! indoor temperature - after Lunde et al. Malaria Journal 2013, 12:28
           ztempindoor=10.33+0.58*rtemp(ix,iy)

           ! water temperature 
           ztempwater=rtemp(ix,iy)+rwater_tempoffset ! water temperature

           IF(lverbose)&
           & WRITE(iounit,*)'rtemp ',rtemp(ix,iy),rwater_tempoffset,ztemp
           ! temperature experience by vector is mix of indoor and outdoor 
           ! temperature on a daily timestep
           ztemp=rbeta_indoor*ztempindoor+(1.0-rbeta_indoor)*rtemp(ix,iy)
           
           !---------------------------------------
           ! ONLY RUN MODEL IF NOT A LAKE/SEA POINT
           ! AND TEMPERATURE/PRECIP ARE NOT MISSING
           !---------------------------------------
           IF (rpopdensity(ix,iy)/=rfillValue & 
              .AND. rtemp(ix,iy)/=rfillvalue &
              .AND. ABS(lats(iy))<rlatmax) THEN

              !---------------------
              ! 3a. Gonotrophic cycle
              !---------------------
              !
              ! Using degree day concept of Detinova (1962)
              ! fraction of gonotrophic cycle in a day...
              zgonof=dt*(ztemp-rtgono)/dgono
              zgonof=MIN(MAX(0.0,zgonof),1.0)

              !---------------------
              ! 3b. Biting treatment
              !---------------------

              ! --------------------------
              ! Transmission probabilities
              ! --------------------------
              !  
              ! - zbiteratio of vectors get a meal
              ! - rzoophilic proportion of these on humans
              ! - rbednet_ratio: proportion of bites NOT on LLIN covered people
              !   = 1.0 - nightbite_frac*proportion of people under nets
              ! - zhostinfect of the people are infected
              ! - rpt* prob of transmission
              !   
              ! zeroth box movement depends on biting ratio
              ! - will possibly adjust the survival for this category 
              !   as feeding is dangerous.
              ! - this will depend on population of vector and host - 4now constant - 
              !  

              !----------------------------------------------
              ! 3.1 set up parameters needed in this timestep
              !----------------------------------------------

              ! ---------
              ! host2vect:
              ! ---------
              ! transmission from the host to vector - 
              ! Assume each biting vector gets one blood meal
              !
              ! v1.4 corrects a bug that was introduced with
              !      zhighrisk.  Categories I and R are considered to
              !      be at higher risk, S are more likely to be safer
              !      Previously this was accounted for for vect2host transmission
              !      But not host2vect.  Corrected in v1.4
              !   
              ! zprobhost2vect_I: probability of I getting a bite that leads host2vector trans
              !
              ! zprobhost2vect_R: probability of R getting a bite that leads host2vector trans
              ! 
              ! zprobhost2vect : overall probability of vector acquiring
              !  parasite accounting for highrisk, zoo factor and categories - sum of above two.
              !  categories as they already account for proportions and risk
              !
              zdel=zhost_SE+rbitehighrisk*(zhost_I+zhost_R) ! repeated RHS bot
              zprobhost2vect_I=rpthost2vect_I*rbitezoo(ix,iy)*zhost_I*rbitehighrisk/zdel
              zprobhost2vect_R=rpthost2vect_R*rbitezoo(ix,iy)*zhost_R*rbitehighrisk/zdel
              zprobhost2vect=zprobhost2vect_I+zprobhost2vect_R ! total prob

              IF(lverbose)WRITE(iounit,*)'prob',rbitezoo(ix,iy),rpthost2vect_I,rpthost2vect_R,zprobhost2vect
              ! ---------
              ! vect2host:
              ! ---------
              !
              ! 1e6 because pop in km**-2 while vectors in m**-2
              znnhbr=rbiteratio*rzoophilic(ix,iy)*zgonof*zvect_density(ix,iy) &
                   & *1.e6/MAX(rpopdensity(ix,iy),rpopdensity_min)
              IF(lbednet) znnhbr=znnhbr*zbednet_ratio(ix,iy)
              IF(loutput_hbr) rdiag2d(ix,iy,ncvar_hbr(2))=znnhbr

              ! daily EIR 
              znndailyeir=zvectinfectbite*znnhbr
              IF(loutput_eir) rdiag2d(ix,iy,ncvar_eir(2))=znndailyeir

              if (znndailyeir>1000.) THEN
                WRITE(iounit,*)"SOMETHING IS WRONG, EIR>1000????",znndailyeir,ix,iy
                WRITE(iounit,*)rbiteratio,rpopdensity(ix,iy),rzoophilic(ix,iy),zvect_density(ix,iy)
                WRITE(iounit,*)zvect_density(ix,iy)
                WRITE(iounit,*)rwaterpond(ix,iy),rwaterperm(ix,iy),"water"

                STOP
              ENDIF

              !
              ! Treat biting as a random event, therefore is distributed as a Poisson PDF.
              ! however, only need to call if vector infection rate greater than zero
              ! 
              zprobvect2hostS=0.0
              zprobvect2hostEIR=0.0
              IF (znndailyeir>reps) THEN
                 ! we could speed this up if rbitehighrisk=1
                 ! highrisk refers to average number of bites on 
                 znndailyeirS=znndailyeir/(1.0+(rbitehighrisk-1.0)*zpr(ix,iy))
                 CALL transmission(znndailyeirS,zprobvect2hostS)
                 znndailyeirEI=znndailyeirS*rbitehighrisk
                 CALL transmission(znndailyeirEI,zprobvect2hostEIR)
              ENDIF

              !--------------------------------
              ! Vector to human transmission
              !--------------------------------
              IF (ltimc) CALL timer('1. tran',icheck,time1,time2) ! cpu timer

              !--------------------------------------------
              ! SEIR model for Vector to human transmission
              !--------------------------------------------
              ! In all releases v1.X this is a simple SEI model 
              ! with multiple E boxes to model the delay correctly.
              ! From v1.4 the SEI model was extended to SEIR to add immunity 
              ! and cases diagnostic for E->I transition recorded.

              !-----------------------------------------
              ! 3.2 Evolution of disease in host S->E->I
              !-----------------------------------------
              ! record pre-transition I totals.
              zcases=SUM(rhost(ninfh,:,ix,iy)) ! this would need to be a weighted mean if nhost>1
              zhostf=dt/ninfh
              DO ihost=1,nhost 
                 ! from v1.3.5 this prob is separate for S class...
                 ! immunity: Only advect first ninfh boxes
                 ! Transition S->E->I
                 CALL advection(zprobvect2hostS,zhostf,rhost(0:ninfh,ihost,ix,iy),ninfh) 
              ENDDO

              ! calculate new I arrivals = cases
              zcases=MAX(0.0,SUM(rhost(ninfh,:,ix,iy))-zcases) ! this would need to be a weighted mean if nhost>1
              IF (loutput_cases) rdiag2d(ix,iy,ncvar_cases(2))=zcases

              !-----------------------
              ! 3.5 Immunity Module
              !-----------------------
              ! 3.5.1 Gain I->R 
              !-----------------------
              IF (rimmune_gain_tau>0.0) THEN ! if gain tau=0 immunity switched off
                 DO ihost=1,nhost
                    !    Pimmune=1.0-EXP(EIRd/EIRTAU)
                    zdel=MAX(0.0,1.0-EXP(-znndailyeir/rimmune_gain_tau))*rhost(ninfh,ihost,ix,iy)
                    ! Transition I->R
                    rhost(nimmune1,ihost,ix,iy)=rhost(nimmune1,ihost,ix,iy)+zdel
                    rhost(ninfh,ihost,ix,iy)=rhost(ninfh,ihost,ix,iy)-zdel
                 ENDDO
                 
                 !---------------------------
                 ! 3.5.2 Immunity Loss R2->S
                 !       from v1.10.0 only R2 
                 !---------------------------
                 ! As loss rate is >> timestep can use explicit numerics
                 DO ihost=1,nhost
                    zdel=rhost(nimmune2,ihost,ix,iy)*dt/rimmune_loss_tau
                    ! Transition R2->S
                    rhost(nimmune2,ihost,ix,iy)=rhost(nimmune2,ihost,ix,iy)-zdel
                    rhost(0,ihost,ix,iy)=rhost(0,ihost,ix,iy)+zdel 
                 ENDDO

                 !------------------------------------------
                 ! 3.5.3 reinfection of cleared immune class
                 !       R2->R1 from v1.10.0
                 !------------------------------------------
                 DO ihost=1,nhost
                    zdel=rhost(nimmune2,ihost,ix,iy)*zprobvect2hostEIR
                    ! Transition R2->R1
                    rhost(nimmune2,ihost,ix,iy)=rhost(nimmune2,ihost,ix,iy)-zdel
                    rhost(nimmune1,ihost,ix,iy)=rhost(nimmune1,ihost,ix,iy)+zdel 
                 ENDDO
              ENDIF ! immunity switch

              IF (loutput_immunity) rdiag2d(ix,iy,ncvar_immunity(2))= &
                & SUM(rhost(nimmune1:nimmune2,:,ix,iy))/MAX(SUM(rhost(:,:,ix,iy)),reps)

              !------------------------------------------------------
              ! 3.7 clearance of parasites: 
              !
              ! As clearing rate is >> timestep use explicit numerics
              !------------------------------------------------------
              DO ihost=1,nhost

                 !------------------------------------
                 !     I->S
                 !     from 1.4.0 only apply to ninfh
                 !------------------------------------
                 zdel=dt/rhostclear*rhost(ninfh,ihost,ix,iy) 

                 !------------------------------------
                 !     Transition I->S
                 !------------------------------------
                 rhost(ninfh,ihost,ix,iy)=rhost(ninfh,ihost,ix,iy)-zdel
                 rhost(0,ihost,ix,iy)=rhost(0,ihost,ix,iy)+zdel

                 !------------------------------------
                 !     R1->R2 (clearance in immune class)
                 !     from v1.8
                 !------------------------------------
                 zdel=dt/rhostimmuneclear*rhost(nimmune1,ihost,ix,iy) 
                 rhost(nimmune1,ihost,ix,iy)=rhost(nimmune1,ihost,ix,iy)-zdel
                 rhost(nimmune2,ihost,ix,iy)=rhost(nimmune2,ihost,ix,iy)+zdel
              ENDDO

              !-----------------------------------------------------
              ! 3.8 Death/Replacement of population: E,I,R->S
              !-----------------------------------------------------
              DO ihost=1,nhost
                 DO iinfh=1,nimmune2
                    zdel=rpop_death_rate_daily*rhost(iinfh,ihost,ix,iy)
                    rhost(iinfh,ihost,ix,iy)=rhost(iinfh,ihost,ix,iy)-zdel
                    rhost(0,ihost,ix,iy)=rhost(0,ihost,ix,iy)+zdel
                 ENDDO
              ENDDO

              !---------------------
              ! 4. Sporogonic cycle
              !---------------------

              ! 4.1 transmission of parasite
              !      
              ! v1.4 bug correct - rbiteratio added to transmission probability
              ! v1.8 - gonof added as gonotrophic cycle resolution removed.
              zdel=rbiteratio*zprobhost2vect*zgonof*rvect(0,ix,iy)
              rvect(0,ix,iy)=rvect(0,ix,iy)-zdel
              rvect(1,ix,iy)=rvect(1,ix,iy)+zdel

              ! degree day concept of Detinova (1962):
              zsporof=dt*(ztemp-rtsporo)/dsporo
              zsporof=MIN(MAX(0.0,zsporof),1.0)

              IF (lverbose) THEN
                WRITE(iounit,*)'prob ',zprobhost2vect,zdel,zsporof
                WRITE(iounit,*)'vect tot',SUM(rvect)
                WRITE(iounit,*)'cspr ',rdiag2d(:,:,ncvar_cspr(2))
                WRITE(iounit,*)'PRd', rdiag2d(:,:,ncvar_prd(2))
                WRITE(iounit,*)'EIRa ',365*znndailyeir
                WRITE(iounit,*)'water frac ',rwaterpond(ix,iy)
              ENDIF
             
              IF (zsporof>reps) THEN
                CALL advection(0.0,zsporof,rvect(0:ninfv,ix,iy),ninfv)
              ENDIF

              IF (ltimc) CALL timer('2. spor',icheck,time1,time2) ! cpu timer

              !---------------------
              ! 5. Gonotrophic cycle
              !---------------------
              ! from v1.8 no longer explicitly resolved. 
              
              !-----------------------------------------
              ! 6. Vector mortality
              !-----------------------------------------
              !
              ! 6a. Vector temperature dependent survival
              !
              ! See PhD Thesis of Ermert 2010 for I-III
              !
              ! 1) Anopheles Martins I
              ! 2) Anopheles Martins II 
              ! 3) Anopheles Bayoh Scheme
              ! 4) Albopictus Metelmann Scheme
              !    https://doi.org/10.1098/rsif.2018.0761
              !
              !-----------------------------------------

              IF (ztemp>rtvecsurvmin.AND.ztemp<rtvecsurvmax) THEN

                 SELECT CASE(nsurvival_scheme)
                 CASE(1) ! martins I
                    zsurvp_vec=rmar1(0) + rmar1(1)*ztemp + rmar1(2)*ztemp**2
                 CASE(2) ! martins II
                    zsurvp_vec=EXP(-1.0/(rmar2(0)+rmar2(1)*ztemp+rmar2(2)*ztemp**2))
                 CASE(3) ! Bayoh scheme 
                    zsurvp_vec= -2.123e-7*ztemp**5 &
                         & +1.951e-5*ztemp**4 &
                         & -6.394e-4*ztemp**3 &
                         & +8.217e-3*ztemp**2 &
                         & -1.865e-2*ztemp + 7.238e-1
                 CASE(4) ! Metelmann scheme 
                    zsurvp_vec= 0.677*EXP(-0.5*((ztemp-20.9)/13.2)**6)*ztemp**0.1 
                 CASE DEFAULT
                    STOP 'invalid survival scheme'
                 END SELECT

                 ! from v1.3.5 base mortality rate added
                 zsurvp_vec=zsurvp_vec*rvecsurv
                 zsurvp_vec=MIN(MAX(rvecsurv_min,zsurvp_vec),1.0)

                 ! Include bednets here, LLINs assumed to remain effective during their full life median ~2year 
                 IF (lbednet) zsurvp_vec=zsurvp_vec*zbednet_ratio(ix,iy)

                 zsurvp_vec=zsurvp_vec**dt ! adjust by timestep:
              ELSE
                 zsurvp_vec=rvecsurv_min**dt ! small background probability for niche  
              ENDIF

              !--------------------------------------------------------
              ! 6b. INTERVENTIONS : LLIN and IRS (BEDNETS AND SPRAYING)
              !--------------------------------------------------------
              ! rbednet is
              ! proportion of people under nets*net efficifacy*night bite prop.
              ! zsurvp_vec=zsurvp_vec*rbednet(ix,iy)

              ! apply the mortality

              rvect(:,ix,iy)=zsurvp_vec*rvect(:,ix,iy)
              !----------------------------------------------------------
              ! 7. Vector oviposition
              !
              ! v1.10 added factor to reduce eggs with SIT females present 
              !-----------------------------------------------------------
              zfac=1.0-rsitf(ix,iy)*zvect_one_d_density(ix,iy)
              sum_egg=0
              DO iinfv=0,ninfv
                 znewegg=neggmn*(1.0-rsitf(ix,iy)*zvect_one_d_density(ix,iy))*zgonof*rvect(iinfv,ix,iy)
                 rlarv(0,ix,iy)=rlarv(0,ix,iy) + znewegg
                 sum_egg = sum_egg + znewegg
                 
              ENDDO
              if (loutput_egg) rdiag2d(ix,iy,ncvar_egg(2))=sum_egg

              IF (ltimc) CALL timer('3. eggs',icheck,time1,time2) ! cpu timer

              !-----------------------------------------
              ! 8. Pond model
              !-----------------------------------------
              ! pond model, increases surface area at rate related to rainfall
              !
              ! This fixes a serious bug pre-1.3.3
              ! whereby waterfrac is the archived/initialized variable but rpuddle was the prognosed variable!
              !
              ! from v1.8.3 rpuddle can be read in from outside, in which case this code is skipped
              ! In this case rwaterfrac is effectively diagnostic, and rainfall is not required. 
              !
              ! from v1.9 cleaned up and all reference to rpuddle deleted... waterfrac used on own and wperm separate
              ! 
              SELECT CASE(npud_scheme)
              CASE(0)
                ! external scheme do nothing here as ponding read in from external data file
                CONTINUE

              ! Simple Hydro scheme described in Tompkins and Ermert (2013)
              CASE(1)
                zpud1=wpond_rate*dt
                rwaterpond(ix,iy)=(rwaterpond(ix,iy)+zpud1*zrain*wpond_max)/ &
                     & (1.0+zpud1*(zrain+wpond_evap+rsoilinfil(ix,iy)))
               
              !--------------------------------------------------------------------------
              ! revised hydrology
              ! implicit scheme described in 
              ! Asare et al. 2016a (Geospatial Health)
              ! Asare, Tompkins and Bomblies PLOS One 2016
              ! Scheme includes shape factor for pond geometry and initial abstraction
              ! the latter term is important to prevent ponding after light rain events. 
              !
              ! dw/dt = K w^(-p/2) (fQ - w(E+fI))
              !         K: pond geometry scale factor (related to S0 and h0 in geometry model)
              !         p: pond geometry power factor (0.5-2 temporary ponds, 3-5 lakes)
              !         f: proportion of maximum pond area factor 1-w/w_max
              !         Q: run off - calculated from SCS formula Q=(P-0.2S)^2/(P+0.8S)
              !         E: evaporation from ponds
              !         I: maxmimum infiltration rate from ponds
              !-----------------------------------------
              CASE(2) 
                zrunoff=MAX(0.0,zrain-0.2*wpond_S)**2/(zrain+0.8*wpond_S)  
                zrunoff=Max(zrunoff,0.0)

                zpud1=(2.0*dt)/(wpond_shapep2*wpond_depthref)!2*Dt/(p*refernce water depth)
                zpud2=(2.*dt*wpond_ref**(wpond_shapep2/2.))/ &
                & (wpond_shapep2*wpond_depthref)
                !2*wref^(p/2)/p*href
 
                ! first guess solution
                zguess=(rwaterpond(ix,iy)+zpud1*(zrunoff*wpond_max))/ &
                & (1.0+zpud1*(zrunoff+wpond_evap-zrain+(wpond_ref*zrain)/wpond_max+&
                & (wpond_ref*rsoilinfil(ix,iy))/wpond_max))

                ! solution
                rwaterpond(ix,iy)=(rwaterpond(ix,iy)+zpud2*(zguess** &
                & (-wpond_shapep2/2.)*zrunoff*wpond_max))/ &
                & (1.0+zpud2*zguess**(-wpond_shapep2/2.)* &
                & (zrunoff+wpond_evap-zrain+ &
                & (zguess*zrain)/wpond_max+ &
                & (zguess*rsoilinfil(ix,iy))/wpond_max))

              CASE DEFAULT
                WRITE(iounit,*)'no default option for wpond - please set npud_scheme=0,1,2'
                STOP
              END SELECT
              
              ! breeding water fraction is the sum of temporary ponds and permanent water bodies. 
              rwaterpond(ix,iy)=MIN(MAX(rwaterpond(ix,iy),wpond_min),wpond_max) ! safety

              !-----------------------------------------------
              ! 9. Larvae maturation - limitation of resourses
              !-----------------------------------------------

              !-------------------------------------------
              ! 10. Larvae maturation - larvae progression
              !-------------------------------------------
              ! larvae maturation progress rate  
              ! degree day concept of Detinova (1962) again:

              ! egg development and pupae development are all around one day, 
              ! therefore the immature phase length is mostly controlled by the 
              ! immature phase.
              IF (ztempwater>rlarv_tmin) THEN
!               IF (ztempwater>rlarv_tmin .AND. ztempwater <rlarv_tmax ) THEN
                 zlarvmaturef=rlarvmature(nlarv_scheme,1)*ztempwater+rlarvmature(nlarv_scheme,2)
                 !zlarvmaturef=zlarvmaturef/(zlarvmaturef*(rlarv_eggtime+rlarv_pupaetime))
                 zlarvmaturef=zlarvmaturef*dt ! timestep 
                 zlarvmaturef=MIN(MAX(0.0,zlarvmaturef),1.0)
              ELSE
                 zlarvmaturef=0.0
              ENDIF
              if (lverbose)WRITE(iounit,*)'twater larvf',ztempwater,zlarvmaturef

              IF (zlarvmaturef>reps) CALL advection(1.0, zlarvmaturef,rlarv(:,ix,iy),nlarv)

              IF (ltimc) CALL timer('4. larv',icheck,time1,time2) ! cpu timer

              !-----------------------------------------
              ! 11. Larvae mortality
              !-----------------------------------------
              ! larvae mortality is due to
              ! 1) Dessication
              ! 2) Background due to preditation and other causes
              ! 3) Water temperatures
              ! 4) Resource limitation
              ! 5) Flushing 
              ! 6) v2.0 Cannibalism 
              ! 
              ! 1) Dessication - if waterfrac reduces over a timestep, 
              !                  the larvae are reduced by the same frac
              !                  this of course assumes that the pool are independent
              !
              ! 2) Initialize the array 
              zsurvp_larv(:)=1.0
              
              !
              ! 2) Water temperatures
              !
              ! Here for function of temperature we use Data from Bayoh and Lindsay 2003,2004
              !           Mean survival                         Proportion of terminal
              !            in days (95%         Range of larval  events occurring as    Equality of survival
              !Temperature
              !( C)       confidence interval) mortality (days) larval mortality (%)   distributions*
              !10           2.7 (2.6-2.8)        2-5             100.0                  a
              !12           3.7 (3.6-3.9)        1-6             100.0                  -
              !14          20.5 (19.3-21.8)      5-42            100.0                  -
              !16          25.5 (24.4-26.5)      9-39            100.0   *cycle survival*  b
              !18          24.9 (23.8-26.2)     10-38             58.0    30.9  0.972      b
              !20          24.9 (23.6-26.4)      3-31             24.7    23.0  0.987      b
              !22          18.1 (17.5-18.6)      5-20             24.0    18.3  0.985       -
              !24          16.4 (15.9-16.8)      6-18             20.7    15.3  0.985    -
              !26          13.5 (13.2-13.9)      5-15             27.3    13.0  0.976    -
              !28          11.0 (10.6-11.4)      3-14             33.3    11.4  0.965    c
              !30          11.2 (10.8-11.5)      4-16             72.7    10.1  0.879    c
              !32          10.2 (9.9-10.5)       5-13             70.0    9.1   0.876    -
              !34           8.9 (8.5-9.3)        4-14            100.0                  -
              !36           6.9 (6.8-7.2)        4-10            100.0                  -
              !38           4.8 (4.6-4.9)        3-7             100.0                  -
              !40           2.8 (2.6-2.9)        2-4             100.0                  a
              ! 
              ! The *cycle survival* is calculated from the survival and development length
              ! survival rate as a function of temperature is calculated.
              ! 
              ! We fit a scaled logistic curve  smin+(smax-smin)*(1.0-1.0/(1.0+exp((t0-tdata)/tau))) using the NLS package in R: 
              ! Smax, t0 and tau are fitted.  Smin is a fixed constant at 0.8 
              ! (you can achieve a good fit with smin at any value between 0 and 0.8, the data does not allow you to specify a first
              !  guess as it does not cover the high temperature range in a useful way).
              ! 
              ! The data is smoothed by a 3 point running mean, with the end points retained:
              ! 
              ! R code:
              ! tdata=c(18,20,22,24,26,28,30,32)
              ! sdata=c(0.972,0.987,0.985,0.985,0.976,0.965,0.879,0.876)
              ! sdata=c(0.972,rollmean(sdata,3),0.876)
              ! fit=nls(sdata ~ smin+(smax-smin)*(1.0-1.0/(1.0+exp((t0-tdata)/tau))) , start=list(t0=t0,tau=tau,smax=smax))
              !
              ! smin=0.8 gives:
              !     Estimate Std. Error t value Pr(>|t|)    
              ! t0   30.975279   0.310659  99.708 1.92e-09 ***
              ! tau   2.189333   0.385852   5.674  0.00237 ** 
              ! smax  0.983665   0.005228 188.164 8.04e-11 ***
              
              ! smin=0.7 gives:
              ! t0   33.1
              ! tau   2.75
              ! smax  0.984
              
              ! assumed same across all larvae
              !zsurvp_larv(:)=0.8+(rlarvsurv-0.8)*(1.0-1.0/(1.0+EXP((30.98-ztempwater)/2.18)))
              SELECT CASE(nsurvival_scheme)

              CASE(1) !Anopheles with martins I
                zsurvp_larv(:)=zsurvp_larv(:)*(0.7+(rlarvsurv-0.7)*(1.0-1.0/(1.0+EXP((33.1-ztempwater)/2.75))))
              CASE(2) !Anopheles with martins II
                zsurvp_larv(:)=zsurvp_larv(:)*(0.7+(rlarvsurv-0.7)*(1.0-1.0/(1.0+EXP((33.1-ztempwater)/2.75))))
              CASE(3) !Anopheles with Bayoh
                zsurvp_larv(:)=zsurvp_larv(:)*(0.7+(rlarvsurv-0.7)*(1.0-1.0/(1.0+EXP((33.1-ztempwater)/2.75))))
              CASE(4) !Aedes albopictus Metelmann 
                ! Larvae mortality
                zsurvp_larv(1:)=zsurvp_larv(1:)*(0.977*EXP(-0.5*((ztempwater-21.8)/16.6)**6))
                ! Egg mortality
                zsurvp_larv(0)=zsurvp_larv(0)*(0.955*EXP(-0.5*((ztempwater-18.8)/21.53)**6))
              CASE DEFAULT
                    STOP 'invalid survival scheme'
              END SELECT
              ! 3) Resource limitation / Overcrowding:
              ! Bomblies slowed development due to overcrowding but 
              ! this causes unrealistically long development
              ! times, and doesn't reflect observations,
              ! so instead we use the overcrowding factor 
              ! to alter the mortality rate for larvae
              ! limitation of resources - negative feedback on numbers
              ! integrate biomass of larvae - bomblies instead slows grow rate 
              ! from v1.3.5 added rwateroccupancy, which gives fractional occupancy by LU type.
              zbiolimit=rbiocapacity* &
              & (rwaterpond(ix,iy)*wpond_ratio + &  ! temporary ponds (gambiae)
              &  rwaterperm(ix,iy)*wperm_ratio + &  ! pools and ponding near streams, rivers and lakes (funestus)
              &  rwaterurbn(ix,iy)*wurbn_ratio )    ! water associated with human habitations (cans, tyres etc) Aedes

              ! The previous parameterization linearly reduces survival rate as a function of food limitation
              ! 
              zcapacity=MIN(MAX((zbiolimit-zmasslarv)/zbiolimit,0.01),1.0)
              zsurvp_larv=zsurvp_larv*zcapacity ! Bomblies type capacity limitation
              
              ! 4) cannibalism

              ! instead of biolimit in terms of laervae mass, should translate into energy per larvae. 
              
              ! food availability for late stage larvae enhanced by L1/L4 ratio...

              
              !-----------------------------
              ! dessication by rain FLUSHING
              !----------------------------
              zflushr=(1.0-rlarv_flushmin)*EXP(-zrain/rlarv_flushtau)+rlarv_flushmin

              !
              ! REPLACE, when we instigate L1-4 larvae will apply
              DO i=0,nlarv
                 ! greatest flushing to L1 larvae so apply as a linear function
                 zlimit=REAL(i)/REAL(nlarv)
                 ! : is a bug to emulate previous
                 zsurvp_larv(i)=zsurvp_larv(i)*(zlimit*(1.0-zflushr)+zflushr)
                 zsurvp_larv(i)=zsurvp_larv(i)**dt ! timestep
                 zsurvp_larv(i)=MIN(MAX(0.01,zsurvp_larv(i)),1.0)               
                 rlarv(i,ix,iy)=rlarv(i,ix,iy)*zsurvp_larv(i)
              ENDDO

              !-----------------------------------------
              ! 12. Larvae hatching
              ! 
              ! v1.10 need to store for update to SIT females
              ! This assumes all female breed on first day
              !-----------------------------------------
              ! emergence of new females from larvae stage:
              !
              ! (m*fac)/(m*fac+v) <- SIT female fraction
              !
              if (loutput_emergence) rdiag2d(ix,iy,ncvar_emergence(2))=rlarv(nlarv,ix,iy)
              rvect(0,ix,iy)=rvect(0,ix,iy)+rlarv(nlarv,ix,iy)
              
              IF (lsit) THEN
                zfac=rsitm(ix,iy)*rsit_breed/(rsitm(ix,iy)*rsit_breed+zvect_density(ix,iy))
                ! how many of these breed with SIT treated males? :
                zsit_newfemales=rlarv(nlarv,ix,iy)*zfac
              ELSE
                zsit_newfemales=0.0
              ENDIF
             
              ! remove larvae that have now hatched:
              rlarv(nlarv,ix,iy)=0.0    

              IF (ltimc) CALL timer('5. end1',icheck,time1,time2) ! cpu timer

              !-------------------------------------------
              ! 13. Update interventions
              !-------------------------------------------
              ! SIT
              !            V- mortality              V- new SIT females from breeding
              IF (lsit) THEN
                rsitf(ix,iy)=zsurvp_vec*rsitf(ix,iy) + zsit_newfemales
              !            V- mortality              V- new SIT male releases                
                rsitm(ix,iy)=(1.0-rsit_mortality+rsit_mortality*zsurvp_vec)*rsitm(ix,iy)+rsit_release(ix,iy)

                ! recalculate for diagnostics:

              ENDIF

              IF (lbednet) THEN
                ! As loss rate is >> timestep can use explicit numerical approx (faster)
                rbednet(ix,iy)=rbednet(ix,iy)*(1.0-dt/rbednet_tau)+rbednet_release(ix,iy)
              ENDIF
             
              ! In version 2 of the code  major rewrite is planned with vectri.f90 to become a module
              ! and all processes as methods on vectri and host types, with parallel processes
              ! facilitated through the break up of the loops.
              !
              ! host migration
              !     for the moment - consider a simple migration from "outside" as a trickle source
              !     a second option will be introduced to represent migration in the gridded world
              !     !------------------------------
              ! options:
              ! a. method 1: do not allow uninfected to exceed 99.5 % (remove in postprocessing)
              !
              ! b.  use the gravity model as in the cholera model 
              !     M=Pop(a).Pop(b)/(distance a-b) (set up matrix value before)
              !     but the matrix will limit maximum distance - small distances 
              !     will be set to zero since workers return to sleep in same 
              !     location each night when transmission occurs.
              !
              ! c. Full agent based model (not implemented yet [only 7 years late!])
              !
              !------------------------------------------------------------------------------------

              ! rmigration represents minimum PRd allowed.
              zdel=MAX(rmigration-rhost(ninfh,nadult_ni,ix,iy),0.0)
              zdel=MIN(zdel,rhost(0,nadult_ni,ix,iy)) ! check enough here (always!) 
              rhost(ninfh,nadult_ni,ix,iy)=rhost(ninfh,nadult_ni,ix,iy)+zdel
              rhost(0,nadult_ni,ix,iy)    =rhost(0,nadult_ni,ix,iy)    -zdel

              IF (lascii) THEN
                 WRITE(iounit,*)'ascii output no longer supported from v1.3.3'
                 lascii=.false.
              ENDIF
              IF (ltimc) CALL timer('diag',icheck,time1,time2) ! cpu timer

              !-----------------------
              ! 15 Safety checks:
              !-----------------------
              DO ihost=1,nhost
                zfac=1.0/SUM(rhost(:,ihost,ix,iy))
                rhost(:,ihost,ix,iy)=rhost(:,ihost,ix,iy)*zfac
              ENDDO

              !--------------------
              ! END OF SPATIAL LOOP
              !--------------------

              !-----------------------
              ! 15 Safety checks:
              !-----------------------
              DO ihost=1,nhost
                zfac=1.0/SUM(rhost(:,ihost,ix,iy))
                rhost(:,ihost,ix,iy)=rhost(:,ihost,ix,iy)*zfac
              ENDDO

           ELSE  ! sea/lake identified by missing population.
              rdiag2d(ix,iy,:)=rfillvalue  ! set all fields to missing over sea points.
           ENDIF ! non-lake or sea point loop
        ENDDO !nlon
     ENDDO !nlat

     !--------------------------
     ! 14 diffusion of vectors
     !--------------------------
     ! In v1.8 implementation we use an explicit method to permit calculation in the long loop
     ! and avoid the expense of the matrix solver.  As 2D dt/dx is very small for vectors at
     ! dx>10km resolutions and daily timesteps then accuracy is not an issue.
     !
     ! The Diffusion coefficient by default is taken from CITATION NEEDED
     ! Not that no downwind/upwind advection is accounted for...
     !
     ! has to be out the loop as it needs the up and down stream gridpoints.
     !
     ! to do: 
     ! - this will be a weighted random walk, weighted
     ! - to allow clustering around populations and breeding sites
     !
     ! First version: simple FTCS... stability not an issue at our timesteps and resolutions.
     !
     ! v1.9.x Bug correct to turn off diffusion for nx=ny=1
     ! v1.10 (v2) need to also implement SIT in diffusion.
     !
     IF (ltimer) CALL timer('6. end loop ',icheck,time1,time2) ! cpu timer

     IF (rvect_diffusion>reps)THEN 
       DO iy=1,nlat
          IF (ABS(lats(iy))<rlatmax) THEN
             DO ix=1,nlon
                ixp1=MIN(ix,nlon)
                ixm1=MAX(ix,1)
                iyp1=MIN(iy,nlat)
                iym1=MAX(iy,1)
                zvectn(:,ix,iy)=ralfa_x(ix,iy)*(rvect(:,ixp1,iy)+rvect(:,ixm1,iy)) + &
                &          ralfa_y(ix,iy)*(rvect(:,ix,iyp1)+rvect(:,ix,iym1)) + &
                & (1-2*ralfa_x(ix,iy)+2*ralfa_y(ix,iy))*rvect(:,ix,iy)
             END DO
          ENDIF
       END DO
       ! cycle arrays
       rvect=zvectn
     END IF

     IF (ltimer) CALL timer('7. end diff ',icheck,time1,time2) ! cpu timer

     !-----------------------
     ! 16. OUTPUT GRIB/NETCDF
     !-----------------------
     IF (.NOT.lspinup) THEN
       CALL write_data(iday)
     ENDIF

     IF (ltimer) CALL timer('7. end write ',icheck,time1,time2) ! cpu timer

  ENDDO ! date loop
  
  ! write restart file
  CALL write_restart

  WRITE(iounit,*) 'integration finished'
  CALL timer('end of code cpu:',icheck,time1,time2)
  STOP
  
  !---------------------------------------
CONTAINS
  SUBROUTINE timer(str,icheck,time1,time2)

    IMPLICIT NONE

    REAL :: time1,time2
    INTEGER :: icheck
    CHARACTER(LEN=*) :: str

    CALL cpu_time(time2)
    WRITE(iounit,*)'Check point ',icheck,str,1000*(time2-time1)
    time1=time2
    icheck=icheck+1

    RETURN
  END SUBROUTINE timer

SUBROUTINE transmission(rdailyeir,rprob)
!--------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins A.M. 2011, ICTP
! tompkins@ictp.it
!
! bites per person follow a Poisson distribution
!   - This is important if bite rate low - since it will reduce the mean 
!     tranmission rate (few people get bitten a lot, but can only get infected once!)
! Poisson distribution - used for biting rate
    ! Poisson PDF - this reduces mean transmission, as getting 7 infectious bites not 
    ! not much different probability to getting 4, but this implies 3 other people do 
    ! not get bitten.
!----------------------------------------------------------

  IMPLICIT NONE

  REAL, INTENT(IN)    :: rdailyeir 
  REAL, INTENT(INOUT) :: rprob     ! mean probability of getting infected, integrated across the PDF

  INTEGER :: i,j

  REAL :: zpk
  REAL :: zpdf(0:nbitepdf) 

  !
  ! probability of N bites distributed by Poisson PDF
  ! with a mean of rdailyeir 
  !
  DO i=0,nbitepdf-1
    zpk=0.0
    DO j=1,i
      zpk=zpk+LOG(REAL(j))
    ENDDO
    zpdf(i)=EXP(REAL(i)*LOG(rdailyeir)-rdailyeir-zpk)
  ENDDO
  zpdf(nbitepdf)=MAX(1.0-SUM(zpdf(0:nbitepdf-1)),0.0)

  !
  ! final transmission rate is the dot product of the bite rate
  ! with the transmission probability array   
  !
  rprob=SUM(zpdf*rpdfvect2host)

  ! bed net use will go in here?
  
  RETURN
END SUBROUTINE transmission

SUBROUTINE advection(rbox0,rfrac,rarray,na)
!--------------------------------------------------------- 
! VECTRI
!
! Tompkins AM. 2011, ICTP
! tompkins@ictp.it
!
!
! advection: routine to move on boxes by rfrac amount
!
! 1: hm04 numerics - move on by integer boxes (inaccurate)
! 2: bilinear forward (inverse semi-Lagrangian) advection
! 3: bilinear semi-Lagrangian advection (default, diffusive but fast)
! 4: cubic semi-Lagrangian advection
! 5: gtd cubic semi-Lagrangian advection
! 5: hans-C spline 
!----------------------------------------------------------

  USE mo_constants
  USE mo_control
  USE mo_vectri

  IMPLICIT NONE

  INTEGER,  INTENT(IN)   :: na
  REAL, INTENT(IN)   :: rfrac 
  REAL, INTENT(IN)   :: rbox0 

  REAL, INTENT(INOUT) :: rarray(0:na)
!  REAL, INTENT(INOUT) :: wrap_array(0:na)

  ! local arrays
  REAL, ALLOCATABLE :: zarray1(:),zarray2(:)

  ! local integers 
  INTEGER :: i,ideli,idx,im1,ip1
  ! local real scalars

  REAL:: zfrac, zdep, zalfa, zalfa2 &
&           ,zdel &
&           ,znew &
&           ,y0,y1,y2,y3,a0,a1,a2,a3


  zfrac=REAL(na)*rfrac

  ALLOCATE(zarray2(0:na))
  zarray2=1.0
  zarray2(0)=rbox0

  SELECT CASE(nnumeric)
    CASE(1) ! hm04 numerics, integer box shifting 

      ideli=NINT(zfrac)
      ALLOCATE(zarray1(0:na))
      zarray1=rarray
      DO i=0,na   
        idx=i+ideli      ! new index
        idx=MIN(na,idx)   ! don't move beyond end of array
        zdel=zarray1(i)*zarray2(i)
        rarray(idx)=rarray(idx)+zdel
        rarray(i)  =rarray(i)  -zdel
      ENDDO
      DEALLOCATE(zarray1)

    CASE(2) ! split box shifting 

      ideli=FLOOR(zfrac)
      zalfa=zfrac-ideli

      ALLOCATE(zarray1(0:na))
      zarray1=rarray ! copy input array
      DO i=0,na   
        im1=i+ideli      ! new index
        ip1=im1+1
        im1=MIN(na,im1)   ! 
        ip1=MIN(na,ip1)   ! don't move beyond end of array
        ! zarray2 means that part of box 0 is left behind:
        zdel=zarray1(i)*zarray2(i) 
        rarray(im1)=rarray(im1)+zdel*(1-zalfa)
        rarray(ip1)=rarray(ip1)+zdel*zalfa
        rarray(i)  =rarray(i)  -zdel
      ENDDO
      DEALLOCATE(zarray1)


    CASE(3) ! semi lagrangian bilin interpolation 

      ALLOCATE(zarray1(-4:2*na))
      zarray1(:)=0.0
      zarray1(0:na)=rarray ! zarray stores the original array
      rarray(:)=0.0

! 1.0-rbox0 fraction of the box0 contents are not advected
      rarray(0)=(1.0-rbox0)*zarray1(0)
      zarray1(0)=rbox0*zarray1(0)

! maybe we need cyclic ??? vector bites lays and then goonotropic cycle starts.
      DO i=0,2*na-1   
        ! departure point
        zdep=MAX(-2.0,REAL(i)-zfrac)

        ! define bounding points 
        im1=FLOOR(zdep)
        ip1=im1+1      
        zalfa=zdep-im1

        znew=zalfa*zarray1(ip1)+(1.0-zalfa)*zarray1(im1)
        idx=MIN(i,na)
        rarray(idx)=rarray(idx)+znew
      ENDDO
      DEALLOCATE(zarray1)
   
    CASE(4) ! semi lagrangian cubic interpolation 
      ALLOCATE(zarray1(-4:2*na))
      zarray1=0.0
      zarray1(0:na)=rarray 
      rarray=0.0
      DO i=0,2*na-1   
        ! departure point
        zdep=MAX(-2.0,REAL(i)-zfrac)

        ! define bounding points 
        im1=FLOOR(zdep)
        ip1=im1+1

        zalfa=zdep-im1
        zalfa2=zalfa*zalfa
        y0=zarray1(im1-1)
        y1=zarray1(im1)
        y2=zarray1(ip1)
        y3=zarray1(ip1+1)

        a0 = y3 - y2 - y0 + y1
        a1 = y0 - y1 - a0
        a2 = y2 - y0
        a3 = y1

!        a0 = -0.5*y0 + 1.5*y1 - 1.5*y2 + 0.5*y3;
!        a1 = y0 - 2.5*y1 + 2*y2 - 0.5*y3;
!        a2 = -0.5*y0 + 0.5*y2;
!        a3 = y1;

        znew=a0*zalfa*zalfa2+a1*zalfa2+a2*zalfa+a3
        idx=MIN(i,na)
        rarray(idx)=rarray(idx)+znew
      ENDDO
      DEALLOCATE(zarray1)

    CASE(5) ! gtd semi lagrangian cubic interpolation 
      ALLOCATE(zarray1(-4:na))
      zarray1=0.0
      zarray1(0:na)=rarray 
      rarray=0.0

      zalfa=1.0-zfrac+FLOOR(zfrac)
      a0 = -(zalfa*(1.0 - zalfa*zalfa))/6.0
      a1 = (zalfa*(1.0 + zalfa)*(2.0 - zalfa))/2.0
      a2 = ((1.0 - zalfa*zalfa)*(2.0 - zalfa))/2.0
      a3 = -(zalfa*(1.0 - zalfa)*(2.0 - zalfa))/6.0

      DO i=0,2*na   
        ! departure point and bounding 
        zdep=MAX(-3.0+zalfa,REAL(i)-zfrac)
        im1=FLOOR(zdep)
        ip1=im1+1      
        y0=zarray1(im1-1)
        y1=zarray1(im1)
        y2=zarray1(ip1)
        y3=zarray1(ip1+1)
        znew=a0*y0 + a1*y1 + a2*y2 + a3*y3        
        idx=MIN(i,na) ! limit to array end
        rarray(idx)=rarray(idx)+znew
      ENDDO
      DEALLOCATE(zarray1)
!   return(a0*mu*mu2+a1*mu2+a2*mu+a3);

!    CASE(4)  ! Catmull-Rom splines. 
!   a0 = -0.5*y0 + 1.5*y1 - 1.5*y2 + 0.5*y3;
!   a1 = y0 - 2.5*y1 + 2*y2 - 0.5*y3;
!   a2 = -0.5*y0 + 0.5*y2;
!   a3 = y1;
    CASE DEFAULT
      WRITE(iounit,*)'incorrect advection scheme'
      STOP
  END SELECT

  RETURN
END SUBROUTINE ADVECTION

END PROGRAM VECTRI
