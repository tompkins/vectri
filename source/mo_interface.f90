MODULE mo_interface
!
! interface routines
!
  USE mo_control
  USE mo_climate
  USE mo_constants
  USE mo_vectri
  USE mo_namelist
  USE mo_inout

  IMPLICIT NONE

CONTAINS

  SUBROUTINE setup
  !--------------------------------------------------------- 
  ! VECTRI: VECtor borne disease infection model of TRIeste. 
  !
  ! Tompkins AM. 2011, ICTP
  ! tompkins@ictp.it  
  !
  ! setup subroutine to read in forcing data and setup output files
  !
  ! Note: needs rewriting to move checks on static data earlier
  !       This way we can switch off reading of precip if pond fraction present
  !
  !---------------------------------------------------------
    
  ! 1. read the CONTROL namelists
  CALL read_namelist(1)

  ! set up default values
  ! (need to do after namelist CONTROL as this might set new vector index) 
  CALL init_constants
  
  ! 2. set up text output
  CALL text_output

  ! 3. open the input netcdf/grib files and get *metadata* and dimensions
  !    note the climate data is read in during the simulation in the loop.
  CALL get_dims

  ! 4. allocate the arrays
  CALL allocate_arrays

  ! 5. read the dimension (lat/lon/dates) data from netcdf/grib clim file
  CALL read_dims

  ! 6. Set array initial values
  !    most single pars are initialized directly in mo_constants
  !    but here set up vector specific parameters, vector selection made in namelist_control.
  !    by setting gridded defaults here, instead of an ELSE clause in the netcdf reading is
  !    more robust since we don't need to update GRIB interface in tandem. 
  CALL set_gridded_defaults

  ! 8. read fixed time-invariant fields (population, permanent hydrology etc) from netcdf
  CALL read_data

  ! 7. Now read the constants namelist which allows user-driver parameter override.
  CALL read_namelist(2)

  ! 9. initialize prognostic data constructs from restart file or artificial initialization.
  CALL initialize

  ! 10. open netcdf/grib output files - these use metadata from input
  CALL open_output

  END SUBROUTINE SETUP 


!-----------------------------------------------
  SUBROUTINE read_namelist(inamelist)

    CHARACTER(500) :: namelistfile
    INTEGER, INTENT(IN) :: inamelist
    
    !-------------------------------
    ! 1. NAMELISTS
    !-------------------------------
    IF(COMMAND_ARGUMENT_COUNT().lt.1) THEN
      WRITE(iounit,*) 'no namelist argument provided, trying ./vectri.namelist'
      namelistfile='./vectri.namelist'
    ELSE 
      CALL GET_COMMAND_ARGUMENT(1,namelistfile)
    ENDIF

    OPEN(8,file=namelistfile,status='OLD')
    SELECT CASE(inamelist)
    CASE(1)
      READ(8,nml=control)
    CASE(2)
      READ(8,nml=constants)
    END SELECT
   
    CLOSE(8) ! namelist file

  RETURN
  END SUBROUTINE read_namelist


  SUBROUTINE text_output
  !-------------------------------
  ! 2. OUTPUT text file 
  !-------------------------------
  
  ! ------
  ! output
  ! ------
  IF (txt_outfile=='screen') THEN
    iounit=6
  ELSE
    iounit=7
    OPEN(iounit,FILE='vectri.out')
  ENDIF

  WRITE(iounit,*) '-------------- VECTRI '//version//' ---------------'
  WRITE(iounit,*) 'run date ',now

  RETURN
  END SUBROUTINE text_output

!--------------------------------------------------------------------------

  SUBROUTINE allocate_arrays
  !
  ! allocate arrays
  !
  INTEGER :: is

  ALLOCATE(lons(nlon))
  ALLOCATE(lats(nlat))
  ALLOCATE(ralfa_x(nlon,nlat)) ! 
  ALLOCATE(ralfa_y(nlon,nlat)) !
  
  ! allocate the fields for dates (has to be after above, since nday read from rainfile
  ALLOCATE(ndate(nday)) 

  ALLOCATE(rwaterpond(nlon,nlat))   ! diagnostic 
  ALLOCATE(rwaterperm(nlon,nlat))   ! diagnostic
  ALLOCATE(rwaterurbn(nlon,nlat))   ! diagnostic
  ALLOCATE(rsoilinfil(nlon,nlat))   ! diagnostic.

  ALLOCATE(rzoophilic(nlon,nlat))
  ALLOCATE(rbitezoo(nlon,nlat))
  ALLOCATE(rvect(0:ninfv,nlon,nlat))
  ALLOCATE(rlarv(0:nlarv,nlon,nlat))
  ALLOCATE(rmasslarv(0:nlarv))

! SIT (v2.0)
  ALLOCATE(rsit_release(nlon,nlat))   ! releases 
  ALLOCATE(rsitf(nlon,nlat))   ! males
  ALLOCATE(rsitm(nlon,nlat))   ! females
! BEDNETS (v2.0)
  ALLOCATE(rbednet(nlon,nlat))   ! LLIN
  ALLOCATE(rbednet_release(nlon,nlat))   ! LLIN

  DO is=1,SIZE(soil)   
     ALLOCATE(soil(is)%vals(nlon,nlat))
  ENDDO 

  ! v1.4.0 add one to ninfh for immunity:
  ! v1.8.0 add two to ninfh for immunity without parasites
  ALLOCATE(rhost(0:nimmune2,nhost,nlon,nlat))  

  ALLOCATE(rtemp(nlon,nlat))
  ALLOCATE(rrain(nlon,nlat))
  ALLOCATE(rpopdensity(nlon,nlat))
  ALLOCATE(rwateroccupancy(nlon,nlat))

  ! safety check on the spin up:  
  IF (nloopspinup>0 .AND. nday<nlenspinup) THEN 
     WRITE(iounit,*) 'spinup period reset to integration length'
     nlenspinup=nday
  ENDIF
  RETURN

  END SUBROUTINE allocate_arrays


  SUBROUTINE set_gridded_defaults

    INTEGER :: is

    DO is=1,SIZE(soil)
      soil(is)%vals(:,:)=1./3.
    ENDDO



  END SUBROUTINE set_gridded_defaults
  
  SUBROUTINE initialize
    !--------------------------------------------------------- 
    ! VECTRI: VECtor borne disease infection model of TRIeste.
    !
    ! Tompkins AM. 2011, ICTP
    ! tompkins@ictp.it
    !
    ! initialize subroutine to initialize arrays 
    !
    !---------------------------------------------------------

    ! LOCAL VARS
    LOGICAL :: lfile
    INTEGER :: ix,iy,is
    ! INTEGER, ALLOCATABLE :: seed(:)
    REAL :: pi=3.1415926, rdx,rdy,alfa
    LOGICAL,ALLOCATABLE :: mask(:,:) 

    ALLOCATE(mask(nlon,nlat))
    !---------------
    ! Time variables 
    !---------------
    nrun=(nday+nloopspinup*INT(nlenspinup))/NINT(dt) ! NEED TO CHANGE - 3D=nday
    ndaydiag=1
    WRITE(iounit,*)' integration days: ',nday
    WRITE(iounit,*) 

    !
    ! initialization, either with file or artificial
    ! *** need *** to add SIT and BEDNETS arrays to restart file
    !
    INQUIRE(FILE=initfile,EXIST=lfile)
    IF (lfile) THEN
       WRITE(iounit,*) 'reading restart file: ',initfile
       CALL read_restart
    ELSE
       ! COLD START: idealized fixed initial conditions  
       WRITE(iounit,*) 'no restart file - modelling initializing from artificial conditions'
       rzoophilic=0.0
       rbitezoo=0.0
       rvect=0.0
       rsit_release=0.0
       rsitf=0.0
       rsitm=0.0
       rbednet=0.0
       rbednet_release=0.0
       rlarv=0.0
       rhost=0.0
       rwaterpond=wpond_min
       rwaterurbn=0.0
       rmasslarv=0.0

       ! initialization for 
       ! reservoir of malaria for initial conditions...
       ! can be overwritten later
       rhost_infect_init=MAX(MIN(rhost_infect_init,1.0),1.e-6) ! safety on range 
       rhost(0,1,:,:)=1.0-rhost_infect_init ! set % that start with parasite
       rhost(ninfh,1,:,:)=0.5*rhost_infect_init ! place half in non-immune class...
       rhost(nimmune1,1,:,:)=0.5*rhost_infect_init ! and half in immune class with parasites to give memory, otherwise they clear very quickly...
       rvect(0,:,:)=100*rhost_infect_init*rvect_min          ! fixed density of vectors
       rvect(ninfv,:,:)=10*rhost_infect_init*rvect_min          ! fixed density of vectors

       ! END OF PROGNOSTIC VARIABLES - now clean up and global initializations...
    ENDIF

    ! ----------------------------------------------------------
    ! No more automatic pop scaling, assume standard units km^-2
    ! ----------------------------------------------------------
    IF (.NOT. lpoptime) THEN
       
       !mask=(rpopdensity/=rpopdensity_FillValue)
       !IF (MAXVAL(rpopdensity,MASK=mask)>1.0) THEN
       !  WRITE(iounit,*) '%I: Population detected in m**-2 rescaling to km**-2'
       !  FORALL(ix=1:nlon,iy=1:nlat,mask(ix,iy))
       !     rpopdensity(ix,iy)=rpopdensity(ix,iy)/1.e6
       !  END FORALL
       !ENDIF
       !DEALLOCATE(mask)

       ! we use standard NF90 fill for all floats...
       WHERE(rpopdensity==rpopdensity_FillValue)
          rpopdensity=rfillValue
       ENDWHERE
    ENDIF

    ! all negative water scal fac set to zero
    WHERE(rwateroccupancy<0.0)rwateroccupancy=0.0

    !------------------------------------
    ! migration scaled to be per timestep
    !------------------------------------
    ! rmigration=rmigration*dt/rdaysinyear

    !   Initial values for vectors/larvae...

    ! mass relationship of larvae - we assume a stage 4 size as Bomblies and 
    ! we will assume a linear mass increase with age unless find other reference 
    ! this linear growth rate is very close to that assumed by Bomblies.   
    DO ix=0,nlarv
       rmasslarv(ix)=FLOAT(ix)*rmasslarv_stage4/FLOAT(nlarv)
    ENDDO

    !
    ! tranmission probabilities as a function of bite number (for speed)
    !
    rpdfvect2host=0.0
    DO ix=1,nbitepdf
       rpdfvect2host(ix)=1.0-(1.0-rptvect2host)**REAL(ix)
    ENDDO

    ! initialize the immunity gain function
    ! conversion of immune rate into tau parameter
    ! assumes 95% are immune at this value
    rimmune_gain_tau=-rimmune_gain_eira/LOG(0.05)

    ! convert annual death rate to daily 
    rpop_death_rate_daily=rpop_death_rate/365.0

    ! set the wpond_S value for puddle 130
    wpond_S=25400/wpond_CN-254

    ! place the larval progression rates into an array
    rlarvmature(1,:)=(/0.0,rlarv_ermert/)  ! temperature independent
    rlarvmature(2,:)=(/rlarv_jepson,-rlarv_tmin*rlarv_jepson/)
    rlarvmature(3,:)=(/rlarv_bayoh,-rlarv_tmin*rlarv_bayoh/)
    rlarvmature(4,:)=(/rlarv_craig,-rlarv_tmin*rlarv_craig/)

    ! Diffusion - safety first for single grid point run
    IF (nlat==1 .AND. nlon==1) THEN
       rvect_diffusion=0.0 
    ELSE
       !
       ! define the 2D dx/dy arrays for diffusion calculation
       ! in regular lat-lon grid this could be constant but make
       ! a nlon/nlat grid in order to allow for irregular grids
       DO iy=1,nlat
          ! rdx 0.1 factor a fudget to prevent unstable check failing near poles in 
          ! global runs - instability can't happen as no vectors there in any case
          ! units of rdx and rdy are KM !!!
          rdx=111.*(lons(2)-lons(1))*max(cos(lats(iy)*pi/180.),0.1)
          rdy=111.*(lats(2)-lats(1))
          alfa=2.*rvect_diffusion*dt/(rdx**2)
          IF (alfa>0.25) THEN
             STOP 'Diffusion coefficient unstable'
          ENDIF
          ralfa_x(:,iy)=alfa
          alfa=2.*rvect_diffusion*dt/(rdy**2)
          IF (alfa>0.25) STOP 'Diffusion coefficient unstable'
          ralfa_y(:,iy)=alfa
       END DO
    ENDIF

    ! initialize infiltration rates 
    DO is=1,SIZE(soil)
       SELECT CASE(soil(is)%varname)
       CASE("clay")
          soil(is)%infil=wpond_infil_clay
       CASE("silt")
          soil(is)%infil=wpond_infil_silt
       CASE("sand")
          soil(is)%infil=wpond_infil_sand
       END SELECT
    ENDDO
    rsoilinfil(:,:)=0.0
    DO is=1,SIZE(soil)
       DO iy=1,nlat
          DO ix=1,nlon
             rsoilinfil(ix,iy)=rsoilinfil(ix,iy)+ &
               & soil(is)%vals(ix,iy)*soil(is)%infil
             !   soil frac           *infil val
          ENDDO
       ENDDO
    ENDDO

    !
    ! Urban water parameterization
    !
    mask=(rpopdensity/=rfillValue)
    FORALL(iy=1:nlat,ix=1:nlon,mask(ix,iy))
       rwaterurbn(ix,iy)=log(rpopdensity(ix,iy)/wurbn_tau+1.0)*wurbn_sf   
    END FORALL

    !
    ! Set new wperm minimum value read from namelist
    !
    rwaterperm=MAX(rwaterperm,wperm_default)

    ! Are any interventions switched on?
    IF (lsit) linterventions=.TRUE.
    IF (lbednet) linterventions=.TRUE.
    IF (lspray) linterventions=.TRUE.

    ! -------------
    ! safety checks
    ! -------------
    nxdg=MAX(MIN(nxdg,nlon),1)
    nydg=MAX(MIN(nydg,nlat),1)

    RETURN
  END SUBROUTINE initialize

END MODULE mo_interface



