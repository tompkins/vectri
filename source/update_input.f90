SUBROUTINE update_input(iday)
!--------------------------------------------------------- 
! VECTRI: VECtor borne disease infection model of TRIeste.
!
! Tompkins AM. 2012, ICTP
! tompkins@ictp.it
!
! climate-pop subroutine to read in forcing data and setup output files
!
! If simple trends are requested, climate change, or fixed population growth,
! these are implemented here...
!
! 2020: Note, this can also be used for manual scaling from K to C and also 
!       to correct units of rainfall
!
!       from 1.7.3 also moved the correction of fill values here to do in one place
!       only due to separate grib and netcdf interfaces, thus changed routine name 
!       Also should move to a module soon...
!
!---------------------------------------------------------
  USE mo_constants
  USE mo_vectri
  USE mo_climate

  IMPLICIT NONE

  INTEGER, INTENT(IN) :: iday


  WHERE(rtemp==rtemp_Fillvalue)
    rtemp=rfillvalue ! set to default missing
  ELSEWHERE
    ! temperature trend and offset
    rtemp=rtemp+rtemperature_trend*REAL(iday)*dt/rdaysinyear+rtemperature_offset
  ENDWHERE

  ! Auto scaling 
  WHERE(rtemp>100.0 .AND. rtemp<500.)rtemp=rtemp-r0CtoK ! Automatic K to C Unit conversion

  WHERE(rrain==rrain_Fillvalue) 
    rrain=rfillvalue ! set to default
  ELSEWHERE
    rrain=rrain*rrainfall_factor
  ENDWHERE

END SUBROUTINE update_input

