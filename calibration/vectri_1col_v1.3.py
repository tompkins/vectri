#!/usr/bin/python
"""Dec to handle model interface for VECTRI"""
import subprocess as subproc
from scipy.io import netcdf as nc
import numpy as np
import scipy.stats as stats
import os

global nday,pop,fname_exp,VECTRI,command
#climatefile='highland_location_africa.txt'
#nday='1000'
#pop='100'

nday='0'
pop='100'
datadir='/home/netapp-clima/users/tompkins/IRI/data'

# THESE OPTIONS FOR STATION
#climatefile='/scratch/tompkins/iri/run1/kericho_030515.txt'
climatefile='kericho_030515.txt'
obsfile='plantation1.nc'
outfile='vectri.nc'

# THESE OPTIONS FOR ERA/ARC
#rainfile='/scratch/tompkins/iri/rain_1d_arc2_date19830101-20041031.nc'
#t2mfile='/scratch/tompkins/iri/t2m_1d_erai_date19830101-20041031.nc'
#outfile='vectri_daily_run_lon35.3-35.3_lat-0.37--0.4_date19830101-20041031.nc'
#obsfile='kericho_malaria_cases_adjusted_19830101-20041031'

#rainfile_ARC2_lon35.3-35.3_lat-0.37--0.4_19830101_20041030.nc'
#tempfile_ERAI_lon35.3-35.3_lat-0.37--0.4_19830101_20041030.nc'
#rainfile='kericho_station_t2m2.nc'
#t2mfile='kericho_station_rain2.nc'

VECTRI=os.environ.get('VECTRI')
fname_exp='vectri_mem'
outdir='./output/'
cor_min=0.0

def init():
    """ initialize the vars needed for the model"""
    global parfile,skilltol,command
    parfile='vectri_par.txt'
    print("-------------------------")
    print(" LOADING VECTRI AS MODEL")
    print("-------------------------")
    skilltol=1.0

def readcases(file):
    """ read timeseries from a file"""
    f=nc.netcdf_file(file,'r')
    cases=f.variables['cases']
    #print('reading from file ',file, 'cases ',cases[:])
    f.close()
    return cases

def skill(exp,obs,opt=1):
    """ calculate a skill metric """

    # R**2 set to correlation score
    if (opt==1): 
        res=stats.pearsonr(obs,exp)
        skill=res[0]*res[0] if res[0]>cor_min else 0.0
        p=res[1]

    # RMS error - 
    # scale to ensure mean is correct, since obs has cases
    # and model has cases per 1000 - so we scale as we don't 
    # know the catchment, and then RMS judges the variability :-)
    if (opt==2):
        skill=np.sqrt(((exp-obs*exp.mean()/obs.mean())**2).mean())
        skill=1.0/(1.0+skill)
        p=0.0 # need to add
    return skill,p
 
def runmodel():
    """ runs a single vectri instance"""        
    command=['{}/scripts/vectri_driver'.format(VECTRI),'5',climatefile,pop,nday]
    #command=['{}/scripts/vectri_driver'.format(VECTRI),'3','-0.37','1','0.1','35.3','1','0.1','19830101','20041031','0',rainfile,t2mfile,'run','0','0',pop,'1']
    #print(command)
    with open('./input/out-file.txt', 'w') as f:
        subproc.call(command, stdout=f)

def exact():
    """make one run with the correct values"""
    # make sure there is an empty vectri.options file
    subproc.call(['rm','-f','./input/vectri.options'])

    # FUDGE for moment - add 4C to default T
    f=open('./input/vectri.options','w') # overwrites existing file
    f.write("rtemperature_offset=0.0")
    f.close()
    runmodel()

def skill_exact(imem):
    """ calculate skill of model compared to perfect run"""

    # check if perfect run is available:
    fname='{}/vectri_exact_pop{}_nday{}.nc'.format(outdir,pop,nday)
    if not os.path.isfile(fname): 
        print("I: making DEFAULT run")
        exact()
        os.rename('./output/vectri.nc',fname)
    
    print("exact skill needs rewriting")
    exit()

    for run in ['c','e']:
        f1,f2='{}/eir_{}.nc'.format(outdir,run),'{}/eir_{}_mn.nc'.format(outdir,run)
        f3,f4='{}/eir_{}_ydmn.nc'.format(outdir,run),'{}/eir_{}_ydstd.nc'.format(outdir,run)
        f5='{}/eir_{}_seas.nc'.format(outdir,run)
        subproc.call(['cdo','-s','timmean',f1,f2])
        subproc.call(['cdo','-s','ydaymean',f1,f3])
        subproc.call(['cdo','-s','timstd',f3,f4])
        subproc.call(['cdo','-s','div','-sub',f3,f2,f4,f5])

    # skill calc here
    
    return sk

#-------------------------------------------------------------
def skill_data(imem):
    """ calculate skill of model compared to observed cases"""

    # call skill bash script
    expfile='{}{}'.format(fname_exp,imem)

    # Make the monmeans and detrended timeseries
    subproc.call(['./case_process.bash',outdir,expfile])

    #
    # parents with Cases=0 receive a massive penalization
    # so they can not have children.
    #
    sk,p=0.0,1.0 # default skillless model
    f=nc.netcdf_file('{}/cases_timmean.nc'.format(outdir), 'r')
    cases_mean=f.variables['cases'][0]
    f.close()

    # only bother to calculate score if cases are simulated
    if cases_mean>1.0e-8:

        # update name to full 
        expfile=expfile+'_cases_monmean'

        #
        # Read in model and data 
        # 
        # basic correlation
        obsdata=readcases(outdir+obsfile+'.nc')
        expdata=readcases(outdir+expfile+'.nc')

        # can call several times and do weighted average
        # opt=1 R**2, opt=2 RMS error
        sk,p=skill(obsdata[:],np.ravel(expdata[:]),opt=1)

    # Completely Ad-hoc score replaced by probability
    return sk,p

def run(imem,vals):
    """run the model"""

    # make the options file
    subproc.call(['mkdir','-p','./input'])
    f=open('./input/vectri.options','w') # overwrites existing file
    f.write("nyearspinup=3\n") # include a spin up
    f.write("rhost_infect_init=0.5\n") # 
    for val in vals:
        f.write("{}={}\n".format(val,vals[val]))
    f.close()

    runmodel()

    subproc.call(['mv','-f','{}/{}'.format(outdir,outfile),'{}/{}{}.nc'.format(outdir,fname_exp,imem)])
