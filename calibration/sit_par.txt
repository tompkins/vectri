name,                    default, tol,   min,   max
init,                    90,      1000,    1,    365,
nrel,			 5,	  1000,    1,    100
len,                     100,     1000,    1,    365,
ratio,                   1,       1000,   0.1,  10,
