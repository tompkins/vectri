#!/bin/bash

sit_pars='vectri_sit_pars.txt'

veclist="1 1e2 3e2 1e3 3e3 1e4 3e4 1e5 3e5 1e6 3e6"
for nvec in $veclist  ; do
    echo $nvec
    echo "name, default, tol, min, max" > $sit_pars
    echo "init,                    90,      90,      1,    365" >> $sit_pars
    echo "nrel,                    5,       5,       1,    20" >> $sit_pars
    echo "len,                     100,     100,     1,    365" >> $sit_pars
    echo "ratio,                   1,       1,     0.1,   10" >> $sit_pars
    echo "nvec,                  "${nvec}",    0,     "${nvec}",   "${nvec} >> $sit_pars
    python3 genetic.py --nens=4 --ngen=2
    odir=./output_nvec${nvec}/
    mkdir -p ./output_nvec${nvec}/
    mv skill_series.json final_skill.csv final_parameters.csv model_parameters_stat.csv model_parameters_ens.csv $odir
done
