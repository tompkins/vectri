#!/usr/bin/python3
import pandas as pd
import xarray as xr
import numpy as np
import os
from scipy.stats import binom,linregress
import subprocess as subproc 

def vname(imem,igen):
    return('./output/vectri_mon_gen{}_mem{}.nc'.format(igen,imem))

def init():
    """ initialize the vars needed for the model"""
    global parfile,skilltol,command
    global psens,pspec,biasfile

    rdttype='paracheck'
    
    # probably need to specify these externally for user calibration

    #
    # RDT results, Hopkins et al. Am. J. Trop. Med. Hyg., 76(6), 2007, pp. 1092–1097
    #
    #psens={"rdt":0.89,"micro":???}
    #pspec={"rdt":0.95,"micro":???}
    #
    # Doctor et al. 2016
    # Malaria Surveillance in the Democratic Republic of the Congo: Comparison of Microscopy, PCR, and Rapid Diagnostic Test
    # Diagn Microbiol Infect Dis. 2016 May; 85(1): 16–18. 
    #
    #psens={"RDT":0.869,"Microscopy":0.767}
    #pspec={"RDT":0.881,"Microscopy":0.972}
    #
    #Usefulness of an Inexpensive, Paracheck® Test in Detecting Asymptomatic Infectious Reservoir of Plasmodium falciparum During Dry Season in an Inaccessible Terrain in Central India
    #https://doi.org/10.1053/jinf.2002.1055

    ###psens={"rdt":{"paracheck":0.869,"parahit":0.0,"optimal":0.0,"falcivax":0.0},"micro":{"study1":0.767}}
    ###pspec={"rdt":{"paracheck":0.881,"parahit":0.0,"optimal":0.0},"micro":{"study1":0.972}}

    # Field and laboratory comparative evaluation of rapid malaria diagnostic tests versus traditional and molecular techniques in India
    # Malaria Journalvolume 9, Article number: 191 (2010) 
    
    # Guthmann, Jean-Paul, et al. "Validity, reliability and ease of use in the field of five rapid tests for the diagnosis of Plasmodium falciparum malaria in Uganda." Transactions of the Royal Society of Tropical Medicine and Hygiene 96.3 (2002): 254-257.    : paracheck and parahit 0.97 and 0.88 compared to slide

    # TURN OFF specificity and sensitivty for the moment as they push the model towards no malaria
    # need to consider how to reduce this effect in areas obviously too cold for tranmission.  
    psens={"RDT":1.0,"Microscopy":1.0}
    pspec={"RDT":1.0,"Microscopy":1.0}

    # open a file to store the biases.
    biasfile=open("bias_file.txt","w")

    parfile='vectri_par.txt'
    print("-------------------------")
    print(" LOADING VECTRI AS MODEL")
    print("-------------------------")

def skill(imem,igen,sfile,opt=0):
    if opt==0:
        stats=skill_map(imem,igen,sfile)
    if opt==1:
        stats=skill_corr(imem,igen,sfile)
    
def skill_corr(imem,igen,sfile):
    """routine to calculate a correlation skill for a vectri timeseries"""
    """pval calculated from correlation"""
        
    ncfile=vname(imem,igen)
    vectri=xr.open_dataset(ncfile)

    return(stats)


def skill_map(imem,igen,sfile):
    """routine to calculate skill of gridded vectri run for calibration"""
    """the code reads in a gridded run output and compares for each time"""   
    """the difference to the PR in the MAP collected survey data"""
     
    biasfile.write("imem,"+str(imem)+",igen,"+str(igen)+"\n")

    vectri_dir=os.environ['VECTRI'] # defunct.

    # NOTE: this output name has to match the one before
    ncfile=vname(imem,igen)
    vectri=xr.open_dataset(ncfile)

    # read in the CSV file of the MAP data
    # specify use cols for efficiency
    df = pd.read_csv(sfile, 
         usecols=["latitude","longitude","month_start","year_start",
                "month_end","year_end","continent_id","country_id",
                "examined","pf_pos",
                  "pf_pr","malaria_metrics_available","location_available",
                  "method","rdt_type"],
                low_memory=False)

    obs_lat,obs_lon = df.latitude,df.longitude
    obs_mon1,obs_year1 = df.month_start,df.year_start
    obs_mon2 = df.month_end
    obs_year2 = df.year_end
    obs_continent=df.continent_id
    obs_country=df.country_id
    obs_prlist=df.pf_pr
    obs_N=df.examined
    obs_P=df.pf_pos
    obs_method=df.method
    flag1=df.malaria_metrics_available
    flag2=df.location_available
    
    #
    # loop over csv to get RMSE error, bias, and pval
    #
    # pval, calculated using a binomial distribution
    # assume the vectri value is the truth and then 
    # calculate the probability of the sample result
    # Thus larger samples will have a higher "weighting"
    # and no tolerance needs to be set. 
    # GA will adjust VECTRI PR results to maximize pval. 
    #
    bias,pval,smodpr,sobspr=[],[],[],[]
    for i,obs_pr in enumerate(obs_prlist):
        # data is present and in Africa/ethiopia
        check=flag1[i] and flag2[i] and obs_continent[i]=='Africa'
        #check=flag1[i] and flag2[i] and obs_country[i]=='ETH' # uncomment for single country (faster)
        if (check):
            date1=str(int(obs_year1[i]))+'-'+str(int(obs_mon1[i]))+'-01'
            date2=str(int(obs_year2[i]))+'-'+str(int(obs_mon2[i]))+'-28'
            # this selects nearest point with a tolerance of one degree
            # if there are no simulation points within 1 degree of the location then failure is 
            # captured in the exception
            try:
                mod_pr=vectri.PRd.sel(lon=obs_lon[i],lat=obs_lat[i],method="nearest",tolerance=1.0)
            except: 
                continue  # this obs is outside the simulation zone so loop to next...

            mod_pr=mod_pr.sel(time=slice(date1,date2))
            
            # safety to ensure date was within range of model run...
            if(len(mod_pr)>0):
                vectri_pr=mod_pr.mean()
                if (vectri_pr<1e-8):
                    vectri_pr=0.0

                biasfile.write(str(obs_pr)+","+str(vectri_pr)+","+str(vectri_pr-obs_pr)+"\n")

                bias.append(vectri_pr-obs_pr)
                smodpr.append(vectri_pr)
                sobspr.append(obs_pr)
                ppos=vectri_pr*psens[str(obs_method[i])] + (1.0-vectri_pr)*(1-pspec[str(obs_method[i])])
                pbinom=binom.pmf(obs_P[i],obs_N[i],ppos)
                pbinom=np.log10(max(1.e-50,pbinom)) # prevent underflow.
                pval.append(pbinom)
                #print(imem,i,obs_year1[i],vectri_pr,ppos,obs_P[i]/obs_N[i],obs_P[i],obs_N[i]," my prob ",pbinom)

    # rms is the rmse of the PR value: our skill metric!
    bias=np.asarray(bias)
    slope, intercept, r_value, r2pval, std_err = linregress(sobspr, smodpr)
    pval=np.sum(np.asarray(pval,dtype=np.float128))
    print("processing mem",imem,"number of obs",bias.size,"log pval sum",pval)

    rmse=np.sqrt(np.mean(bias**2))
    bias=np.mean(bias)
    pval=np.log10(pval,dtype=np.float128).sum()  # sum the log10 of the pvals 

    # stats are returned as a dictionary rather than a list:
    stats={"rmse":rmse,"bias":bias,"bpval":pval,"r2pval":r2pval,"rval":r_value}  
    return(stats) 
    
def run(imem,igen,vals):  
    """run an ensemble member of the model"""
    #
    # make the options file for this member
    #
    indir='./input_mem{}/'.format(imem)
    ofile='./output/vectri_mem{}.nc'.format(imem)
    subproc.call(['mkdir','-p',indir])
    subproc.call(['mkdir','-p','./output'])
    subproc.call(['rm','-f',ofile]) # clean file to be sure
    f=open(indir+'vectri.options','w') # overwrites existing file
    f.write("nloopspinup=1\n") # include a spin up
    #f.write("rhost_infect_init=0.5\n") # 
    for val in vals:
        f.write("{}={}\n".format(val,vals[val]))
    f.close()
    #
    # run the model 
    #
    vdir=os.environ['VECTRI']
    command=[vdir+'/vectri','-r',indir,'-c','./clim_calibration.nc','-d','./pop_calibration.nc','-o',ofile,'-n',indir+'vectri.namelist']
    with open(indir+'out-file{}.txt'.format(imem), 'w') as f:
        subproc.call(command, stdout=f)
    print ("model run",imem,"complete")
    subproc.call(['cdo','-s','monmean',ofile,vname(imem,igen)])

