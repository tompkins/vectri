In this directory there are the routines needed for calibration of the model. Still under development for the gridded case.

genetic.py: is a python code for a simple constrained genetic algorithm for parameter setting.

vectri_par.txt : is a text list of the input parameters that can be tuned by the process. The first guess value and tolerance is required, along with a hard bounds.

vectri.py : this contains two interface routines, one to run the model (in genetic.py, there is a generic model interface) and one to calculate the "skill" of a particular model run.  Probably it would be better to split this.  The skill calculation in this code is presently for a single location timeseries

For more details on the GA method, see

Tompkins, Adrian M., and Madeleine C. Thomson. "Uncertainty in malaria simulations in the highlands of Kenya: Relative contributions of model parameter setting, driving climate and initial condition errors." PloS one 13.9 (2018): e0200638.