#!/bin/python

import os 
from glob import glob
import subprocess as subproc 
import multiprocessing
import getopt, sys, ast

def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts

def main(argv):

    Lparallel=True
    Lisimip=True

    # get the queue string
    try:
        opts, args = getopt.getopt(argv,"h",["model=","rcp="])
    except getopt.GetoptError:
        print (argv)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h","--help"):
            print("pass the queue string")
            sys.exit()
        elif opt in ("--model"):
            model=arg
        elif opt in ("--rcp"):
            rcp=arg
  
    datadir="/home/netapp-clima-scratch/tompkins/isimip/"

    files=glob(datadir+"/"+model+"/"+rcp+"/*/vectri*.nc")

    runs=[]

    # get all SSP scenarios used apart from GRUMP for this rcp, this is for output pp
    scenlist=[]
    for file in files:
        p=splitall(file)
        scenlist.append(p[7])
    scenlist=set(scenlist)
    if "grump" in scenlist: scenlist.remove("grump")

    # now we will loop over the files
    for file in files:
        p=splitall(file)
        daterange=p[8][-20:-3] 
        if (Lisimip): # only matching scenarios.
            if p[7]!="grump":
                runs.append({"model":p[5],"rcp":p[6],"popin":p[7],"popout":p[7],"dates":daterange})
        else: # processes all (nature paper)
            if p[7]=="grump":
                for scen in scenlist:
                    runs.append({"model":p[5],"rcp":p[6],"popin":p[7],"popout":scen,"dates":daterange})
            else:
                runs.append({"model":p[5],"rcp":p[6],"popin":p[7],"popout":p[7],"dates":daterange})

#models="GFDL-ESM2M  HadGEM2-ES IPSL-CM5A-LR  MIROC5"
#rcps="rcp26  rcp45  rcp60  rcp85"
#popsin="grump ssp1 ssp2 ssp5"
#popspp="hist ssp1 ssp2 ssp5"
    for run in runs:
        print("list entry: ",run)

    #
    # parallel calls over the run cluster 
    #
    if Lparallel:
        ncore=int(multiprocessing.cpu_count())
        pool=multiprocessing.Pool(processes=ncore)
        pool.starmap(pp_files,([run] for run in runs)) # initerr
        pool.close()
        pool.join()
    else:
        for run in runs:
             pp_files(run)

def pp_files(run):
    command=["/home/netapp-clima/users/tompkins/ISIMIP2/scripts/post_process.bash",run["model"],run["rcp"],run["popin"],run["popout"],run["dates"]]
    print (command)
    subproc.call(command)


if __name__ == "__main__":
    main(sys.argv[1:])


