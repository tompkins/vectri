#!/bin/bash

fname(){
        # full in/out name can change:
        # echo $thisdir/vectri_${lmodel}_${rcp}${2}_co2_${1}_global_annual_${year1}_${year2}.nc4
        # fudge for isimip assuming in=out for pop
        echo $thisdir/vectri_${lmodel}_${rcp}_${2}_co2_${1}_global_annual_${year1}_${year2}.nc4
     }

cleanfile(){

  local myfile="$1"
  local oldname="$2"
  local newname="$3"
  local longname="$4"
  local units="$5"

  # att_nm, var_nm, mode, att_type, att_val

# first set the variable name itself
  
  if [[ $oldname != $newname ]] ; then
    ncrename -h -O -v $oldname,$newname $myfile 
  fi
  ncatted -h -a standard_name,$newname,o,c,"$newname" $myfile
  ncatted -h -a long_name,$newname,o,c,"$longname" $myfile
  ncatted -h -a units,$newname,o,c,"$units" $myfile

# now add global atrributes
  ncatted -h -O -a "climate model",global,c,c,${model}"\n" $myfile
  ncatted -h -O -a "scenario",global,c,c,${rcp}"\n" $myfile
  ncatted -h -O -a "population density used by VECTRI (input)",global,c,c,${popin}"\n" $myfile
  ncatted -h -O -a "population density used for post-processing",global,c,c,${poppp}"\n" $myfile
}

dir=/home/netapp-clima/scratch/tompkins/isimip/
popdir=/home/netapp-clima/users/tompkins/ISIMIP2/population/
#diro=/home/tompkins-archive-b/tompkins/ISIMIP2/output
#odir=/home/esp-shared-a/Distribution/Users/tompkins/isimip
odir=$dir/postproc/
wdir=$dir/wrk
mkdir -p $odir $wdir

#models="GFDL-ESM2M  HadGEM2-ES	IPSL-CM5A-LR  MIROC5"
#rcps="hist rcp26  rcp45  rcp60  rcp85"
#popins="grump hist ssp1 ssp2 ssp5"
#poppps="hist ssp1 ssp2 ssp5"

models=$1 ; rcps=$2 ; popins=$3 ; poppps=$4 ; dates=$5 

# stage 1: eir, lts, and pr
# stage 2: the 3 par (that depend on lts)
stage=2


# comment this if you want to loop.
echo $models $rcps $popins $poppps $dates

nwinm1=2
nwin=3

for model in $models ; do 
for rcp in $rcps ; do 
for popin in $popins ; do  # input run pop
for poppp in $poppps ; do  # postproc pop 

   lmodel=`echo $model | tr A-Z a-z`

   ddir=$dir/$model/$rcp/$popin/
   #thisdir=$odir/$model/$rcp/ ; mkdir -p $thisdir
   thisdir=$odir ; mkdir -p $thisdir # flat structure for isimip

   #wdirh=$wdir/wrk_$(tr -dc A-Za-z0-9 </dev/urandom | head -c 8)
   cd $wdir
   
   # now one file only (replace with wild cards in running on desktop with loop)
   #files=`ls ${ddir}/vectri_${model}_${rcp}_${popin}_?????????????????.nc`
   files=`ls ${ddir}/vectri_${model}_${rcp}_${popin}_${dates}.nc`
   #files=${ddir}/vectri_${model}_${rcp}_${popin}_${dates}.nc

   # population_ssp5soc_0p5deg_daily_20310101-20401231_density.nc4

   for file in $files ; do

   # get correspondign pop file:
   # vectri_MIROC5_rcp85_ssp2_20510101-20601231.nc

     date=${file: -20:17}
     year1=${file: -20:4}
     year2=${file: -11:4}
     echo $year1 $year2

     popfile=$popdir/population_${poppp}soc_0p5deg_annual_${date}.nc4
     denfile=$popdir/popdensity_${poppp}soc_0p5deg_annual_${date}_perkm.nc4

     # zip level 
     zip=zip_1
     cdo="cdo --no_history -f nc4 -z $zip"

     # NON - post proc pop - change to "nosoc" if using full runs.
     feir=$(fname eir ${popin}soc )
     flts=$(fname lts ${popin}soc )
     fpr=$(fname pr ${popin}soc )

     # we will leave some tmp files for later as they are needed for several output combos... to speed up (selvar slow!)
     # NOTE: no poppp here, so add where needed later to avoid clashes.
     tmp=tmp_${model}_${rcp}_${popin}_${poppp}_${dates}
     
     # EIR
     if [ $stage -eq 1 ] ; then 
       if [ ! -f $feir ] ; then 
         $cdo selvar,eir $file ${tmp}_eir.nc # this will keep for other runs
         $cdo yearsum ${tmp}_eir.nc $feir
         cleanfile $feir eir eir "Entomological inoculation rate" "per person per year" $model $rcp $popin "Not Applicable"
       fi

       # LTS bases on EIR (from caminade 2014)
       if [ ! -f $flts ] ; then
         $cdo gec,0.001 ${tmp}_eir.nc ${tmp}_mask.nc 
         $cdo --timestat_date first yearsum ${tmp}_mask.nc $flts
         cleanfile $flts eir lts "Length of Transmission Season" "days" $model $rcp $popin "Not Applicable"
         rm -f ${tmp}_mask.nc
       fi

       # Prd 
       if [ ! -f $fpr ] ; then
         $cdo yearmean -selvar,PRd $file $fpr
         cleanfile $fpr PRd pr "Parasite Ratio (prevalence)" "fraction" $model $rcp $popin "Not Applicable"
       fi 

     else

       # PARANN:  population at risk if LTS > 1 month in a particular year
       fparann=$(fname parann ${poppp}soc )
       if [ ! -f $fparann ] ; then
         $cdo gec,30 $flts ${tmp}_ltsgt30.nc
         $cdo mul ${tmp}_ltsgt30.nc $popfile $fparann
         cleanfile $fout lts parann "Persons at risk (lts>1 month in one year)" "number" $model $rcp $popin $poppp
       fi

       rm -f ${tmp}*.nc
       #PAR: population at risk if LTS > 1 month over previous 3 years.
        
       fpar=$(fname par ${poppp}soc ) 
       if [ ! -f $fpar ] ; then 
         $cdo gec,30 $flts ${tmp}_ltsgt30.nc
         $cdo -seltimestep,2/${nwin} ${tmp}_ltsgt30.nc ${tmp}_1.nc
         $cdo -shifttime,-${nwin}years ${tmp}_1.nc ${tmp}_chunk.nc
         $cdo mergetime ${tmp}_chunk.nc ${tmp}_ltsgt30.nc ${tmp}_long.nc # 1=LTS>30 in that year
         $cdo --timestat_date last runsum,${nwin} ${tmp}_long.nc ${tmp}_sum.nc # total cases in 3 years
         # 1 if LTS>30 in any of previous three years:
         $cdo -gec,1 ${tmp}_sum.nc ${tmp}_mask.nc
         $cdo -mul  ${tmp}_mask.nc $popfile $fpar
         cleanfile $fout lts par "Persons at risk (lts>1 month in any of previous 3 years)" "number" $model $rcp $popin $poppp
       fi 

       rm -f ${tmp}_*.nc

       # PARWHO: population at risk if >1 case in last 3 years
       fparwho=$(fname parwho ${poppp}soc )
       if [ ! -f $fparwho ] ; then
         $cdo selvar,cases $file ${tmp}_cases.nc # total # cases per km in each cell
         $cdo yearsum ${tmp}_cases.nc ${tmp}_cases_ys.nc
         $cdo mul ${tmp}_cases_ys.nc $denfile ${tmp}_cases_ann.nc # total # cases per km in each cell
         $cdo seltimestep,2/${nwin} ${tmp}_cases_ann.nc ${tmp}_1.nc 
         $cdo shifttime,-${nwin}years ${tmp}_1.nc ${tmp}_chunk.nc 
    
         # allow running mean to produce same length
    
         $cdo mergetime ${tmp}_chunk.nc ${tmp}_cases_ann.nc ${tmp}_cases_long.nc
         $cdo gec,1 ${tmp}_cases_long.nc ${tmp}_mask.nc # if the cases per yearexceeds 1 per km, then mask is 1.
         $cdo --timestat_date last runsum,${nwin} ${tmp}_mask.nc ${tmp}_mask2.nc # total cases in 3 years
         $cdo gec,1 ${tmp}_mask2.nc ${tmp}_2.nc
         $cdo mul ${tmp}_2.nc $popfile $fparwho

         cleanfile $fout cases parwho "Persons at risk (WHO definition)" "number" $model $rcp $popin $poppp
       fi 
     fi
 
     rm -f ${tmp}_*.nc

# we are most certainly done:
done ; done ; done ; done ; done 


