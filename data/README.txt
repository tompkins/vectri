EIRa_WAvalidata_ermert_etal_2011b.txt
EIR data for West Africa compiled for the PhD thesis of Volker Ermert from the open literature (Ermert 2011 for details)


era_geo_an.grb
Geopotential height field for ERA for dynamical downscaling (obs)

example_data.nc
example_sys5.grb
example_sys5.nc

Example seasonal forecast from ECMWF and Afripop population density projected onto the same grid, try a run with $VECTRI/vectri -c example_sys5.nc -d example_data.nc

vectri_fake_clim.nc
vectri_fake_data.nc

Two fake climate and population density files for a single location. This is dummy data to test the model. try
$VECTRI/vectri -c vectri_fake_clim.nc -d vectri_fake_data.nc

public_pf_data.csv

this is a CSV file of PR2-10 parasite ratio data downloaded from the MAP Oxford websiote on 5th June 2019. All data are taken from the open literature and references are contained in the file.
