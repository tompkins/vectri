VECTRI
=======

**VECTRI**  is a open source gridded model for the climate-sensitive life-cycle of mosquito vectors and the transmission of vector borne diseases, written and distributed by the Abdus Salam International Centre for Theoretical Physics, Trieste. (ICTP).
