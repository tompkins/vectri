.. VECTRI documentation master file, created by
   sphinx-quickstart on Wed Sep 18 22:29:29 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VECTRI's documentation!
==================================

.. toctree::
   :numbered:
   :maxdepth: 3
   :caption: Contents:

   Prologue
   Prerequisites
   Running
   Output
   Input
   Model
   Parameters
   Rerun
   Data
   Interventions
   Toolbox
   Calibration


#Indices and tables
#=================

#* :ref:`genindex`
#* :ref:`search`
