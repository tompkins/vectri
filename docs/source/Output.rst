Output
======

The main output file is called vectri.nc which is found in the run directory (unless you chose to rename it with the -o option).

The file is in standard netcdf format and you can use two commands to examine its content `ncview <https://cirrus.ucsd.edu/ncview/>`_ and ``ncdump <<https://www.unidata.ucar.edu/software/netcdf/workshops/2011/utilities/Ncdump.html>``_.

NCDUMP
--------

This command lets us know the dimensions of the output, gives a list of the model output variables, as well as their metadata.  We will be using it a lot to interrogate netcdf files!

.. tip::

   ncdump outputs the entire contents of a file, perhaps a little more than you bargained for! So use the useful option is ``-h`` to examined the file *header*.

Remember, if you don't know what a netcdf file is, check out our `online video introduction <https://www.youtube.com/watch?v=UvNBnjiTXa0>`_.  

So let's try examining the output using this command

.. code-block ::

    ncdump -h vectri.nc

.. tip::

   sometimes the header is very long - to save yourself having to scroll up and down, it is helpful to pipe the output of ncdump into the command less
   .. code-block::
      
      ncdump -h vectri.nc | less
      

The output shows the dimensions first, and then the dump provides a list of all the model run global attributes. Here you find stored all the parameter settings used to run the model. We will see how to change this in the :doc:`../Input` section, while the parameters themselves are outlined in the :doc:`../Parameters` section.  Finally the output fields follows, which are grouped under the titles of **vector**, **disease**, **interventions**, **hydrology**, and **input**, which will be introduced next. 

.. note::
   The exact github tag release used is stored in the global attributes, along with the run command and all the simulation parameter settings. This is to ensure that, even if the model is upgraded, you should be able to download the exact code version and reproduce your results later, essential for reeproducability of published results!


NCVIEW
--------

We can now open the output using the ``ncview`` utility. Try this out now!

.. code-block::

   ncview vectri.nc

This should open a window with the available output groups, looking like this:

.. image:: ../images/ncview1.png
  :width: 400
  :alt: Alternative text

You can see that the output is divided into key groups, which in the default run consist of **vector**, **disease**, **hydrology** and **input**.  If you are using interventions, there will also be a 5th group call **interventions**. 

	
Output Variables
----------------
We introduce the VECTRI output for each group in turn as well as the switch to control if the output is produced.  We will show you how to change that setting in the :doc:`../Input` section. 


.. csv-table:: VECTRI Output: vector group 
  :header: "Name", "Description", "Units", "Switch name", "Default"
  :width: 65%
  :widths: auto
  :align: left
	  
	  "vector ",  "vector density", "m :sup:`-2`","loutput_vector",".true."
	  "larvae ",  "larvae density", "m :sup:`-2`","loutput_larvae",".false."
	  "lbiomass",  "larvae biomass", "mg m :sup:`-2`","loutput_lbiomass",".false."
	  "hbr ",  "human bite rate", "day :sup:`-1` person :sup:`-1`","loutput_hbr",".false."
	  "eggs","Density of new eggs laid per day", "m :sup:`-2` day :sup:`-1`","loutput_egg",".false."
	  "emergence","Emergence rate of new vectors", "m :sup:`-2` day :sup:`-1`","loutput_emergence",".false."
          "vecthostratio","vector to host ratio","","loutput_vecthostratio",".false."

.. csv-table:: VECTRI Output: disease group 
  :header: "Name", "Description", "Units", "Switch name", "Default"
  :widths: 10 5 10 20 10
  :width: 25%
  :align: left

          "PR","Parasite Ratio (malaria)"," ","loutput_pr", ".false."
          "PRd","Detectable Parasite Ratio (malaria) [assumes ten days before positive test]", "","loutput_prd",".true. [IVEC=0:9]"
	  "cspr","circumsporozoite protein rate - proportion of infective vectors (malaria)", "","loutput_cspr",".false."
	  "eir","Entomological Inoculation Rate (malaria)","day :sup:`-1` person :sup:`-1`","loutput_cspr",".true. [IVEC=0:9]"
	  "cases","Symptomatic cases (malaria)","day :sup:`-1` person :sup:`-1`","loutput_cases",".false. [IVEC=0:9]"
	  "immunity","Proportion of population with immunity (malaria)","","loutput_immunity",".false."

.. csv-table:: VECTRI Output: interventions group 
  :header: "Name", "Description", "Units", "Switch name", "Default"
  :width: 65%
  :widths: auto
  :align: left
	  
	  "sit_males ",  "SIT vector density", "m :sup:`-2`","loutput_sit",".false. (switched on if intervention found)"
	  "sit_females ",  "vector density of females that had bred with a SIT male", "m :sup:`-2`","loutput_sit",".false."


.. csv-table:: VECTRI Output: hydrology group 
  :header: "Name", "Description", "Units", "Switch name", "Default"
  :width: 65%
  :widths: auto
  :align: left
	  
	  "wperm",  "Fractional coverage of breeding sites on boundaries of permanent water bodies", "fraction","loutput_wperm",".false."
	  "wurbn",  "Fractional coverage of breeding sites in urban environment, not rain fed", "fraction","loutput_wurbn",".false."
	  "wpond",  "Fractional coverage of temporary rain-fed breeding sites (rural puddles/ponds and urban rainfed sites) ", "fraction","loutput_wpond",".false."
	  "infiltration",  "Soil Infiltration Rate", "mm day :sup:`-1`","loutput_soilinfil",".false."
	  
.. csv-table:: VECTRI Output, input group (duplicates driving fields for convenience) 
  :header: "Name", "Description", "Units", "Switch name", "Default"
  :width: 65%
  :widths: auto
  :align: left
	  
	  "population_density",  "population density", "m :sup:`-2`","loutput_population",".true."
	  "RAIN :sup:`(a)`",  "Precipitation", "mm day :sup:`-1`","loutput_rain",".true."
	  "TEMPERATURE :sup:`(a)`",  "Two meter temperature", "deg C :sup:`(b)`","loutput_t2m",".true."

.. note::
    (a) note that rain and temperature output names are taken from the driver file used for input and not changed
    (b) temperature units are converted automatically to deg C if the input is in Kelvins. 
	  
.. tip::
   For some purposes you may wish to have the output file "flattened", that is, without the group hierarchical structure.  For example, CDO does not work on netcdf4 files with the group structure currently. In this case you will need to convert the output to a netcdf3 file format which is easy to do with nco: ``ncks -3 vectri.nc vectri_3.nc``. If you open the new created file vectri_3.nc in ncview you will notice that the group structure is gone and the variables are contained in a single simple block.

