Simulation nitialization
========================

VECTRI simulations may be for a variety of purposes and thus have simulation lengths for varying timescales. VECTRI has been used both in a seasonal forecasting context, where simulations are usually for a seasonal and last three to six months, but it has also been applied to model transmission in both the present and potential future climate scenarios.


Cold start
----------

This is the simplest default method of starting a simulation.  If you do not specify an initial condition file or change any parameter, the model initializes with fixed specific conditions of 10\% (set by parameter ``rhost_infect_init``) of the human population positive for malaria and a vector population density that is 110 times the background minimum specified by ``rvect_min`` =10 :sup:`-4` m :sup:`-2`, and with a CSPR rate of 10\%.


Spinning up the model 
---------------------
Tompkins and Thomson (PLOS1, 2018) show that the simulation initial conditions are essential "forgotten" within 6 to 24 months of the simulation start, with the time depending on temperature (the adjustment is longer at colder temperatures). Obviously, if one is using the model in a climate change context, it would be possible to ignore the first 1 or 2 years of the simulation.  However, a better method is to spin up the model.  This is done using the parameters ``nlenspinup`` and ``nloopspinup``.  Essentially if these are set to non-zero values, the model uses the climate information for the first ``nlenspinup`` days and drives the model starting from the cold start initial conditions for this period ``nloopspinup`` times. The output from these loops is discarded and the output file then saves the output from the ``nloopspinup+1``:sup:`th` integration, which then continues as usual for the whole driving file period.

Example, let's say we have a climate file with ten years of climate information. If we run with:

.. code-block ::

    vectri -v,"nlenspinup=365,nloopspinup=2"


This would use the first year of climate data and run this year twice, throw these two years away and then the output from the third integration of this year would be saved in the file along with the output from year 2-10.  The output file has exactly ten years length, but the initial conditions are those after the two years "spin up".  

Being able to set the loop length and looping number give you quite a lot of flexibility. For example, if you only have a 6 month seasonal forecast driving file, spinning up over a period of a year is not possible, in this case you could spin up using only the first month but increase the loop number, e.g. To loop the first month 12 times (i.e. a year spin up). 

.. code-block ::

    vectri -v,"nlenspinup=30,nloopspinup=12"



Restarting the model
--------------------

For seasonal forecasting purposes, the initial conditions really should reflect the conditions that have occurred *prior* to the simulation period.  While some experiments into malaria seasonal forecasting with the LMM have simply used the spin up method, this is not very accurate.  An improvement therefore is to use the VECTRI restart facility.  After a simulation has completed, in addition to the output file ``vectri.nc``, there is also a second file created in the ``input`` directory called ``restart_vectri.nc``.  This contains a dump of the model arrays and can be used to restart the model using the ``-i`` command line option. thus

.. code-block ::

    vectri -i ./input/restart_vectri.nc

Thus you can using a reanalysis/observations-driven vectri model to simulation the period up to near real-time, creating a restart file with which one can then initiate a seasonal forecast (this was the method used in the Tompkins et al. 2019 GeoHealth paper investigating seasonal forecasting in Uganda for example).  Another application of these restart files is when you wish to drive the model for climate change experiments where the climate data is divided into "chunks".

.. warning ::

   When the restart facility was introduced, we made an effort to ensure it was bit reproducible, that is, if you divided a climate file into two halves, and run two separate simulations using the restart facility, the second half of the simulation should be bit-indentical to the result you would get running straight through with the single original file without restarts.  However, we must admit that this feature is not part of our model "nose-testing" (it should be!) and the bit reproducibility may sometimes be broken with new developments. If this is important to you, and you find bit reproducibility is not functioning, please flag it to us and we will try to fix it!
