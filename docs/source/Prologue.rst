Prologue
========

by Adrian Tompkins

Thank you for your interest in VECTRI!  Before we get started on the code, I wanted  to quickly give you a little bit of background of why this code was written and what it is, and what it is not.

History
-------

My background is not in disease modelling at all, but rather in tropical clouds and convection. VECTRI was an accident, that originated with my participation in an EU-FP7 project on climate impacts on health.  One task of that project was to investigate the differences between urban, peri urban and rural transmission of malaria.  As I always like to have at least a rudimentary understanding of the tools in use in any project I am involved in, I spent some time to read the paper describing the main malaria modelling tool, which was the Liverpool malaria model (LMM).  I was rather confused as the documentation made no mention of the human population density, and it seems clear that higher population densities might lead to a lower mean bite rate and reduce transmission; essentially the dilution effect. Thus it was not clear how the model could address the urban versus rural transmission question posed above. I resolved to bring this up at the project kick off meeting with the LMM developer. Eventually at the meeting dinner the opportunity arose and the conversation went something like this:

me: *"I didn't see any mention of the population density used in LMM, which source does it use?"*

LMM: *"None, we simply assume 100 people in each grid box"*

me: *"But how can the model assess rural/urban transmission differences?  Seems like it would be an easy thing to add, perhaps I could have a go so I can see how the model works.  Where can I get the source code?"*

LMM: *"You can't, we only give out the executable."*

me: *"Come on, how can people play with the model?"*

LMM: *"they can change parameter settings in the GUI interface"*

me: *"But that is not the same.  How can they add new functionality?  How can they even check to make sure the calculation is correct?"*

LMM: *"Listen, if you want a code you can play with, why don't you just write one yourself, it is not that hard"*

Now, had we been on the starter or even the main course, I probably would have just shrugged and moved the topic on, but as we had finished dessert and I had already consumed a couple of glasses of wine, I took the bait and replied, *"ok, I will!"* :-)  I invited Volker Ermert to visit for a month, who had an encyclopedic knowledge of the literature, I would ask him how process X or Y works, he would cite the relevant articles, I would code up the relationships, and then on to the next process... until 4 weeks later, VECTRI v1.0 was born!  We gave it some preliminary evaluation, and then we pushed it to the cloud for all the world to see and use; an open-source, community model for assessing the climate impacts on malaria. 


VECTRI - What it is
-------------------

It is a gridded model that primarily accounts for climate impacts on the life cycles on mosquitoes, with parameterizations for a variety of *Anopheles* and *Aedes* species. It can run on the regional, country or continental scale for resolutions down to about 1km.  At the moment it can only model one vector at a time, and the only disease represented so far is malaria when modelling *Anopheles*.  These lists are expanding with new developments though (we want to add a dengue module for example), and we also aim to link the model with the WISDOM agent based human model soon, which will allow for better representation of human mobility, co-infection, and multiple socio-economic impacts.

The model is designed to run on a grid and be fast, the ethos is to use 1st order accurate equation solvers for speed, with the justification being that inaccuracies in the knowledge of parameters and processes will far outweigh those due to the use of fast but approximate numerical solvers. You can also use it for single locations.

Above all, as you see from the history section, VECTRI is free and open source.  You can see the source code, you can change the source code, you can pass the model to your friends (although it is always best they clone their own copy), VECTRI is a community model, thus the model is  yours, have fun learning with it, criticize it, improve it!  If you do improve it, please consider becoming a VECTRI developer and contribution your improvements to the community version.  Oh and if you publish an article using it, we would appreciate knowing about it for our database. 


VECTRI - What it isn't
----------------------

We are a very small research group and thus the model is provided "as-is" and is not supported by us. We can try to respond and help you, but we can't promise fast turn around times (not unless you have funding for us to hire someone to directly interact with you in your project ;-) ). The model goes through sporadic development cycles, again contingent on external funding.

The model does have bednets and SIT as interventions, but these are **not** well tested (unlike the situation with the STPH openmalaria model).  In general the model should be used firstly as a **research tool**.  It should also be recalled that it only attempts to model climate impacts on malaria, thus the predicted clinical cases count will be considerably higher than observed due to the neglect of interventions, treatment and other factors that reduce case counts.   It should always be well tested and calibrated before any operational use, and we do not bear any responsibility for the employment of the model in this way.  If you do wish to use the model in a planning/operational environment we strongly suggest you get in touch. 

Lastly, we should emphasize that the model is not a "point and click to run" type model, it is Fortran based and a certain familiarity with Linux would be helpful, while not essential. We sporadically consider rewriting the wrapper in python and give it a GUI makeover, but there are always more interesting things higher up our to-do list I'm afraid and this never quite floats to the top.  


A Note on this manual
---------------------

The manual is currently a work in progress, please feel free to pass us any feedback about aspects you feel are not clear or sections that could be added.  T\he eventual idea is to host the latest version of the documentation always online on the VECTRI web page.  However, as the manual source is contained and built in the same git library, you can always open your local version of the manual which should pertain precisely to the version of the model you are using (assuming we manage to maintain the manual up to date).  To do this go to your browser and open the file ``PATH_TO_YOUR_VECTRI/docs/build/html/index.html`` - You will need to substitute the path to your VECTRI library which is stored in ``$VECTRI`` - read the next section to understand what this means. 
