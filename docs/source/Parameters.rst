Model Parameters
================

In this section we will introduce the parameter settings, first how to set them and then what the settings are.


How to set parameters
---------------------


In summary, there are essentially 3 ways to control parameter settings in VECTRI, via the command line, via a input file, or by direct edited of the code (not recommended unless you are aiming to develop the model with new processes). 

Command line input
^^^^^^^^^^^^^^^^^^

One of the most straight forward to set a parameter is to do it directly from the command line itself when running the code.  This is the recommended way to set options if you only want to set one or two parameters or you want to test out the impact of a parameter on the fly for a single simulation as this method is immediate and straightforward to implement.

In this method we use the ``-v`` option in the following way

.. code-block ::

    vectri -v,"param1=val,param2=val"

and so on... the options are comma separated and the option list passed as a string.  So for example, in the vectri initialization section you will see how to implement a model spinup period using two parameters which can be set in the following way:

.. code-block ::

    vectri -v,"nloopspinup=3,nlenspinup=365"


If you try this with a test run and then examine the output file vectri with ``ncdump -h vectri.nc`` you should see that the new settings for these parameters in the global attributes section.  You can also grep for these in the following way

.. code-block ::

    ncdump -h vectri.nc | grep -i spinup

    

vectri.options file
^^^^^^^^^^^^^^^^^^^

A second way of setting parameter options is to specify them in a file called "vectri.options" which should be placed in the input subdirectory that resides just below your run directory (recall that this is made after the first simulation by vectri if it doesn't exist, but you can also create it yourself with ``mkdir input``).  This way is recommended if you want to change a large number of parameters, which would get unwieldy on the command line, or if you have a set of parameter settings that are pseudo-permanent for your simulation setup.

In this method you just need to use your favorite text editor to open the file, here we use emacs for example. So from the run directory you would

.. code-block ::

    emacs ./input/vectri.options


And in that file you simply specific your options, either as a comma separated list, or easier to read is to simply put each option on a new line.  So in our example above you would enter the following text into vectri.options 

.. code-block ::

   nloopspinup=3
   nlenspinup=365

Don't forget to save the file before you run!

.. warning ::

   One word of warning, because the file vectri.options is hidden away in the input directory, it is sometimes possible to forget it is there!  If you are comparing experiments it is always a good idea to cross-check the settings in the global attributes.

.. note ::

  Both the command line ``-v`` option and the ``vectri.options`` settings are passed to the model in the form of a namelist file.  You don't need to know how this works in practise, but the key thing to realize is that first the vectri.options settings are placed in the namelist file and then the command line options.  As the model will use the last specification of a parameter, this means that in the case of parameter duplication, **the command line ``-v`` specification overrides the vectri.options one**.  This gives quite a lot of flexibility as you can use an approach of "mix-n-match" for vectri option settings. For example you may have your preferred default set up in vectri.options, and then choose to set specific test options to override these from the command line using ``-v``. It is also "safer" in that the very visible option (i.e. you see it on the command line) is the one you actually use; see warning above to understand what we mean here.

  
  
Fortran changes
^^^^^^^^^^^^^^^
You can change settings by directly editing the fortran files themselves, but this is for advanced users only and we anyway suggest you do this in a git branch to avoid making changes directly in the master.  We do not recommend this approach for changing standard parameters, it is always better to use one of the above two approaches, this is only recommended if you are making code developments.  We suggest then to add any new parameter settings you introduce to the namelist structure so you can then calibrate their specification without the need to resort to repeated recompilation of the code. 


Parameter settings tables
-------------------------

The following sections give a list of the settings you can change which can be cross-referenced directly to the model description (when that is ready!).

.. note ::

     In the tables a default value is given, but this is for *An. gambiae* and *Plas. falciparum* malaria transmission, which is what the model simulates by default if no options are given.  Please note that switching to another vector (and eventually other diseases) will change these default settings.  Please see the output of a run to see these revised defaults.


Simulation parameters
^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Simulation parameters
  :header: "Name","default","units","Description"
  :width: 65%
  :widths: auto
  :align: left

    "nloopspinup","0","","Number of spinup loops to perform on cold start"
    "nlenspinup","10","days","length of spinup loops to perform on cold start"
    "dt","1","days","Time step (do not change!)"
    "nnumeric","2","","Numerical integration scheme [0-5] (more details later)"
    "rtemperature_offset","0","K","Toy climate change, constant offset to add to input"
    "rtemperature_trend","0","K","Toy climate change, temperature trend to add to input"
    "rrainfall_factor","1","ratio","Toy climate change, multiply input rainfall by this factor"


Vector parameters
^^^^^^^^^^^^^^^^^

.. csv-table:: vector parameters 
  :header: "Name","Default", "Description"
  :width: 65%
  :widths: auto
  :align: left

    "neggmn","60 *","Number of eggs per batch resulting in female adults"
    "rlarv_tmin","12.16",""
    "rlarv_tmax","38",""
    "rlarv_eggtime","1",""
    "rlarv_pupaetime","1",""
    "rlarv_flushmin","0.4",""
    "rlarv_flushtau","20",""
    "rlarv_ermert","0.08333",""
    "rlarv_jepson","0.011",""
    "rlarv_bayoh","0.005",""
    "rbeta_indoor","0.2",""
    "rmasslarv_stage4","0.45",""
    "rbiocapacity","100",""
    "rlarvsurv","0.987",""
    "rbiteratio","0.6",""
    "rbitehighrisk","5",""
    "rvect_diffusion","0",""
    "rzoophilic_tau","30",""
    "rzoophilic_min","0.1",""
    "rtgono","7.7",""
    "dgono","37.1",""
    "rtsporo","16",""
    "dsporo","111",""
    "nlarv_scheme","4",""
    "rvecsurv","0.95",""
    "nsurvival_scheme","2",""
    "rmar1","0.45f, 0.054f, -0.0016",""
    "rmar2","-4.4f, 1.31f, -0.03",""
    "rtvecsurvmin","5",""
    "rtvecsurvmax","39.9",""


Disease parameters
^^^^^^^^^^^^^^^^^^
.. csv-table:: disease parameters 
  :header: "Name","Default", "Description"
  :width: 65%
  :widths: auto
  :align: left

    "rhostclear","15",""
    "rhostimmuneclear","300",""
    "rpthost2vect_I","0.25",""
    "rpthost2vect_R","0.1",""
    "rptvect2host","0.15",""
    "rhost_infectd","20",""
    "rhost_detectd","9",""
    "rimmune_gain_eira","300",""
    "rimmune_loss_tau","365",""
    "rhost_infect_init","0.1",""

Hydrology parameters
^^^^^^^^^^^^^^^^^^^^
.. csv-table:: hydrology parameters 
  :header: "Name","Default", "Description"
  :width: 65%
  :widths: auto
  :align: left

    "wperm_default","1.e-06",""
    "npud_scheme","2",""
    "wpond_rate","0.001",""
    "wpond_CN","85",""
    "wpond_min","1.e-06",""
    "wpond_max","0.2",""
    "wpond_evap","5",""
    "wpond_infil_clay","50",""
    "wpond_infil_sand","700",""
    "wpond_infil_silt","250",""
    "wpond_shapep2","1",""
    "rwater_tempoffset","2",""
    "wpond_ratio","0.9",""
    "wperm_ratio","0.05",""
    "wurbn_ratio","0.05",""
    "wurbn_tau","20",""
    "wurbn_sf","0.005",""

Population parameters
^^^^^^^^^^^^^^^^^^^^^
.. csv-table:: VECTRI parameters 
  :header: "Name","Default", "Description"
  :width: 65%
  :widths: auto
  :align: left

    "rpop_death_rate","0.02",""
    "rpopdensity_min","1",""
    "rmigration","1.e-05",""


Interventions parameters
^^^^^^^^^^^^^^^^^^^^^^^^
.. csv-table:: VECTRI parameters 
  :header: "Name","Default", "Description"
  :width: 65%
  :widths: auto
  :align: left

    "rsit_breed","0.56",""
    "rsit_mortality","1",""
    "rbednet_tau","1052",""





