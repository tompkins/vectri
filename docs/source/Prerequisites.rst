Prerequisites
=============

In this section, we will see how which software we need to have installed to run VECTRI and analyse and manipulate the output, how to get the code on your computer and how to set up the environment to run the code.

.. note::
   All steps in this section only need to carried out once on the machine you are running on. Once these steps are successfully completed, they never need to be repeated, ever! Not even once more! Promise!

Libraries and software requirements
-----------------------------------

The VECTRI code is a FORTRAN90 code with a bash shell script “wrapper”.  Throughout this documentation we will assume you are working on a flavour of a linux platform (e.g. Ubuntu or Mint for example).  Notes on installation on MAX OSX and Windows are given below, but the steps are very similar. 

At the moment, in order to install and run it you will need to install the following libraries. In ubuntu this can be done using the apt install command:

`git <https://git-scm.com/>`_ :

.. code-block:: console
		
    sudo apt install git

`gfortran <https://gcc.gnu.org/fortran/>`_ to compile the code

.. code-block:: console
		
    sudo apt install gfortran

The appropriate `netcdf <https://www.unidata.ucar.edu/software/netcdf/>`_ libraries

.. code-block:: console

    sudo apt install netcdf-bin
    sudo apt install libnetcdf-dev
    sudo apt install libnetcdff-dev

You will probably also find `ncview <https://linux.die.net/man/1/ncview>`_ , `cdo <https://code.mpimet.mpg.de/projects/cdo>`_ and the `nco <https://nco.sourceforge.net/>`_ command suite useful for postprocessing and viewing the files:

.. code-block:: console

    sudo apt install cdo
    sudo apt install ncview
    sudo apt install nco

The model reads all input from, and writes all output to files in the netcdf self-describing file format.  If you are unfamiliar with this format and how to use ncview to view these and the cdo toolbox to manipulate files, video tutorials can be found on `ClimateUnboxed <https://www.youtube.com/channel/UC94xkaJn1NkxR4trAfVArbg>`_. 

Mac Users
^^^^^^^^^
Installation on a MAC is super easy.  If you are using homebrew, then the above software can mostly be installed by replacing sudo apt with brew, but you may need to google some 

.. code-block:: console

    brew install gcc
    brew install netcdf-fortran
    brew install cdo
    brew install ncview
    brew install nco
    
Windows 10+ users
^^^^^^^^^^^^^^^^^

If you are a windows user, using version 10 or higher, it is now possible and very straightforward to install native ubuntu (or other linux flavours) directly under windows, without the need for cygwin or a wine server. There are many online sites that show you how to do this, but the easiest is probably to follow the `instructions for Ubuntu <https://ubuntu.com/desktop/wsl>`_. We strongly recommend that you install linux under windows which will then enable you to easily install the above software and run the model (we have successfully done this at a number of workshops and training events).

Note on GRIB
^^^^^^^^^^^^
For use at ECMWF, there is also the option for using a `GRIB <https://en.wikipedia.org/wiki/GRIB>`_ input/output interface instead of netcdf, but most users will rely on the standard interface, and this interface is not regularly tested and is frequently broken. We advice all users to use the netcdf interface whenever possible. 

Directories
-----------

Before you run the code familiarise yourself with the concept of directories and subdirectories in your file system. When you want to use VECTRI there are two directory locations that are important, the one where you STORE the code and the one where you RUN the code.

.. warning::
    As the code is managed using the repository software git, you *never* want to run the code in the directory or subdirectory of the code location, to avoid making lots of files that git will start to track!  This makes pulling new future shiny versions of VECTRI super problematic.  In fact, the code run script tries to detect if you do this and prevents this occurrence.

We suggest that you place the code in a location such as

.. code-block:: console

    $HOME/vectri

(`$HOME` is an environmental variable that points to your home directory, if you are unfamiliar with environmental variables, we will return to those in the next section.

and run it somewhere in parallel such as

.. code-block:: console

    $HOME/myruns

although for large runs with lots of output you may want to run the model in a scratch or wrk subdirectory of course. 

Download the code
-----------------

To get the code, we simply clone it using git.  The following git command will make a new subdirectory called "vectri" to your present location. So if you want the code to reside in `$HOME/vectri`, your terminal window will need to be in the directory location above, that is `$HOME`.  Change to the parent directory where you want to host the code, e.g.:

.. code-block:: console

    cd $HOME

and now we are ready to retrieve a copy.

Let's clone it using git:

.. code-block:: console
		
   git clone http://www.gitlab.com/tompkins/vectri
   
Now remember, please **always** clone from the gitlab repository, don't copy the code from colleagues, and don't send them copies.  By cloning from gitlab, you get the latest code version, and are linked to the online repository which makes it super easy in future to get all updates to the code and stay up to date. To reiterate the note above, **you only ever need to clone the code once, this step never needs to be repeated on this machine**. To upgrade the code, see below. 

Updating the code
^^^^^^^^^^^^^^^^^
At the very start, we said that all the steps in this section only need to be carried out once.  This is also true for the cloning of the model.  In future, if you want to *update* the code to the latest version, you just need to run the command:

.. code-block:: console
		
   git pull 

from the top directory.  If you want to do any code developments yourself, we strongly recommendation you to do this in `git branches <https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell>`_, keeping the master branch clean to permit easy code upgrades. If you think your contributions are useful for the wider user community, please get in touch to be added as a developer!

.. tip::
   If you want to use VECTRI as a research tool and make any changes to the code beyond simple parameter setting modifications, we strongly recommend that you take a little time get conversant with the basics of git, including branches, merging and checking in.  Day to day use of git only requires the use of a handful of commands and it will enormously facilitate your software management processes.  Under no circumstances should you start to make multiple clones of the software in different directories to "try something out quickly".  This is a surefire recipe for getting yourself in a mess, and was the motivation for software management tools such as git to be developed in the first place!


Setting up the Environment
--------------------------

Now there is one more thing we need to do before we start, and that is to set up a few environmental variables. We mentioned these at the start.  A variable in the shell (we will assume you are using bash or zsh here) is definied as simply as this

.. code-block::

   a=1

try typing this and then try ``echo a``.  What do you see?  The letter "a"! That's because to actually read the contents of a variable in shell you need the dollar sign "$".  If you instead try

.. code-block::

    echo $a

You should hopefully now see a "1" on the terminal screen.  If you just see a blank line, the variable is undefined. This maybe because you used a uppercase letter by mistake?

.. tip::
   In shell, variable names are case-sensitive!  In the following ensure that you use upper case at all times to match the instructions. 

VECTRI needs to know where to find the source code, which compiler you prefer and where the system netcdf libraries are stored. Thus we define:

.. list-table:: Environmental variables required by VECTRI
   :widths: 25 100
   :header-rows: 1

   * - VARIABLE
     - definition
   * - VECTRI
     - this will point to where the code repository is held
   * - NETCDF_LIB
     - the library link commands for NETCDF
   * - NETCDF_INCLUDE
     - the include directory for NETCDF
   * - FC
     -  the preferred fortran compiler

So the first variable is called ``VECTRI``, which points to the code. This is so, no matter where you run, the scripts know where to find the code. Setting the variable will also unfortunately depend on the flavour of shell you use:

ksh, zsh or bash:

.. code-block:: console

    $ export VECTRI=directorylocation

csh (not recommended):

.. code-block:: console

    $ setenv VECTRI directorylocation

**Reminder: UPPER CASE “VECTRI” is important**. So, for example, if your code is in ``$HOME/vectri`` and you use bash then you need to type

.. code-block:: console

    $ export VECTRI=$HOME/vectri

test it by typing

.. code-block:: console

	$ ls $VECTRI

and you should see a list of directories and files similar to this (but longer!)

.. code-block:: console

    data		papers		source
    calibration	docs		public		utils ...


If instead you get a blank response, you haven't followed the steps above correctly.
The other environmental variables point to the relevant libraries. On a standard mint/ubuntu set up, you may set them as follows (not recommended):

NETCDF variables
^^^^^^^^^^^^^^^^

.. code-block:: console
		
    export NETCDF_LIB="-L/usr/libx -lnetcdff -lnetcdf"
    export NETCDF_INCLUDE="-I/usr/include"
    export FC=gfortran

If you are using linux straight "out of the can" on a ubuntu desktop, this is likely to work for you. However, often on clusters, mainframes and MACS this won't work; the library setup can be diverse. Do not panic, there is a utility that can help called ``nf-config`` which is installed as part of the netcdf suite. Check you have it by typing this:

.. code-block:: console

    nf-config --flibs

So you can use this to set the variables in the following way (recommended!): 

.. code-block:: console

    export NETCDF_LIB=$(nf-config --flibs)
    export NETCDF_INCLUDE=$(nf-config --fflags)
    export FC=$(nf-config --fc)

We always recommend using this second method as it is robust across platforms and installations. 
		
Permanently setting
^^^^^^^^^^^^^^^^^^^

You could simply define these each time you run the code, but that would be pretty tedious, as you don't want to do this each time you log in to a new konsole shell terminal. Thus it is more convenient to place these commands in the start-up script that runs when you open a new console in your $HOME. This will depend on your linux flavour (.kshrc, .login, .profile, etc).

For example, I use bash and so my "start up" is ``$HOME/.bashrc`` (**the dot is important, and the filename is lower case**), but on some systems it may be ``.bash_profile``. Go to your home directory and open up this file using your favourite text editor, we use emacs, but any text editor is fine:

.. code-block::

   cd $HOME
   emacs .bashrc

Then place those four lines in the script:

.. code-block:: console

    export VECTRI=$HOME/vectri
    export NETCDF_LIB=$(nf-config --flibs)
    export NETCDF_INCLUDE=$(nf-config --fflags)
    export FC=$(nf-config --fc)


save and close the file.  Now test it by opening a **new** terminal window (tip: CTRL-SHIFT-T on ubuntu, or Command-T on MAC is a shortcut to open a new tab). In the **new** window, typ

.. code-block::

   ls $VECTRI

and again you should see the list of files and directories, if you get a blank line, you have done something wrong, or placed the snippets in the wrong startup file.

.. tip::
   Take your time to complete the above steps carefully and set up the environment variables so they are all defined correctly on a new log in. This will save you a lot of time in future if you become a regular user of the code. 
