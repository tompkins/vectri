Running VECTRI
==============

While the bulk of the code is written in Fortran90, the user calls the code with the aid of a wrapper script written in bash shell script, called, wait for it... vectri.  The code reside in the top level directory of the repository.  Rememeber how we said in the last section that we should never run in the repository directory or a subsection thereof?  Good. So instead let's make our run directory and change directory to that location.

.. code-block::

   mkdir $HOME/run
   cd $HOME/run

The first command makes the directory you want to run from. You only need to do that once, although you might want to make a different new directory for other future new runs of course. Remember you can always check where you are with the "print working directory" command ``pwd``.

Let's just try to run the code and see what happens!

.. code-block::

    $VECTRI/vectri

Now as we have not provided any input files for climate or population data (we will see later that these are the bare minimum requirements for a simulation), VECTRI can't do anything and instead dumps the use log.  So you should see something like this:

.. code-block::
   
    +++ run command is: /Users/tompkins/vectri/vectri
    +++ host is lapt
    --------------
     vectri usage
    --------------
    From version 1.5 in Feb 2019 the code has a simplified interface
    ...

Which is followed by a list of command line options, some of which are self-explanatory but mostly will seem obscure at the moment.

.. tip::
   You may get fed up always typing the ``$VECTRI/`` before the ``vectri`` command to "point" towards the remote directory location.  One way to shorten this is to set up an `alias <https://en.wikipedia.org/wiki/Alias_(command)>`_ , which is essentially the process of defining a new command. Remember that ``.bashrc`` or ``.zshrc`` file that we used earlier to set up the environmental variables?  If you open that up again and add the following line ``alias vectri="$VECTRI/vectri"``, then you define a new command vectri which always points to the bash script. Test it by opening a new shell window and then typing ``vectri``. Cool no? From here on, we will assume you set up an alias and will call the code with just ``vectri`` in the documentation for brevity.
   

Key input fields
----------------

While there are many optional inputs to VECTRI, there are three fields that are needed to operate as a minimum: Daily 2 meter temperature, daily rainfall, and a population density dataset.  These can be for a single location or for a grid of lat-lon points. 

.. warning:: Grid fields
	     
   Note that all input fields need to be on identical spatial regular lat-lon grids.  The wrapper scripts no longer attempt to interpolate to put everything on the same grid, and it is your responsibility to regrid data to ensure all grids are identical. The model output matches that of the climate data (or temperature data in the case of separate temperature and rainfall files). The model is "blind" regarding data, thus if you pass it a population file that has exactly the same number of latitude and longitude points as the climate file, but for a different location of the globe, it will go ahead and read it and run anyway, so be careful!!! (If the grids differ it will simply fall over).

climate fields
^^^^^^^^^^^^^^

The name of the climate file that contains temperature and rainfall is passed to VECTRI using the ``-c`` option, i.e.

.. code-block::

   vectri -c climate_file

However, for your convenience, there is also the option to pass precipitation in a separate file, which is specified using ``-p``, so in this case you would write

.. code-block::

   vectri -c temperature_file -p precipitation_file

The climate fields need to have specific field names, such as the standard names used by CMIP, ERA5 or WMO grib table 128, but we will explain that later in the :doc:`../Data` section.

Population fields
^^^^^^^^^^^^^^^^^

All other data needs to passed in a datafile.  This file can optionally contain information about soil types, terrain slope, permanent water bodies and interventions, but the inclusion of population density is mandatory.  The data file name is specified with the ``-d`` option, thus the minimum run command example is thus:

.. code-block::

   vectri -c climate_file -d data_file


Example Simulation
------------------

With no further ado, let's try a sample run using some built in files, which can found in the directory ``$VECTRI/data``.  In this directory there are two files ready for you: ``example_sys5.nc`` and ``example_data.nc``.  These are just a single seasonal forecast from ECMWF and a population dataset.  Let's try a simulation with these.  Make sure you are in your designated run directory and then type:

.. code-block::

   vectri  -c $VECTRI/data/example_sys5.nc -d $VECTRI/data/example_data.nc

and hopefully if you have set uo correctly you should see output like this

.. code-block::
   
   +++ run command is: /Users/tompkins/vectri/vectri -c /Users/tompkins/vectri/data/example_sys5.nc -d /Users/tompkins/vectri/data/example_data.nc
   +++ host is lapt
   IVEC 0 /Users/tompkins/vectri/data/example_sys5.nc /Users/tompkins/vectri/data/example_data.nc
   +++ VECTRI version: v1.11.3-10-g3269ec4
   +++ compiling the model +++

Followed by a set of compile directives:

.. code-block::
   
   gfortran `nf-config --fflags`  -c -O0 -g -cpp -Wall -fcheck=all -fbacktrace -ffree-line-length-none mo_climate.f90 -o mo_climate.o `nf-config --flibs` `nc-config --libs`
   gfortran `nf-config --fflags`  -c -O0 -g -cpp -Wall -fcheck=all -fbacktrace -ffree-line-length-none mo_constants.f90 -o mo_constants.o `nf-config --flibs` `nc-config --libs`



etc.  If the model compiles successfully there should then be a number of messages about the options chosen

.. code-block::
   

    +++ running +++
    running ./input/vectri.exe
     %I: Vector: Anopheles Gambiae                                                                                   
     %I: Disease: Plasmodium Falciparum                                                                               
     -------------- VECTRI v1.11.3-10-g3269ec4                                                                                  ---------------
     run date Thu 24 Oct 2024 12:06:12 CEST                                                                       
     %I: opening climate file /Users/tompkins/vectri/data/example_sys5.nc
     %I: Found rain variable called: tp

and so on finishing with...

.. code-block::

   step          1 INTEGRATION STARTS
   step        215 input/output closed - now dump restart
   restart finished ok
   integration finished
   Check point            1 end of code cpu:   1963.78809


If this is the case, your simulation has ended correctly and if you type ``ls``, you should find the output file ``vectri.nc`` has appeared.

.. tip::
   What has happened?  If you dig around, you will notice that a new subdirectory called ``input`` has been made.  In that directory you will find symbolic links to the source code, compiled routines, and some other files for restarting a run that we will come to later. In your run directory itself there is a namelist file and the output file called ``vectri.nc``.  You can actually specify a different output file name using the ``-o`` option.  See the input section for more details.
   







