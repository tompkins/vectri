Command Line Options
====================

In this section we will show you how to set simulation options in the model.  We have already seen two of these, namely ``-c`` to set the climate file and ``-d`` to set the data file.  The other options are described in the table below.  Not all of these will be clear to you until you have gone through the following sections of the documentation.  

.. csv-table:: VECTRI command line options
  :header: "Name", "Description"
  :width: 65%
  :widths: auto
  :align: left

  "-a","path/file, arguments file setting vectri options (default=./input/vectri.options)  (note: If you change the input directory from the default using the -r option then you  need to specify this option)" 
  "-c","climate file name (can be grib or netcdf) - auto detect from the file extension .grb/.nc  This file contains the temperature data, and optionally also the rainfall, see -p option" 
  "-d","netcdf data file containing population (mandatory), permanent breeding fraction (optional), land use category (optional)"
  "-e","ensemble number. Only needed if you are running an ensemble for bit reproducibility. Random seed set using this ensemble  member number"
  "-g","force climfile format to be considered grib (default is netcdf otherwise if not .grb) (if filename passed with -c ends in .grb then this argument not required)"
  "-h","print this usage/help message"
  "-i","initfile (restart file name)
        if this is not set (or given file doesn't exist), artifical initial conditions are employed. These set initial the PR to a default of 5%, unless modified by rhost_infect_init"
  "-n","namelist file path/name (default ./vectri.namelist)"
  "-o","output filename (default vectri.nc)"
  "-p","precipitation filename (if precipitation is contained in climate file set with -c option then this is not required)" 
  "-r","location of input directory (default=./input)"
  "-u","debug compilation (-g)"
  "-v","pass a set of namelist options as a string (alternative to editing vectri.options), example -v ""nloopspinup=3,nlenspinup=180"""
  "-x","Select Vector to simulate, 0: An. gambiae (default), 1: An. funestus (unvalidated), 2: An. sacharovi, 10: Ae. albopictus, 11: Ae. agypti (under development)" 
  "-z","text message output path/file (default is output to screen)"


